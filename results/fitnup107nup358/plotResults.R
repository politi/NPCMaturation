library("Hmisc")

# -----
setwd('/Users/toni/Dropbox/Shotaro_PoreMaturation/Simulations/LiveCellImaging')
simulNUP107<-read.csv('simulated_NUP107.csv', skip=3, sep='\t')
simulNUP358<-read.csv('simulated_NUP358.csv', skip=3, sep='\t')
datain <-read.csv('/Users/toni/Dropbox/Shotaro_PoreMaturation/ExpData/LiveCellImaging/HeLa4D.csv', sep='\t')

# -----
plotDataF <-function(d, simul, names, cols, alpha,  fname) {
  # function to plot data no separation between interphase and postmitotic
  pdf(fname, width = 2.8, height=2, useDingbats = F,pointsize = 8, paper="special", fonts="Helvetica")
  lw<- 2
  par(mar=c(4, 4, 1,1), mgp=c(2.5,1,0))
  plot(d$x, d$y, xlab = "", ylab = "Normalized total intensity", 
       type = "n", yaxt = 'n', xaxt = 'n', xlim = c(0, 120), ylim = c(0, 1.4), bty='l')
  axis(1, tck = -0.05, at=c(0,20,40,60,80,100,120))
  axis(2, las=2, tck =-0.05, at=c(0,0.2,0.4,0.6,0.8,1,1.2))
  mtext(side=1, text="Time after AO (min)", line=2)
  cols1_t <-adjustcolor(cols[1], alpha.f = alpha) 
  polygon(c(d$x, rev(d$x)), c(d$upper, rev(d$lower)), col = cols1_t, border = NA)
  points(d$x, d$y, pch = 17, col = cols[1], cex=0.6)
  colfun <- colorRampPalette(c(cols[1], 'gray60'))
  lines(simul[,names[1]], simul[,names[2]], lwd = lw, col = colfun(50)[40])
  lines(simul[,names[1]], simul[,names[3]], lwd = lw, lty = 2, col = cols[2])
  lines(simul[,names[1]], simul[,names[4]], lwd = lw, col=cols[2])
  par(xpd=TRUE)
  legend(0,1.65, c("data", "post-mitotic+interphase", "post-mitotic", "interphase"), 
         lwd = c(0,1.5,1.5,1.5),pch = c(17,-1,-1,-1), lty = c(0,1,2,1), 
         col=c(cols[1], colfun(50)[40], cols[2], cols[2]), horiz = F, bty='n', 
         y.intersp = 0.88, cex = 0.88)
  dev.off()
}


# -----
plotDataAll <-function(d1,d2, simul, names, cols, alpha,  fname) {
  # function to plot data no separation between interphase and postmitotic
  pdf(fname, width = 2.8, height=2, useDingbats = F,pointsize = 8, paper="special", fonts="Helvetica")
  lw<- 2
  par(mar=c(4, 4, 1,1), mgp=c(2.5,1,0))
  plot(d1$x, d1$y, xlab = "", ylab = "Normalized total intensity", 
       type = "n", yaxt = 'n', xaxt = 'n', xlim = c(0, 120), ylim = c(0, 1.4), bty='l')
  #title('NUP107 inner-core region')
  axis(1, tck = -0.05, at=c(0,20,40,60,80,100,120))
  axis(2, las=2, tck =-0.05, at=c(0,0.2,0.4,0.6,0.8,1,1.2))
  mtext(side=1, text="Time after AO (min)", line=2)
  #transparent colors
  cols1_t <-adjustcolor(cols[1], alpha.f = alpha) 
  cols2_t <-adjustcolor(cols[2], alpha.f = alpha) 
  #plot data for Non-core and core region
  polygon(c(d1$x, rev(d1$x)), c(d1$upper, rev(d1$lower)), col = cols1_t, border = NA)
  polygon(c(d1$x, rev(d1$x)), c(d2$upper, rev(d2$lower)), col = cols2_t, border = NA)
  points(d1$x, d1$y, pch = 17, col = cols[1], cex=0.6)
  points(d1$x, d2$y, pch = 17, col = cols[2], cex=0.6)
  
  #plot simulations
  lines(simul[,names[1]], simul[,names[2]], lwd = lw, col = cols[1])
  lines(simul[,names[1]], simul[,names[3]], lwd = lw, col = cols[2])
  par(xpd=TRUE)
  legend(0,1.65, c("Non-core", "Inner-core","Non-core model", "Inner-core model"), 
         lwd = c(0,0, 1.5,1.5), pch = c(17,17, -1,-1), lty = c(0,0, 1,1), 
         col= c(cols[1], cols[2], cols[1], cols[2]), horiz = F, bty='n', 
         y.intersp = 0.88, cex = 0.88)
  
  dev.off()
}

# -----
plotData_IP <-function(simul1, simul2, names, cols,  fname) {
  pdf(fname, width = 2.8, height=2, useDingbats = F,pointsize = 8, paper="special", fonts="Helvetica")
  lw<- 2
  par(mar=c(4, 4, 1,1), mgp=c(2.5,1,0))
  plot(simul1[,names[1]], simul1[,names[2]], xlab = "", ylab = "Normalized interphase density", 
       type = "n", yaxt = 'n', xaxt = 'n', xlim = c(0, 120), ylim = c(0, 1.1), bty='l')
  axis(1, tck = -0.05, at=c(0,20,40,60,80,100,120))
  axis(2, las=2, tck =-0.05, at=c(0,0.2,0.4,0.6,0.8,1))
  mtext(side=1, text="Time after AO (min)", line=2)
  lines(simul1[,names[1]], simul1[,names[2]], lwd = lw, lty = 1, col = cols[1])
  lines(simul2[,names[1]], simul2[,names[2]], lwd = lw, lty = 1, col = cols[2])
  par(xpd=TRUE)
  legend(0,1.1, c("Nup107", "Nup358"), 
         lwd = c(1.5,1.5),
         col=c(cols[1], cols[2]), horiz = F, bty='n', 
         y.intersp = 0.88, cex = 0.88)
  
  dev.off()
}


# ----
nc107 <- data.frame('x'= datain$time, 'y' = datain$NUP107_NC, 'upper'=datain$NUP107_NC + datain$NUP107_NC_std, 
                 'lower' = datain$NUP107_NC - datain$NUP107_NC_std)
c107 <- data.frame('x'= datain$time, 'y' = datain$NUP107_C, 'upper'=datain$NUP107_C + datain$NUP107_C_std, 
                     'lower' = datain$NUP107_C - datain$NUP107_C_std)

nc358 <- data.frame('x'= datain$time, 'y' = datain$NUP358_NC, 'upper'=datain$NUP358_NC + datain$NUP358_NC_std, 
                    'lower' = datain$NUP358_NC - datain$NUP358_NC_std)
c358 <- data.frame('x'= datain$time, 'y' = datain$NUP358_C, 'upper'=datain$NUP358_C + datain$NUP358_C_std, 
                   'lower' = datain$NUP358_C - datain$NUP358_C_std)

plotDataAll(nc107, c107,  simulNUP107, c('time','NC', 'C'), c('#20BCEF', '#A2C964' ), 0.4, 'NUP107.pdf')
plotDataAll(nc358, c358,  simulNUP358, c('time','NC', 'C'), c('#20BCEF', '#A2C964' ), 0.4, 'NUP358.pdf')

plotDataF(nc107,  simulNUP107, c('time','NC', 'NC_pm', 'NC_ip'), c('#20BCEF', 'gray20'), 0.4, 'NUP107_NC.pdf')
plotDataF(c107,  simulNUP107, c('time','C', 'C_pm', 'C_ip'), c('#A2C964',   'gray20'), 0.4, 'NUP107_C.pdf')

plotDataF(nc358,  simulNUP358, c('time','NC', 'NC_pm', 'NC_ip'), c('#20BCEF', 'gray20' ), 0.4, 'NUP358_NC.pdf')
plotDataF(c358,  simulNUP358, c('time','C', 'C_pm', 'C_ip'), c('#A2C964', 'gray20'), 0.4, 'NUP358_C.pdf')

plotData_IP(simulNUP107, simulNUP358,  c('time','C_ip_density'), c('#A2C964' , '#d01c8b'), 'NUP107_358_C_den.pdf' )
#c('#A2C964', '#C1FF00')