% Model 
% N 5
% useStd 0 useIntermediate 1 useSpline 0
% fittedIds 3  6  7  8  9
% fittedPar k P01 M01 P02 M02
% factParvariability 0.50
% ts	v	k	stdk	kd	P01	M01	P02	M02	norm	nrpara
1.000e+01	1.500e-02	9.976e-02	0.000e+00	4.200e-04	2.569e+03	2.075e+03	2.344e+03	3.990e+03	3.563e+01	5
1.000e+01	1.500e-02	9.976e-02	0.000e+00	4.200e-04	2.569e+03	2.075e+03	2.344e+03	3.990e+03	3.563e+01	5
1.000e+01	1.500e-02	9.977e-02	0.000e+00	4.200e-04	2.570e+03	2.075e+03	2.344e+03	3.990e+03	3.563e+01	5
