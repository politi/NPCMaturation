!!THIS REPO HAS MOVED TO THE MPIBPC REPO!!!
!!THIS REPO HAS MOVED TO THE MPIBPC REPO!!!


# NPC maturation
This repositories contains code and data for the anlysis and computational modelling of NPC maturation

## Extract cytoplasmic and nucleoplasmic intensities in the absence of an external marker
This pipeline combines ilastik, and matlab workflows. Images are typically starting from metaphase.
1. Convert lsm files to hdf5 for reading in ilastik
2. For a given cell line create or reuse a classifier (ilastik 1.8)
3. Process all files and save segmented images