# odeModel: Functions to compute odeModel of NPC maturation
Models account for  an early (e.g. NUP107) and a late  (e.g. NUP358) NUP


Differnce in assembly pathway, i.e. interphase and postmitotic assembly pathway, is simulated by differences in rate constants k1 and k2. The kinetics in the core and non-core region is a linear superposition of postmitotic and interphase assembly. All in all assembly in core and non-core region is characterized by 4 rate constants and the ratio of post-mitotic to interphase assembly in the two regions (2 parameters).

Model used in Otsuka et al. Elife 2016 was the using the total number/fluorescence of proteins (``proteinMaturationTotal.m``). The density based model and the model considering the total are basically the same.

In Otsuka et al. N1 = 1 and N2 = 1. There was no difference in using more steps. 

# Class absPoreMaturation
Abstact class for model of pore maturation. Multi step process but with same rate constant. N1 steps rate constant k1 and N2 steps rate constant k2. N1 steps until protein1 binds. Total number of steps is N1+N2+1. Protein 2 binds only at the end.

k1 set the rate for formation of protein1 containing complex
k2 set the rate of intermediate steps until protein2 is
integrated
X_1 -k1> X_2 ... -k1> X_(N1+1) -k2> X_(N1+2) ... -k2> X_(N1+N2+1)
sum X_(i=N1+1)_end: protein1
    X_(N1+N2+1): protein2 

## proteinMaturationTotal 
Multi step maturation process N1 steps with rate equal k1 and N2 steps with rate equal k2
Compute total number of pores so no need to use the surface area.


## proteinMaturationDen

Multi step maturation process N1 steps with rate equal k1 and N2 steps with rate equal k2
Use surface area to compute density. runfitlive execute the fit using the data for NUP107 and NUP358.

# Class absPoreMaturationMultiPar
Abstact class for model of pore maturation. Multi step process but with different rate for each step. 
N total steps, N1 steps until first protein binds.


	X_1 -k1> X_2 ... -k2> X_(N1+1) -k3> X_(N1+2) ... -kN-1> X_(N)
	sum X_(i=N1)_end: protein1
		X_(N): protein2

k1-k_{N-1} are the transition rates for postmitotic and interphase (different set of parameters for the two processes). The last transition is the accumulation of the 2nd protein (NUP358) N1 is the position where the first protein is present
In some models degradation and production of the proteins is also included

## poreMaturationTotalProdDeg
Multi step maturation process with N steps and N-1 rate constants. Include protein production and degradation. Compute total number of pores so no need to use the surface area.

	X_1 -k1> X_2 ... > X_(N1+1) -k_{N1+1}> X_(N1+2) ... -k_{N-1}> X_(N)
    	sum X_(i=N1)_end: protein1
        X_(N): protein2
    paramters are:
    	* k1-k_{N-1} (postmitotic); k1-k_{N-1} (interphase); kd; vproduction;
    	* fraction_maturePore_noncore; fraction_maturePore_core;
    	* scaling_factor_protein1_NC;scaling_factor_protein2_NC;scaling_factor_protein1_Core;
    	* scaling_factor_protein2_Core;
    



## poreMaturationReducedPara
Multi step maturation process N1 steps with rate equal k1 and N2 steps with rate equal k2 and protein production and degradation. Compute total number of pores so no need to use the surface area.

	X_1 -k1> X_2 ... -k1> X_(N1+1) -k2> X_(N1+2) ... -k2> X_(N)
    	sum X_(i=N1)_end: protein1
        X_(N): protein2
    paramters are:
    	* k1, k2 (postmitotic); k1, k2 (interphase); kd; vproduction;
    	* fraction_maturePore_noncore; fraction_maturePore_core;
    	* scaling_factor_protein1_NC;scaling_factor_protein2_NC;scaling_factor_protein1_Core;
    	* scaling_factor_protein2_Core;
    

## poreMaturationReducedPara2
Similar to poreMaturationReducedPara only that for multi-step and different rate constant for each step. 
