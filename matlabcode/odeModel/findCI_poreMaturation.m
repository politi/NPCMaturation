%%
% find confidence Interval using likelihood profile method for ode model.
% The scaling coefficients are not varied
% pM: class of model
% filenames: 4 filenames containing parameter values
function findCI_poreMaturation(pM,  filenames)
    try
        k1_pm = load(filenames{1});
        k2_pm = load(filenames{2});
        k1_ip = load(filenames{3});
        k2_ip = load(filenames{4});
    catch
        %priming
        tpm = [4 4];
        % time begin interphase assembly for Non-core and core region
        tip = [10 10];
        hb = [2 2 2 2 2 2 2 2];
        lb = [0  0  0  0 0 0 0 0];
        opti  = optimset('Display', 'iter');
        solB = [0.3549    0.0449    0.0435    0.0492    0.92    0.5    1    1    1    1]; %bset solution
        ifit = [1 2 3 4]; %parameters to fit
        for i=1:4
            iv = hb.*rand(1,length(hb))
            [parFit{i}, norm(i)] = lsqnonlin(@pM.distData,iv, lb, hb, opti, [1:4], solB);
        end
        normB = min(norm); 
        ibf = find(norm == normB);
        solB = pM.getparset(parFit{ibf}, ifit, solB);
        
        %% vary each one parameter 
        minP = 0.2;
        
        fitParCI = {[2 3 4], [1 3 4], [1 2 4], [1 2 3]};
        %fit parameter [2 3 4] and vary parameter 1 step by step, etc.
        step_p = 100;
       
        for i=1:4
            hb = [2 2 2];
            lb = [0  0  0];
            fidloc = fopen(filenames{i}, 'w');
            fprintf(fidloc, '%%k1_pm \t k2_pm \t k1_ip \t k2_ip \t norm \t normBest\n');
            ifit = fitParCI{i};
            solLoc = solB;
            parVar = solB(i)*10.^((-minP+minP*2*[1:step_p]/step_p));
            for j = 1:step_p
                solLoc(i) = parVar(j);
                iv = solLoc(ifit);
                [parFit, norm] = lsqnonlin(@pM.distData,iv, lb, hb, opti, ifit, solLoc);
                sol = pM.getparset(parFit, ifit, solLoc);
                fprintf(fidloc, '%.3e \t %.3e \t %.3e \t %.3e \t %.3e \t %.3e\n', sol(1), sol(2), sol(3), sol(4), norm, normB);
            end
            fclose(fidloc);
        end
        k1_pm = load(filenames{1});
        k2_pm = load(filenames{2});
        k1_ip = load(filenames{3});
        k2_ip = load(filenames{4});
    end
    
    %%
    NrDataPts = size(pM.protF(1).core,1)+size(pM.protF(2).core,1) + size(pM.protF(1).noncore,1)+size(pM.protF(2).noncore,1);
    parList = {k1_pm, k2_pm, k1_ip, k2_ip};
    for i=1:4
        par = parList{i};
        CI = par(:,i).*((abs(NrDataPts*log(par(:,end-1)/NrDataPts) - NrDataPts*log(par(:,end)/NrDataPts)))<chi2inv(0.95,1));
        CIL(i,:) = [par(find(par(:,end-1)-par(:,end)<1e-5), i) min(CI(find(CI>0))) max(CI(find(CI>0)))];
    end

end

