classdef poreMaturationTotalProdDeg < absPoreMaturationMultiPar
    %% Multi step maturation process steps with rates equal k1-kN. First N1 steps 
    %% are for protein 1, last step is protein 2. Includes protein degradation
    % Compute total number of pores so no need to use the surface area
    % k1-kN-1 are the transition rates for postmitotic and interphase. The
    % last transition is the accumulation of the 2nd protein (NUP358)
    % N1 is the position where the first protein is present
    % X_1 -k1> X_2 ... -k2> X_(N1+1) -k3> X_(N1+2) ... -kN-1> X_(N)
    % sum X_(i=N1)_end: protein1
    %     X_(N): protein2
    % paramters are
    %   k1-kN-1 (postmitotic); k1-kN-1 (interphase); kd; vproduction;
    %   fraction_maturePore_noncore; fraction_maturePore_core;
    %   scaling_factor_protein1_NC;scaling_factor_protein2_NC;scaling_factor_protein1_Core;
    %   scaling_factor_protein2_Core;
    % Author: Antonio Politi, EMBL August 2016
    methods
        function MO = poreMaturationTotalProdDeg(N, N1, tpm, ti, force, showplot)
            MO@absPoreMaturationMultiPar(N, N1, tpm, ti,force, showplot)
            MO.protF = MO.protA;
            for i=1:2
                MO.protF(i).core = MO.protA(i).tot_core;
                MO.protF(i).noncore = MO.protA(i).tot_noncore;
            end
            MO.parnames = '';
            
            for i=1:MO.N-1
               MO.parnames = [MO.parnames 'kpm' num2str(i) ' '];
            end
            for i=1:MO.N-1
               MO.parnames = [MO.parnames 'kip' num2str(i) ' '];
            end
            MO.parnames = [MO.parnames 'kd v fMP_NC fMP_C scal_prot1_NC scal_prot2_NC scal_prot1_C scal_prot2_C'];
            
            MO.LB = [zeros(1,2*MO.N) 0.9 0.385 0.8 0.8 0.8 0.8];
            MO.HB = [10*ones(1,2*(MO.N-1)) 0.1 0.1 0.94 0.617 1.2 1.2 1.2 1.2 ];
            MO.name = mfilename('class');
        end
        
        function setTimeStart(MO, tpm, ti)
            % set time of start of assembly of postmitotic and interphase
            assert(length(tpm) > 1)
            assert(length(ti)  > 1)
            MO.tpm = round(tpm);
            MO.ti = round(ti);
        end
        
        function [prot] = computeProteins(MO, par, iprot)
            %% compute distance model to data
            % par: par(1) - k1 for postmitotic
            %      par(2) - k2 for postmitotic
            %      par(3) - k1 for interphase
            %      par(4) - k2 for interphase
            %      par(5) - fraction postmitotic in non-core region
            %      par(6) - fraction postmitotic in core region
            %      par(7) - coefficient scaling non-core protein 1
            %      par(8) - coefficient scaling non-core protein 2
            %      par(9) - coefficient scaling core protein 1
            %      par(10) - coefficient scaling core protein 2
            %      par(11) - v, production of intermediates
            %      par(12) - kd, degradation of mature pores
            
            %experimental data has 1 min time resolution
            smoothedArea = fnval(MO.protF(1).surfspl, MO.protF(1).time');
            PM = MO.solveModelN([par(1:MO.N-1) par(2*MO.N-1) par(2*MO.N)], MO.timeI,1);
            IP = MO.solveModelN([par(MO.N:2*(MO.N-1)) par(2*MO.N-1) par(2*MO.N)], MO.timeI,1);
            %compute solution for specific time
            PM_t = deval(PM, MO.timeI);
            IP_t = deval(IP, MO.timeI);
            
            %% compute protein amounts in non-core and core regions
            tend = MO.protF(1).time(end);
            iend = MO.protF(1).time(end) + 1;
            baseN = 2*MO.N+1;
            
            % total amount post-mitotic non-core region
            p1NC_pm = [zeros(1, MO.tpm(1)-4) sum(PM_t(MO.N1:end,1:iend-MO.tpm(1)))]*par(baseN)*par(baseN+2);
            % total amount interphase non-core region
            p1NC_ip = [zeros(1, MO.ti(1)-4)  sum(IP_t(MO.N1:end,1:iend-MO.ti(1)))]*(1-par(baseN))*par(baseN+2);
            p1NC = (p1NC_pm+p1NC_ip);
            p1NC_den = p1NC./smoothedArea*max(smoothedArea)/par(baseN+2);
            
            p2NC_pm = [zeros(1,MO.tpm(1)-4) PM_t(end,1:iend-MO.tpm(1))]*par(baseN)*par(baseN+3);
            p2NC_ip = [zeros(1,MO.ti(1)-4) IP_t(end,1:iend-MO.ti(1))]*(1-par(baseN))*par(baseN+3);
            p2NC = (p2NC_pm + p2NC_ip);
            p2NC_den = p2NC./smoothedArea*max(smoothedArea)/par(baseN+3);
            
            p1C_pm = [zeros(1,MO.tpm(2)-4) sum(PM_t(MO.N1:end,1:iend-MO.tpm(2)))]*par(baseN+1)*par(baseN+4);
            p1C_ip = [zeros(1,MO.ti(2)-4) , sum(IP_t(MO.N1:end,1:iend-MO.ti(2)))]*(1-par(baseN+1))*par(baseN+4);
            p1C_ip_den =  p1C_ip/(1-par(baseN+1))/par(baseN+4)./smoothedArea*max(smoothedArea);
            %contains no protein 1 and protein 2 core region PM and
            %interphase
            pC_nop1p2 = [zeros(1,MO.tpm(2)-4) sum(PM_t(1:MO.N1-1,1:iend-MO.tpm(2)),1)]*par(baseN+1)+ ...
            [zeros(1,MO.ti(2)-4) , sum(IP_t(1:MO.N1-1,1:iend-MO.ti(2)),1)]*(1-par(baseN+1));
            %contains no protein 1 and protein 2 core region, density
            pC_nop1p2_den =  pC_nop1p2./smoothedArea*max(smoothedArea);
            %contains no protein 1,2 core region
            pC_ip_nop1p2 = [zeros(1,MO.ti(2)-4) , sum(IP_t(1:MO.N1-1,1:iend-MO.ti(2)),1)]*(1-par(baseN+1))
            %contains no protein 1,2 core region
            pC_ip_nop1p2_den =  pC_ip_nop1p2./smoothedArea*max(smoothedArea);
            
            %contains only p1 but nop2
            p1C_nop2 = [zeros(1,MO.tpm(2)-4) sum(PM_t(MO.N1:end-1,1:iend-MO.tpm(2)),1)]*par(baseN+1)+ ...
            [zeros(1,MO.ti(2)-4) , sum(IP_t(MO.N1:end-1,1:iend-MO.ti(2)),1)]*(1-par(baseN+1));
            %contains only p1 but no p2, interphase
            p1C_ip_nop2 = [zeros(1,MO.ti(2)-4) , sum(IP_t(MO.N1:end-1,1:iend-MO.ti(2)),1)]*(1-par(baseN+1));
            p1C_nop2_den =  p1C_nop2./smoothedArea*max(smoothedArea);
            p1C_ip_nop2_den = p1C_ip_nop2./smoothedArea*max(smoothedArea);
            p1C = (p1C_pm + p1C_ip);
            p1C_den = p1C./smoothedArea*max(smoothedArea)/par(baseN+4);
            
            
            
            p2C_pm = [zeros(1,MO.tpm(2)-4) PM_t(end,1:iend-MO.tpm(2))]*par(baseN+1)*par(baseN+5);
            p2C_ip =  [zeros(1,MO.ti(2)-4) IP_t(end,1:iend-MO.ti(2))]*(1-par(baseN+1))*par(baseN+5);
            p2C_ip_den = p2C_ip/(1-par(baseN+1))/par(baseN+5)./smoothedArea*max(smoothedArea);
            p2C = (p2C_pm + p2C_ip);
            
            p2C_den = p2C./smoothedArea*max(smoothedArea)/par(baseN+5);
            
            
            %densities
            
            
            p1 = struct('time_NC', [4:tend],'time_C', [4:tend], 'NC', p1NC, 'NC_pm', p1NC_pm, 'NC_ip', p1NC_ip,...
                'C', p1C, 'C_pm', p1C_pm, 'C_ip', p1C_ip, 'C_pm_pure', p1C_pm/par(baseN+1)/par(baseN+4), 'C_ip_pure', p1C_ip/(1-par(baseN+1))/par(baseN+4),...
                'C_ip_den', p1C_ip_den, 'C_ip_den_nop2', p1C_ip_nop2_den, 'C_ip_den_nop1p2',pC_ip_nop1p2_den,  'C_den', p1C_den, 'C_den_nop2',  p1C_nop2_den, 'C_den_nop1p2',pC_nop1p2_den );
            p2 = struct('time_NC',[4:tend],'time_C',[4:tend], 'NC', p2NC, 'NC_pm', p2NC_pm, 'NC_ip', p2NC_ip, ...
                'C', p2C, 'C_pm', p2C_pm, 'C_ip', p2C_ip, 'C_pm_pure', p2C_pm/par(baseN+1)/par(baseN+5), 'C_ip_pure', p2C_ip/(1-par(baseN+1))/par(baseN+5), ...
                'C_ip_den', p2C_ip_den, 'C_ip_den_nop2', p1C_ip_nop2_den,  'C_ip_den_nop1p2',pC_ip_nop1p2_den, 'C_den', p2C_den,  'C_den_nop2',  p1C_nop2_den, 'C_den_nop1p2',pC_nop1p2_den );
            
            prot = [p1;p2];
            %%
            
        end
        
        
        function M = modelMatrix(MO, par)
            %% Generate matrix for multi step maturation process N1 steps with rate equal k1 and N2 steps with rate equal k2
            % k1 set the rate for formation of protein1 containing complex
            % k2 set the rate of intermediate steps until protein2 is
            % integrated
            % X_1 -k1> X_2 ... -k1> X_(N1+1) -k2> X_(N1+2) ... -k2> X_(N1+N2+1)
            M1 = diag(-1*ones(1,MO.N));
            M2 = diag(ones(1,MO.N),-1);
            M = M1*diag(par(1:end-1)) + M2(1:end-1, 1:end-1)*diag(par(1:end-1));
        end
        
        function dydt = model(MO, t, y, par, iprot)
            %% model to integrate
            Ar = fnval(MO.protF(1).surfspl, t);
            M = MO.modelMatrix(par);
            dydt = M*y;
            if t > MO.ti(iprot)
                dydt = dydt + Ar*par(end)*ones(MO.N,1);
            end
        end
        
    end
end

