% %% collections of function calls to run the fits and compute confidence intervals
%
% %% ratio mature to total in  inner core density-minipore-to-Antonio-CoreRegion-mean-sd.xlsx at 19.2 min
% % compute its distrubution if both processes are random processes
% % mature pore
% m = 4.3;
% v = 2.97^2;
% mu = log((m^2)/sqrt(v+m^2));
% sigma = sqrt(log(v/(m^2)+1));
% X = lognrnd(mu,sigma,1,1e6);
%
% %minipore
% m = 3.7;
% v = 1.19^2;
% mu = log((m^2)/sqrt(v+m^2));
% sigma = sqrt(log(v/(m^2)+1));
% Y = lognrnd(mu,sigma,1,1e6);
%
% ratio = X./(X+Y);
% [prctile(ratio, 50) prctile(ratio, 25) prctile(ratio, 75)]
%
% % ratio 0.385 - 0.617
% % in elife used 0.5
% %% ratio mature to total in  outer core 160229-EMdata-density-precursor.xlsx at 19.2 min
% m = 11.3;
% v = 2.3^2;
% mu = log((m^2)/sqrt(v+m^2));
% sigma = sqrt(log(v/(m^2)+1));
% X = lognrnd(mu,sigma,1,1e6);
%
% %minipore
% m = 0.95;
% v = 0.36^2;
% mu = log((m^2)/sqrt(v+m^2));
% sigma = sqrt(log(v/(m^2)+1));
% Y = lognrnd(mu,sigma,1,1e6);
%
% ratio = X./(X+Y);
% [prctile(ratio, 50) prctile(ratio, 25) prctile(ratio, 75)]

% this yields 0.90-0.94
%+-0.031 for the ratio mature/totalpores in inner-core
% in elife used 0.92
%% Initialization
outdir = 'C:\Users\toni\Dropbox\NPCMaturation\results\fitnups_2018';
tpm = [4 4];
% time begin interphase assembly for Non-core and core region
tip = [10 10];
hb = [10 10 10 10 1 1 1.2 1.2 1.2 1.2 ];
lb = [0  0  0  0  0  0  0 0 0 0];
force = 0;
showplot = 0;
dataUse = 2018;
clear('pM')
opti  = optimset('Display', 'iter', 'MaxIter', 100);

%% fit all parameters

pM = poreMaturationTotal(1, 1, tpm, tip, force, showplot, dataUse);
solB = [0.3549    0.0449    0.0435    0.0492    0.7    0.3    1    1    1    1];
ifit = [1:10];
% Use Seh1 as read out
for i = 1:8
    iprot = [6 i];
    
    for rndfit = 1
        %iv = solB;
        iv = hb.*rand(1,length(hb));
        solB = iv;
        iv = iv(ifit);
        [sol_con_loc(rndfit,:), norm_con_loc(rndfit)] = lsqnonlin(@pM.distData, iv, lb(ifit), hb(ifit), opti, ifit, iprot, solB);
    end
    [val, pos] = min(norm_con_loc);
    sol_con(i,:) = sol_con_loc(pos, :);
    norm_con(i) = norm_con_loc(pos);
    save(fullfile(outdir, 'allpar.mat'))
end

%%
pM.showGraph(sol_con_fixpar(1,:), [6 1]);

%% Compute mean of pm to ip fraction in core and non-core (skip TPR)
frac = mean(sol_con([1:6 8], [5 6]))
k1_pm_seh1 =  mean(sol_con([1:6 8], [1]))
k1_ip_seh1 =  mean(sol_con([1:6 8], [3]))
%%
%% fit data obtained for total number of proteins
pM = poreMaturationTotal(1, 1, tpm, tip, 0, 0, 2018);
clear('sol_con_loc', 'sol_con_fixpar', 'norm_con_loc')
ifit = [2 4 7:10];
if exist('sol_Tot')
    iv = sol_Tot;% hb.*rand(1,10);
else
    iv = hb.*rand(1,length(hb));
end
for i = 1:8
    iprot = [6 i];
    
    for rndfit = 1:5
        iv = hb.*rand(1,length(hb));
        solB = [k1_pm_seh1 iv(2) k1_ip_seh1 iv(4) frac iv(7:10)];
        iv = iv(ifit);
        [sol_con_loc(rndfit,:), norm_con_loc(rndfit)] = lsqnonlin(@pM.distData, iv, lb(ifit), hb(ifit), opti, ifit, iprot, solB);
    end
    [val, pos] = min(norm_con_loc);
    sol_con_fixpar(i,:) = pM.getparset(sol_con_loc(pos,:), ifit, solB);
    
    norm_con_fixpar(i) = norm_con_loc(pos);
    save(fullfile(outdir, 'fixpar.mat'));
end


%% fit all parameters put Seh1 at the end

pM = poreMaturationTotal(1, 1, tpm, tip, force, showplot, dataUse);
solB = [0.3549    0.0449    0.0435    0.0492    0.7    0.3    1    1    1    1];
ifit = [1:10];
% Use Seh1 as read out
for i = 1:8
    iprot = [i 6];
    
    for rndfit = 1:10
        %iv = solB;
        iv = hb.*rand(1,length(hb));
        solB = iv;
        iv = iv(ifit);
        [sol_con_loc_Seh1last(rndfit,:), norm_con_loc_Seh1last(rndfit)] = lsqnonlin(@pM.distData, iv, lb(ifit), hb(ifit), opti, ifit, iprot, solB);
    end
    [val, pos] = min(norm_con_loc_Seh1last);
    sol_con_Seh1last(i,:) = pM.getparset(sol_con_loc_Seh1last(pos,:), ifit, solB);
    
    norm_con_Seh1last(i) = norm_con_loc_Seh1last(pos);
    save(fullfile(outdir, 'allpar_Seh1last.mat'))
end



%% create outputs
pM = poreMaturationTotal(1, 1, tpm, tip, force, showplot, dataUse);
midname = 'fixedpar';
sol_out = sol_con_fixpar;
%midname = 'allpar';
%sol_out = sol_con;
for iprot = 1:8
    
    prot = pM.computeProteins(sol_out(iprot,:), [6 iprot])
    %fid(1) = fopen(fullfile(outdir,['simulated_' midname '_' pM.protF(6).name '.csv']), 'w')
    fid(2) = fopen(fullfile(outdir,['simulated_' midname '_' pM.protF(iprot).name '.csv']), 'w')
    
    try
        for ifile = 2
            fprintf(fid(ifile), '%% Parameters k1_pm k2_pm k1_ip k2_ip NC_frac_pm  C_frac_pm  factProd_NC_p1 factPro_NC_p2  factProd_C_p1 factPro_C_p2 (only the first 4 parameters are fixed)\n');
            for ipar = 1:length(sol_out(iprot,:))
                fprintf(fid(ifile),'%.4f ', sol_out(iprot,ipar));
            end
            fprintf(fid(ifile), '\n');
            fprintf(fid(ifile), 'tpm = [%d %d], tip = [%d %d], N1 = %d, N2 = %d\n', tpm(1), tpm(2), tip(1), tip(2), 1, 1);
            fprintf(fid(ifile), 'time\tNC\tNC_pm\tNC_ip\tC\tC_pm\tC_ip\tC_ip_pure\tC_ip_density\n');
            for it = 1:length(prot(ifile).time_C)
                try
                    fprintf(fid(ifile), '%d\t%.3e\t%.3e\t%.3e\t%.3e\t%.3e\t%.3e\t%.3e\t%.3e\n', ...
                        prot(ifile).time_NC(it), prot(ifile).NC(it), prot(ifile).NC_pm(it), prot(ifile).NC_ip(it),  ...
                        prot(ifile).C(it), prot(ifile).C_pm(it), prot(ifile).C_ip(it),  prot(ifile).C_ip_pure(it), prot(ifile).C_ip_den(it));
                catch
                    fprintf(fid, '%d\t%.3e\t%.3e\n', p1.time_NC(ifile), p1.NC_pm(ifile), p1.NC_ip(ifile), p1.NC_ip(ifile));
                end
            end
            fclose(fid(ifile));
        end
    catch
        fclose(fid(ifile));
    end
end

%%


