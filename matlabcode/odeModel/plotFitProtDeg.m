%% collections of function calls to run the fits and compute confidence intervals



% time begin interphase assembly for Non-core and core region
clear;
clear all;
fclose all
tip = [10 10];
tpm = [4 4];
nrfits = 10;
outval = [];

%%
for N=3
    for N1 = 2%:N-1
        
        MO = poreMaturationTotalProdDeg(N,N1, tpm, tip, 0, 0);
        par = [1*ones(1,2*(N-1)) 0 0 0.92    0.5    1    1    1    1];
%         ifit = [1:2*(N-1) 2*N+1:2*N+6];
%         fname = ['poreMatuLiveMultiParFractionN_' num2str(N) 'N1_' num2str(N1) '.txt'];
%         MO.runFit(par, ifit, nrfits, 0.5, fname);
%         parf = MO.getBestfit(fullfile(MO.outdir, fname));
%          MO.showGraph(parf,[1 2], 1, fullfile(MO.outdir, fname) );
        ifit = [1:2*(N-1) 2*N+3:2*N+6];
        fname = ['poreMatuLiveMultiParN_' num2str(N) 'N1_' num2str(N1) '.txt'];
        
        MO.runFit(par, ifit, nrfits, 0.5, fname)
        
        parf = MO.getBestfit(fullfile(MO.outdir, fname));
        MO.writeDensities( parf, ['poreMatuLiveMultiParN_' num2str(N) 'N1_' num2str(N1)])
         MO.showGraph(parf,[1 2], 1, fullfile(MO.outdir, fname) );
        outval = [outval; [N, N1, parf(end), length(ifit)]];
    end
end


%%
clear;
clear all;
fclose all
tip = [10 10];
tpm = [4 4];
nrfits = 10;
for N=3:10
    for N1 = 2:N-1
        MO = poreMaturationTotalProdDeg(N,N1, tpm, tip, 0, 0);
        par = [1*ones(1,2*(N-1)) 4.200e-04 4.2e-4  0.92   0.5    1    1    1    1];
        ifit = [1:2*N 2*N+1:2*N+6];
        MO.runFit(par, ifit, nrfits, 0.5, ['poreMatuLiveMultiParFractionProdDegN_' num2str(N) 'N1_' num2str(N1) '.txt'])
        ifit = [1:2*N 2*N+3:2*N+6];
        MO.runFit(par, ifit, nrfits, 0.5, ['poreMatuLiveMultiParProdDegN_' num2str(N) 'N1_' num2str(N1) '.txt'])
    end
end