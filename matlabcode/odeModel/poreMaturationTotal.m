classdef poreMaturationTotal < absPoreMaturation
    %% Multi step maturation process N1 steps with rate equal k1 and N2 steps with rate equal k2
    % Compute total number of pores so no need to use the surface area
    % k1 set the rate for formation of protein1 containing complex
    % k2 set the rate of intermediate steps until protein2 is
    % integrated
    % X_1 -k1> X_2 ... -k1> X_(N1+1) -k2> X_(N1+2) ... -k2> X_(N1+N2+1)
    % sum X_(i=N1+1)_end: protein1
    %     X_(N1+N2+1): protein2
    methods 
        function MO = poreMaturationTotal(N1, N2, tpm, ti, force, showplot, date)
            MO@absPoreMaturation(N1, N2, tpm, ti, force, showplot, date)
            MO.protF = MO.protA;
%             %invert order
%             for i=1:2
%                 MO.protF(3-i).core = MO.protA(i).tot_core;
%                 MO.protF(3-i).noncore = MO.protA(i).tot_noncore;
%             end
            
           for i = 1:length(MO.protA)
                MO.protF(i).core = MO.protA(i).tot_core;
                MO.protF(i).noncore = MO.protA(i).tot_noncore;
            end
        end
          
        function setTimeStart(MO, tpm, ti)
            % set time of start of assembly of postmitotic and interphase 
            assert(length(tpm) > 1)
            assert(length(ti)  > 1)
            MO.tpm = round(tpm);
            MO.ti = round(ti);
        end
        
        function [prot] = computeProteins(MO, par, iprot)
            %% compute distance model to data
            % par: par(1) - k1 for postmitotic
            %      par(2) - k2 for postmitotic
            %      par(3) - k1 for interphase
            %      par(4) - k2 for interphase
            %      par(5) - fraction postmitotic in non-core region
            %      par(6) - fraction postmitotic in core region
            %      par(7) - coefficient scaling non-core protein 1
            %      par(8) - coefficient scaling non-core protein 2
            %      par(9) - coefficient scaling core protein 1
            %      par(10) - coefficient scaling core protein 2


            %experimental data has 1 min time resolution
            try
                smoothedArea = fnval(MO.protF(1).surfspl, MO.protF(1).time');
            catch
                smoothedArea = 1;
            end
            PM = MO.solveModelN([par(1) par(2)], MO.timeI, 1); 
            IP = MO.solveModelN([par(3) par(4)], MO.timeI, 1);

            %compute solution for specific time
            for i = 1:2
                baseN = 5;
                %evaluate at same time point as data 
                PM_t = deval(PM, MO.protF(iprot(i)).time - MO.protF(iprot(i)).time(1));
                IP_t = deval(IP, MO.protF(iprot(i)).time - MO.protF(iprot(i)).time(1));
                % time step
                dt = MO.protF(iprot(i)).time(2) - MO.protF(iprot(i)).time(1);
                iend = size(PM_t,2);
                pend = size(PM_t,1);
                if i == 1
                    prot_sum = MO.N1+1:pend;
                end
                if i == 2
                    prot_sum = pend;
                end
                % non-core region
                zeropad_pm = 0*[MO.protF(iprot(i)).time(1):dt:MO.tpm(1)];
                zeropad_ip = 0*[MO.protF(iprot(i)).time(1):dt:MO.ti(1)];
                pNC_pm = [zeropad_pm sum(PM_t(prot_sum, 1:iend-length(zeropad_pm)),1)]*par(baseN)*par(baseN+2);
                pNC_ip = [zeropad_ip sum(IP_t(prot_sum, 1:iend-length(zeropad_ip)),1)]*(1-par(baseN))*par(baseN+2);
                pNC = (pNC_pm+pNC_ip);
                
                % core region
                zeropad_pm = 0*[MO.protF(iprot(i)).time(1):dt:MO.tpm(2)];
                zeropad_ip = 0*[MO.protF(iprot(i)).time(1):dt:MO.ti(2)];
                pC_pm = [zeropad_pm sum(PM_t(prot_sum,1:iend-length(zeropad_pm)),1)]*par(baseN+1)*par(baseN+4);
                pC_ip = [zeropad_ip sum(IP_t(prot_sum,1:iend-length(zeropad_ip)),1)]*(1-par(baseN+1))*par(baseN+4);
                pC_ip_den = pC_ip/(1-par(baseN+1))/par(baseN+4)./smoothedArea*max(smoothedArea);
                pC = (pC_pm + pC_ip);
                pC_den = pC./smoothedArea;
                protl = struct('time_NC', MO.protF(iprot(i)).time,'time_C', MO.protF(iprot(i)).time, ...
                        'NC', pNC, 'NC_pm', pNC_pm, 'NC_ip', pNC_ip,...
                    'C', pC, 'C_pm', pC_pm, 'C_ip', pC_ip, ...
                    'C_pm_pure', pC_pm/par(baseN+1)/par(baseN+4), 'C_ip_pure', pC_ip/(1-par(baseN+1))/par(baseN+4), ...
                    'C_ip_den', pC_ip_den);
                if ~exist('prot', 'var')
                    prot = protl;
                else
                    prot = [prot; protl]';
                end
            end
        end
        
        
        function A = modelMatrix(MO, par)
        %% Generate matrix for multi step maturation process N1 steps with rate equal k1 and N2 steps with rate equal k2
        % k1 set the rate for formation of protein1 containing complex
        % k2 set the rate of intermediate steps until protein2 is
        % integrated
        % X_1 -k1> X_2 ... -k1> X_(N1+1) -k2> X_(N1+2) ... -k2> X_(N1+N2+1)
            k1 = par(1);
            k2 = par(2);
            vrate = [k1*ones(MO.N1,1); k2*ones(MO.N2,1); 0];
            A1 = diag(-vrate,0);
            A2 = diag(vrate,-1);
            A = A2(1:end-1,1:end-1)+A1;
        end
        
        function dydt = model(MO, t, y, par, iprot)
        %% model to integrate 
            A = MO.modelMatrix(par);
            dydt = A*y;    
        end
        
     end
end

