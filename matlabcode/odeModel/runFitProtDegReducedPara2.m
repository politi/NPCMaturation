%% collections of function calls to run the fits and compute confidence intervals



% time begin interphase assembly for Non-core and core region
clear;
clear all;
fclose all
tip = [10 10];
tpm = [4 4];
nrfits = 3;
for N = 3:10
    for N1 = 2:N-1
        
        MO = poreMaturationReducedPara2(N,N1, tpm, tip, 0, 0);
        par = [0.5*ones(1,N+1) 0 0 0.92    0.5    1    1    1    1];
        ifit = [1:N+1 N+4:N+9];
        fname = ['poreMatuLiveReducedPara2FractionN_' num2str(N) 'N1_' num2str(N1) '.txt'];
        MO.runFit(par, ifit, nrfits, 0.5, fname);
        MO.showGraph(MO.getBestfit(fullfile(MO.outdir, fname)), [1 2], 1, fullfile(MO.outdir, fname));
        
      
        ifit = [1:N+1 N+6:N+9];
        fname =  ['poreMatuLiveReducedPara2N_' num2str(N) 'N1_' num2str(N1) '.txt'];
        MO.runFit(par, ifit, nrfits, 0.5, fname);
        MO.showGraph(MO.getBestfit(fullfile(MO.outdir, fname)), [1 2], 1, fullfile(MO.outdir, fname));
        
        
    end
end


%%
clear;
clear all;
fclose all
tip = [10 10];
tpm = [4 4];
nrfits = 10;
for N=3:10
    for N1 = 2:N-1
        MO = poreMaturationReducedPara(N,N1, tpm, tip, 0, 0);
        par = [0.5*ones(1,N+1) 4.200e-04 4.2e-4  0.92   0.5    1    1    1    1];
        ifit = [1:N+3 N+4:N+9];
        fname =  ['poreMatuLiveReducedPara2FractionProdDegN_' num2str(N) 'N1_' num2str(N1) '.txt'];
        MO.runFit(par, ifit, nrfits, 0.5,fname);
        
        MO.showGraph(MO.getBestfit(fullfile(MO.outdir, fname)), [1 2], 1, fullfile(MO.outdir, fname));
        
        ifit = [1:N+3 N+6:N+9];
        fname =  ['poreMatuLiveReducedPara2ProdDegN_' num2str(N) 'N1_' num2str(N1) '.txt'];
        
        MO.runFit(par, ifit, nrfits, 0.5, fname)
        
        MO.showGraph(MO.getBestfit(fullfile(MO.outdir, fname)), [1 2], 1, fullfile(MO.outdir, fname));
    end
end