%% collections of function calls to run the fits and compute confidence intervals



% time begin interphase assembly for Non-core and core region
clear;
clear all;
fclose all
tip = [10 10];
tpm = [4 4];
nrfits = 10;
for N = 4:10
    for N1 = 2:N-1
        
        MO = poreMaturationReducedPara(N,N1, tpm, tip, 0, 0);
        par = [0.5*ones(1,4) 0 0 0.92    0.5    1    1    1    1];
        ifit = [1:4 7:12];
        fname = ['poreMatuLiveReducedParaFractionN_' num2str(N) 'N1_' num2str(N1) '.txt'];
        MO.runFit(par, ifit, nrfits, 0.5, fname);
        MO.showGraph(MO.getBestfit(fullfile(MO.outdir, fname)), [1 2], 1, fullfile(MO.outdir, fname));
        
      
        ifit = [1:4 9:12];
        fname =  ['poreMatuLiveReducedParaN_' num2str(N) 'N1_' num2str(N1) '.txt'];
        MO.runFit(par, ifit, nrfits, 0.5, fname);
        MO.showGraph(MO.getBestfit(fullfile(MO.outdir, fname)), [1 2], 1, fullfile(MO.outdir, fname));
        
        
    end
end


%%
clear;
clear all;
fclose all
tip = [10 10];
tpm = [4 4];
nrfits = 10;
for N=3:10
    for N1 = 2:N-1
        MO = poreMaturationReducedPara(N,N1, tpm, tip, 0, 0);
        par = [0.5*ones(1,4) 4.200e-04 4.2e-4  0.92   0.5    1    1    1    1];
        ifit = [1:12];
        fname =  ['poreMatuLiveReducedParaFractionN_' num2str(N) 'N1_' num2str(N1) '.txt'];
        MO.runFit(par, ifit, nrfits, 0.5,fname);
        
        MO.showGraph(MO.getBestfit(fullfile(MO.outdir, fname)), [1 2], 1, fullfile(MO.outdir, fname));
        
        ifit = [1:6 9:12];
        fname =  ['poreMatuLiveReducedParaProdDegN_' num2str(N) 'N1_' num2str(N1) '.txt'];
        
        MO.runFit(par, ifit, nrfits, 0.5, fname)
        
        MO.showGraph(MO.getBestfit(fullfile(MO.outdir, fname)), [1 2], 1, fullfile(MO.outdir, fname));
    end
end