%% collections of function calls to run the fits and compute confidence intervals

%% ratio mature to total in  inner core density-minipore-to-Antonio-CoreRegion-mean-sd.xlsx at 19.2 min
% compute its distrubution if both processes are random processes
% mature pore
m = 4.3;
v = 2.97^2;
mu = log((m^2)/sqrt(v+m^2));
sigma = sqrt(log(v/(m^2)+1));
X = lognrnd(mu,sigma,1,1e6);

%minipore
m = 3.7;
v = 1.19^2;
mu = log((m^2)/sqrt(v+m^2));
sigma = sqrt(log(v/(m^2)+1));
Y = lognrnd(mu,sigma,1,1e6);

ratio = X./(X+Y);
[prctile(ratio, 50) prctile(ratio, 25) prctile(ratio, 75)]

% ratio 0.385 - 0.617
%% ratio mature to total in  outer core 160229-EMdata-density-precursor.xlsx at 19.2 min
m = 11.3;
v = 2.3^2;
mu = log((m^2)/sqrt(v+m^2));
sigma = sqrt(log(v/(m^2)+1));
X = lognrnd(mu,sigma,1,1e6);

%minipore
m = 0.95;
v = 0.36^2;
mu = log((m^2)/sqrt(v+m^2));
sigma = sqrt(log(v/(m^2)+1));
Y = lognrnd(mu,sigma,1,1e6);

ratio = X./(X+Y);
[prctile(ratio, 50) prctile(ratio, 25) prctile(ratio, 75)]

% this yields 0.90-0.94
%+-0.031 for the ratio mature/totalpores in inner-core

%%

tpm = [4 4];
% time begin interphase assembly for Non-core and core region
tip = [10 10];
hb = [10 10 10 10 1.2 1.2 1.2 1.2 ];
lb = [0  0  0  0 0 0 0 0];
pM = poreMaturationTotal(1,1, tpm, tip, 0, 0);
%unconstrained
%hb = [10 10 10 10  1  0.617  10 10 10 10];
%lb = [0  0  0  0   0.8  0.385   1 1 1 1];

opti  = optimset('Display', 'iter');

%% fit data obtained for total number of proteins
clear('pM')
pM = poreMaturationTotal(1,1, tpm, tip, 0, 0);
if exist('sol_Tot')
    iv = sol_Tot;% hb.*rand(1,10);
else
    iv = hb.*rand(1,length(hb));
end

iv = [0.3549    0.0449    0.0435    0.0492 1 1 1 1];
solB = [0.3549    0.0449    0.0435    0.0492    0.92    0.5    1    1    1    1];
ifit = [1:4 7:10];
for i=1
    [sol_con{i}, norm_con(i)] = lsqnonlin(@pM.distData, iv, lb, hb, opti, ifit, solB);
end
solB = pM.getparset(sol_con{1}, ifit, solB);
pM.showGraph(solB, [2 1]);

%% compute confidence interval
tpm = [4 4];
tip = [10 10];
hb = [10 10 10 10 ];
lb = [0  0  0  0];
pM = poreMaturationTotal(1,1, tpm, tip, 0, 0);
filenames = {fullfile(pM.outdir, 'k1_pm_CI.txt'), fullfile(pM.outdir,'k2_pm_CI.txt'), fullfile(pM.outdir,'k1_ip_CI.txt'), fullfile(pM.outdir,'k2_ip_CI.txt')};
findCI_poreMaturationx(pM, filenames);

%%
filenames = {fullfile(pM.outdir, 'k1_pm_CI_coeffMax.txt'), fullfile(pM.outdir,'k2_pm_CI_coeffMax.txt'), ...
    fullfile(pM.outdir,'k1_ip_CI_coeffMax.txt'), fullfile(pM.outdir,'k2_ip_CI_coeffMax.txt')};
findCI_poreMaturation_coeffMax(pM, filenames);

%% create output
prot = pM.computeProteins(solB, [1 2])

%%
fid(1) = fopen(fullfile(pM.outdir,'simulated_NUP107.csv'), 'w')
fid(2) = fopen(fullfile(pM.outdir,'simulated_NUP358.csv'), 'w')
try
for i=1:2
    fprintf(fid(i), '%% Parameters k1_pm k2_pm k1_ip k2_ip NC_frac_pm  C_frac_pm  factProd_NC_p1 factPro_NC_p2  factProd_C_p1 factPro_C_p2 (only the first 4 parameters are fixed)\n');
    for ipar=1:length(solB)
        fprintf(fid(i),'%.4f ', solB(ipar));
    end
    fprintf(fid(i), '\n');
    fprintf(fid(i), 'tpm = [%d %d], tip = [%d %d], N1 = %d, N2 = %d\n', tpm(1), tpm(2), tip(1), tip(2), 1, 1);
    fprintf(fid(i), 'time\tNC\tNC_pm\tNC_ip\tC\tC_pm\tC_ip\tC_ip_pure\tC_ip_density\n');
    for it = 1:length(prot(i).time_C)
        try
            fprintf(fid(i), '%d\t%.3e\t%.3e\t%.3e\t%.3e\t%.3e\t%.3e\t%.3e\t%.3e\n', prot(i).time_NC(it), prot(i).NC(it), prot(i).NC_pm(it), prot(i).NC_ip(it),  ...
                 prot(i).C(it), prot(i).C_pm(it), prot(i).C_ip(it),  prot(i).C_ip_pure(it), prot(i).C_ip_den(it));
        catch
            fprintf(fid, '%d\t%.3e\t%.3e\n', p1.time_NC(i), p1.NC_pm(i), p1.NC_ip(i), p1.NC_ip(i));
        end
    end
    fclose(fid(i));
end
catch
     fclose(fid(i));
end
%%
fid= fopen(fullfile(pM.indir,'HeLa4D.csv'), 'w');
fprintf(fid, 'time\tNUP107_NC\tNUP107_NC_std\tNUP107_C\tNUP107_C_std\tNUP358_NC\tNUP358_NC_std\tNUP358_C\tNUP358_C_std\n');
for i=1:length(pM.protF(1).time)
    fprintf(fid,'%d\t%.3e\t%.3e\t%.3e\t%.3e\t%.3e\t%.3e\t%.3e\t%.3e\n',  pM.protF(1).time(i),  pM.protF(1).noncore(i,1),pM.protF(1).noncore(i,2), ...
         pM.protF(1).core(i,1),pM.protF(1).core(i,2),  pM.protF(2).noncore(i,1),pM.protF(2).noncore(i,2), ...
         pM.protF(2).core(i,1),pM.protF(2).core(i,2));
end
fclose(fid)