%%
% find confidence Interval using likelihood profile method for ode model.
% The scaling coefficients are varied
% pM: Class of model
% filenames: 4 filenames containing parameter values
function findCI_poreMaturation_coeffMax(pM,  filenames)
    try
        k1_pm = load(filenames{1});
        k2_pm = load(filenames{2});
        k1_ip = load(filenames{3});
        k2_ip = load(filenames{4});
    catch
        %priming
        tpm = [4 4];
        % time begin interphase assembly for Non-core and core region
        tip = [10 10];
        hb = [2 2 2 2 2 2 2 2];
        lb = [0  0  0  0 0 0 0 0];
        opti  = optimset('Display', 'iter');
        solB = [0.3549    0.0449    0.0435    0.0492    0.92    0.5    1    1    1    1]; %bset solution
        ifit = [1 2 3 4 7:10]; %parameters to fit
        for i=1:4
            iv = hb.*rand(1,length(hb))
            [parFit{i}, norm(i)] = lsqnonlin(@pM.distData,iv, lb, hb, opti, ifit, solB);
        end
        normB = min(norm); 
        ibf = find(norm == normB);
        solB = pM.getparset(parFit{ibf}, ifit, solB);
        
        %% vary each one parameter 
        minP = 0.2;
        fitParCI = {[2 3 4 7:10], [1 3 4 7:10], [1 2 4 7:10], [1 2 3 7:10]};
        step_p = 100;
       
        for i=1:4
            hb = [2 2 2 1.2 1.2 1.2 1.2];
            lb = [0  0  0 0 0 0 0];
            fidloc = fopen(filenames{i}, 'w');
            fprintf(fidloc, '%%k1_pm \t k2_pm \t k1_ip \t k2_ip \t norm \t normBest\n');
            ifit = fitParCI{i};
            solLoc = solB;
            parVar = solB(i)*10.^((-minP+minP*2*[1:step_p]/step_p));
            for j = 1:step_p
                solLoc(i) = parVar(j);
                iv = solLoc(ifit);
                [parFit, norm] = lsqnonlin(@pM.distData,iv, lb, hb, opti, ifit, solLoc);
                sol = pM.getparset(parFit, ifit, solLoc);
                fprintf(fidloc, '%.3e \t %.3e \t %.3e \t %.3e \t %.3e \t %.3e \t %.3e \t %.3e \t %.3e \t %.3e \n', sol(1), sol(2), sol(3), ...
                    sol(4), sol(7), sol(8), sol(9), sol(10), norm, normB);
            end
            fclose(fidloc);
        end
        k1_pm = load(filenames{1});
        k2_pm = load(filenames{2});
        k1_ip = load(filenames{3});
        k2_ip = load(filenames{4});
    end
    
    %%
    NrDataPts = size(pM.protF(1).core,1)+size(pM.protF(2).core,1) + size(pM.protF(1).noncore,1)+size(pM.protF(2).noncore,1);
    parList = {k1_pm, k2_pm, k1_ip, k2_ip};
    for i=1:4
        par = parList{i};
        CI = par(:,i).*((abs(NrDataPts*log(par(:,end-1)/NrDataPts) - NrDataPts*log(par(:,end)/NrDataPts)))<chi2inv(0.95,1));
        CIL(i,:) = [par(50, i) min(CI(find(CI>0))) max(CI(find(CI>0)))];
    end
CIL
end

% %systematically change tauM and kM with fixed parameters (do not refit) 
% function findCI(par, fileout1, fileout2)
%     try
%         kM = load(fileout1);
%         tauM = load(fileout2);     
%     catch     
%         stepi = 50;
%         stepj = 50;
%         parloc = par;
%         data = getData(density, 0, 0);
%         %options = odeset('MaxStep', 0.1);
%         clear('normFit')
%         minTMv = 0.2;
%         minkMV = 1.6;
%         
%         tauMv = par.tauM*10.^((-minTMv+minTMv*2*[1:stepi]/stepi));
%         kMv =  par.kM*10.^((-minkMV + minkMV*2*[1:stepj]/stepj));
%         tofit_names_loc = {'tauM', 'P01', 'M01', 'P02', 'M02'};
%         normO = sum(distModel(getfields(par, tofit_names_loc), tofit_names_loc, par, data, options).^2);
%         
%         tofit_names_loc = {'tauM', 'P01', 'M01', 'P02', 'M02'};
%         fidloc = fopen(fileout1, 'w');
%         fprintf(fidloc, '%%kM \t tauM \t P01 \t M01 \t P02 \t M02 \t norm \t normBest\n');
%         parloc = par;
%         for idx = 1:stepj
%                 parloc.tauM = par.tauM;
%                 parloc.kM = kMv(idx);
%                 [parfit_kM, norm_kM(idx)] = performFit(tofit_names_loc, lbStruct, hbStruct, parloc, 5, 0.1)
%                 parloc = setfields(parloc, tofit_names_loc, parfit_kM);
%                 fprintf(fidloc, '%.3e \t %.3e \t %.3e \t %.3e \t %.3e \t %.3e \t %.3e \t %.3e\n', parloc.kM, parloc.tauM, parloc.P01, parloc.M01, parloc.P02, parloc.M02, norm_kM(idx), normO);
%         end
%         fclose(fidloc);
%          
%         tofit_names_loc = {'kM', 'P01', 'M01', 'P02', 'M02'};
%         fidloc = fopen(fileout2, 'w');
%         fprintf(fidloc, '%%kM \t tauM \t P01 \t M01 \t P02 \t M02 \t norm \t normBest\n');
%         parloc = par;
%         for idx = 1:31
%                 parloc.tauM = tauMv(idx);
%                 parloc.kM = 2;
%                 [parfit_tauM, norm_tauM(idx)] = performFit(tofit_names_loc, lbStruct, hbStruct, parloc, 5, 0.1)
%                 parloc = setfields(parloc, tofit_names_loc, parfit_tauM);
%                 fprintf(fidloc, '%.3e \t %.3e \t %.3e \t %.3e \t %.3e \t %.3e \t %.3e \t %.3e\n', parloc.kM, parloc.tauM, parloc.P01, parloc.M01, parloc.P02, parloc.M02, norm_tauM(idx), normO);
%         end
%         fclose(fidloc);
%     end
% %%
%     data = getData(density, 0, 0);
%     NrDataPts = (size(data,1)-1)/2*size(data,2);
%     CIkM = kM(:,1).*((abs(NrDataPts*log(kM(:,end-1)/NrDataPts) - NrDataPts*log(kM(:,end)/NrDataPts)))<chi2inv(0.95,1));
%     CIkM = [min(CIkM(find(CIkM>0))) max(CIkM(find(CIkM>0)))]
%     CItauM = tauM(:,2).*((abs(NrDataPts*log(tauM(:,end-1)/NrDataPts) - NrDataPts*log(tauM(:,end)/NrDataPts)))<chi2inv(0.95,1));
%     CItauM = [min(CItauM(find(CItauM>0))) max(CItauM(find(CItauM>0)))]
% end