classdef poreMaturationReducedPara2 < absPoreMaturationMultiPar
    %% Multi step maturation process. N1 steps with rate equal k1 and N2 steps with rate equal k2 for 
    %% postmitotic. Multiple rates for interphase
    % Compute total number of pores so no need to use the surface area
    % k1-kN-1 are the transition rates for interphase. 
    % For postmitotic same as poreMaturationTotal, only 2 rate constants. 
    % k_{1}-k_{N1} = k1, k_{N1+1}-k_{N-1} = k2.
    % Last transition is the accumulation of the 2nd protein (NUP358)
    % N1 is the position where the first protein binds
    % X_1 -k1> X_2 ... -k2> X_(N1+1) -k3> X_(N1+2) ... -kN-1> X_(N)
    % sum X_(i=N1)_end: protein1
    %     X_(N): protein2
    % Parameters; 
    %   k1, k2 (postmitotic); k1, kN-1 (interphase); kd; vproduction;
    %   fraction_maturePore_noncore; fraction_maturePore_core;
    %   scaling_factor_protein1_NC;scaling_factor_protein2_NC;scaling_factor_protein1_Core;
    %   scaling_factor_protein2_Core;
    
    methods
        function MO = poreMaturationReducedPara2(N, N1, tpm, ti, force, showplot)
            MO@absPoreMaturationMultiPar(N, N1, tpm, ti,force, showplot)
            MO.protF = MO.protA;
            for i=1:2
                MO.protF(i).core = MO.protA(i).tot_core;
                MO.protF(i).noncore = MO.protA(i).tot_noncore;
            end
            MO.parnames = '';
            % k1 and k2 for postmitotic
            for i=1:2
               MO.parnames = [MO.parnames 'kpm' num2str(i) ' '];
            end
            
            % k1 to k_{N-1} for interphase
            for i=1:MO.N-1
               MO.parnames = [MO.parnames 'kip' num2str(i) ' '];
            end
            MO.parnames = [MO.parnames 'kd v fMP_NC fMP_C scal_prot1_NC scal_prot2_NC scal_prot1_C scal_prot2_C'];
            
            MO.LB = [zeros(1,MO.N+1)  0 0 0.9 0.385 0.8 0.8 0.8 0.8];
            MO.HB = [10*ones(1,MO.N+1) 0.1 0.1 0.94 0.617 1.2 1.2 1.2 1.2 ];
            MO.name = mfilename('class');
        end
        
        function setTimeStart(MO, tpm, ti)
            % set time of start of assembly of postmitotic and interphase
            assert(length(tpm) > 1)
            assert(length(ti)  > 1)
            MO.tpm = round(tpm);
            MO.ti = round(ti);
        end
        
        function [prot] = computeProteins(MO, par, iprot)
            %% compute distance model to data
            % par: par(1) - k1 for postmitotic
            %      par(2) - k2 for postmitotic
            %      par(3) - k1 for interphase
            %      par(4) - k2 for interphase
            %      par(5) - fraction postmitotic in non-core region
            %      par(6) - fraction postmitotic in core region
            %      par(7) - coefficient scaling non-core protein 1
            %      par(8) - coefficient scaling non-core protein 2
            %      par(9) - coefficient scaling core protein 1
            %      par(10) - coefficient scaling core protein 2
            %      par(11) - v, production of intermediates
            %      par(12) - kd, degradation of mature pores
            
            %experimental data has 1 min time resolution
            smoothedArea = fnval(MO.protF(1).surfspl, MO.protF(1).time');
            Nloc = MO.N;
            N1loc = MO.N1;


            parloc = [par(1) par(2) par(MO.N+2) par(MO.N+3)];
            MO.N = 3;
            MO.N1 = 2;
            PM = MO.solveModelN(parloc, MO.timeI,1);
            MO.N = Nloc;
            MO.N1 = N1loc;
            IP = MO.solveModelN([par(3:MO.N+1) par(MO.N+2) par(MO.N+3)], MO.timeI,1);
            %compute solution for specific time
            PM_t = deval(PM, MO.timeI);
            IP_t = deval(IP, MO.timeI);
            
            %% compute protein amounts in non-core and core regions
            baseN = MO.N+4;
            tend = MO.protF(1).time(end);
            iend = MO.protF(1).time(end) + 1;
            p1NC_pm = [zeros(1, MO.tpm(1)-4) sum(PM_t(2:end,1:iend-MO.tpm(1)))]*par(baseN)*par(baseN+2);
            p1NC_ip = [zeros(1, MO.ti(1)-4)  sum(IP_t(MO.N1:end,1:iend-MO.ti(1)))]*(1-par(baseN))*par(baseN+2);
            p1NC = (p1NC_pm+p1NC_ip);
            p1NC_den = p1NC./smoothedArea;
            
            p2NC_pm = [zeros(1,MO.tpm(1)-4) PM_t(end,1:iend-MO.tpm(1))]*par(baseN)*par(baseN+3);
            p2NC_ip = [zeros(1,MO.ti(1)-4) IP_t(end,1:iend-MO.ti(1))]*(1-par(baseN))*par(baseN+3);
            p2NC = (p2NC_pm + p2NC_ip);
            p2NC_den = p2NC./smoothedArea;
            
            p1C_pm = [zeros(1,MO.tpm(2)-4) sum(PM_t(2:end,1:iend-MO.tpm(2)))]*par(baseN+1)*par(baseN+4);
            p1C_ip = [zeros(1,MO.ti(2)-4) , sum(IP_t(MO.N1:end,1:iend-MO.ti(2)))]*(1-par(baseN+1))*par(baseN+4);
            p1C_ip_den = p1C_ip/(1-par(baseN+1))/par(baseN+4)./smoothedArea*max(smoothedArea);
            p1C = (p1C_pm + p1C_ip);
            p1C_den = p1C./smoothedArea;
            
            
            
            p2C_pm = [zeros(1,MO.tpm(2)-4) PM_t(end,1:iend-MO.tpm(2))]*par(baseN+1)*par(baseN+5);
            p2C_ip =  [zeros(1,MO.ti(2)-4) IP_t(end,1:iend-MO.ti(2))]*(1-par(baseN+1))*par(baseN+5);
            p2C_ip_den = p2C_ip/(1-par(baseN+1))/par(baseN+5)./smoothedArea*max(smoothedArea);
            p2C = (p2C_pm+ p2C_ip);
            p2C_den = p2C./smoothedArea;
            
            
            %densities
            
            
            p1 = struct('time_NC', [4:tend],'time_C', [4:tend], 'NC', p1NC, 'NC_pm', p1NC_pm, 'NC_ip', p1NC_ip,...
                'C', p1C, 'C_pm', p1C_pm, 'C_ip', p1C_ip, 'C_pm_pure', p1C_pm/par(6)/par(9), 'C_ip_pure', p1C_ip/(1-par(6))/par(9), 'C_ip_den', p1C_ip_den);
            p2 = struct('time_NC',[4:tend],'time_C',[4:tend], 'NC', p2NC, 'NC_pm', p2NC_pm, 'NC_ip', p2NC_ip, ...
                'C', p2C, 'C_pm', p2C_pm, 'C_ip', p2C_ip, 'C_pm_pure', p2C_pm/par(6)/par(10), 'C_ip_pure', p2C_ip/(1-par(6))/par(10), 'C_ip_den', p2C_ip_den);
            
            prot = [p1;p2];
            %%
            
        end
        
        
        function M = modelMatrix(MO, par)
            %% Generate matrix for multi step maturation process N1 steps with rate equal k1 and N2 steps with rate equal k2
            % k1 set the rate for formation of protein1 containing complex
            % k2 set the rate of intermediate steps until protein2 is
            % integrated
            % X_1 -k1> X_2 ... -k1> X_(N1+1) -k2> X_(N1+2) ... -k2> X_(N1+N2+1)
            M1 = diag(-1*ones(1,MO.N));
            M2 = diag(ones(1,MO.N),-1);
            M = M1*diag(par(1:end-1)) + M2(1:end-1, 1:end-1)*diag(par(1:end-1));
        end
        
        function dydt = model(MO, t, y, par, iprot)
            %% model to integrate
            
            Ar = fnval(MO.protF(1).surfspl, t);
            M = MO.modelMatrix(par);
            dydt = M*y;
            if t > MO.ti(iprot)
                dydt = dydt + Ar*par(end)*ones(MO.N,1);
            end
        end
        
    end
end

