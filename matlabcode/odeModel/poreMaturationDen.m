classdef poreMaturationDen < absPoreMaturation
    %% Multi step maturation process N1 steps with rate equal k1 and N2 steps with rate equal k2
    % Use surface area to compute density
    % k1 set the rate for formation of protein1 containing complex
    % k2 set the rate of intermediate steps until protein2 is
    % integrated
    % X_1 -k1> X_2 ... -k1> X_(N1+1) -k2> X_(N1+2) ... -k2> X_(N1+N2+1)
    % sum X_(i=N1+1)_end: protein1
    %     X_(N1+N2+1): protein2
    methods 
        function MO = poreMaturationDen(N1, N2, tpm, ti, force, showplot)
            MO@absPoreMaturation(N1, N2, tpm, ti,force, showplot)
            MO.protF = MO.protA;
            for i=1:2
                MO.protF(i).core = MO.protA(i).d_core;
                MO.protF(i).noncore = MO.protA(i).d_noncore;
            end
        end
          
        function setTimeStart(MO, tpm, ti)
            % set time of start of assembly of postmitotic and interphase 
            assert(length(tpm) > 1)
            assert(length(ti)  > 1)
            MO.tpm = round(tpm);
            MO.ti = round(ti);
        end
        
        function [prot] = computeProteins(MO, par,iprot, varargin)
            %% compute distance model to data
            % par: par(1) - k1 for postmitotic
            %      par(2) - k2 for postmitotic
            %      par(3) - k1 for interphase
            %      par(4) - k2 for interphase
            %      par(5) - fraction postmitotic in non-core region
            %      par(6) - fraction postmitotic in core region
            %      par(7) - coefficient scaling non-core protein 1
            %      par(8) - coefficient scaling non-core protein 2
            %      par(9) - coefficient scaling core protein 1
            %      par(10) - coefficient scaling core protein 2
            if nargin <= 3
                tend = 125;
            else
                tend = varargin{1};
            end
            %experimental data has 1 min time resolution
            p1_NC_pm = MO.solveModelN([par(1) par(2)], [MO.tpm(1) tend],iprot(1));
            p1_NC_ip = MO.solveModelN([par(3) par(4)], [MO.ti(1) tend],iprot(1));
            
            p2_NC_pm = MO.solveModelN([par(1) par(2)], [MO.tpm(1) tend],iprot(2));
            p2_NC_ip = MO.solveModelN([par(3) par(4)], [MO.ti(1) tend],iprot(2));
            
            p1_C_pm = MO.solveModelN([par(1) par(2)], [MO.tpm(2) tend],iprot(1));
            p1_C_ip = MO.solveModelN([par(3) par(4)], [MO.ti(2) tend],iprot(1));
            
            p2_C_pm = MO.solveModelN([par(1) par(2)], [MO.tpm(2) tend],iprot(2));
            p2_C_ip = MO.solveModelN([par(3) par(4)], [MO.ti(2) tend],iprot(2));
            
            pm = deval(p1_NC_pm,[MO.tpm(1):1:tend]);
            ip = deval(p1_NC_ip,[MO.ti(1):1:tend]);
            p1NC_pm = [zeros(1,MO.tpm(1)-4) sum(pm(MO.N1+1:end,:))]*par(5)*par(7);
            p1NC_ip = [zeros(1,MO.ti(1)-4) sum(ip(MO.N1+1:end,:))]*(1-par(5))*par(7);
            p1NC = (p1NC_pm+p1NC_ip);
            
            pm = deval(p2_NC_pm,[MO.tpm(1):1:tend]);
            ip = deval(p2_NC_ip,[MO.ti(1):1:tend]);
            p2NC_pm = [zeros(1,MO.tpm(1)-4) pm(end,:)]*par(5)*par(8);
            p2NC_ip = [zeros(1,MO.ti(1)-4) ip(end,:)]*(1-par(5))*par(8);
            p2NC = (p2NC_pm + p2NC_ip);
            
            pm = deval(p1_C_pm,[MO.tpm(2):1:tend]);
            ip = deval(p1_C_ip,[MO.ti(2):1:tend]);
            p1C_pm = [zeros(1,MO.tpm(2)-4) sum(pm(MO.N1+1:end,:))]*par(6)*par(9);
            p1C_ip = [zeros(1,MO.ti(2)-4) sum(ip(MO.N1+1:end,:))]*(1-par(6))*par(9);
            p1C = (p1C_pm + p1C_ip);
            
        
            pm = deval(p2_C_pm,[MO.tpm(2):1:tend]);
            ip = deval(p2_C_ip,[MO.ti(2):1:tend]);
            p2C_pm = [zeros(1,MO.tpm(2)-4) pm(end,:)]*par(6)*par(10);
            p2C_ip = [zeros(1,MO.ti(2)-4) ip(end,:)]*(1-par(6))*par(10);
            p2C = (p2C_pm + p2C_ip);
            
            p1 = struct('time_NC', [4:tend],'time_C', [4:tend], 'NC', p1NC, 'NC_pm', p1NC_pm, 'NC_ip', p1NC_ip,...
                'C', p1C, 'C_pm', p1C_pm, 'C_ip', p1C_ip, 'C_pm_pure', p1C_pm/par(6)/par(9), 'C_ip_pure', p1C_ip/(1-par(6))/par(9));
            p2 = struct('time_NC',[4:tend],'time_C',[4:tend], 'NC', p2NC, 'NC_pm', p2NC_pm, 'NC_ip', p2NC_ip, ...
                'C', p2C, 'C_pm', p2C_pm, 'C_ip', p2C_ip, 'C_pm_pure', p2C_pm/par(6)/par(10), 'C_ip_pure', p2C_ip/(1-par(6))/par(10));

            prot = [p1;p2];
        end
        
       function [idxm, idxd]  = findIdxs(MO)
           idxm = [1 1];
           idxd = [1 1];
       end
        
        function A = modelMatrix(MO, par)
        %% Generate matrix for multi step maturation process N1 steps with rate equal k1 and N2 steps with rate equal k2
        % k1 set the rate for formation of protein1 containing complex
        % k2 set the rate of intermediate steps until protein2 is
        % integrated
        % X_1 -k1> X_2 ... -k1> X_(N1+1) -k2> X_(N1+2) ... -k2> X_(N1+N2+1)
            k1 = par(1);
            k2 = par(2);
            vrate = [k1*ones(MO.N1,1); k2*ones(MO.N2,1); 0];
            A1 = diag(-vrate,0);
            A2 = diag(vrate,-1);
            A = A2(1:end-1,1:end-1)+A1;
        end
        
        function dydt = model(MO, t, y, par, iprot)
        %% model to integrate 
            A = MO.modelMatrix(par);
            areaDeg = fnval(MO.protF(iprot).dsurfspl,t)/fnval(MO.protF(iprot).surfspl,t);
            if isnan(areaDeg)
                areaDeg = fnval(MO.protF(iprot).dsurfspl,125)/fnval(MO.protF(iprot).surfspl,125);
            end
                
            dydt = A*y - y*areaDeg;   
        end
        
        
       function dist  = distData(MO,parfit, ifit, par)
            %% calculate distance model to data
            prot = MO.computeProteins(par, [1 2]);
            % if useStd == 0 do not divide by standard deviation
            useStd = 0;
            distNC = [];
            distC = [];
            for i=1:2
                if useStd
                    distNC =  [distNC (MO.protF(i).noncore(:,1)' - prot(i).NC)./MO.protF(i).noncore(:,2)'];
                    distC =  [distC (MO.protF(i).core(:,1)' - prot(i).C)./MO.protF(i).core(:,2)'];
                else
                    distNC =  [distNC MO.protF(i).noncore(:,1)' - prot(i).NC];
                    distC =  [distC MO.protF(i).core(:,1)' - prot(i).C];
                end
            end
            dist = [distNC distC];
        end
     end
end

