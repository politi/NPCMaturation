%% ABSPOREMATURATION
%  Abstact class for model of pore maturation. 
%  Multi step process but with same rate constant. N1 steps rate constant k1 and N2 steps rate constant k2
classdef absPoreMaturation < handle
    properties (Access = protected)
        timeI = [0 150]; %intergration time points
        useStd = 1; % if == 1 scale by std
        protA; %data structure containing array of all data 
        tpm;  %time of postmitotic start for non-core NC and Core region  [time_start_pm_NC time_start_pm_C]
        ti; %time interphase assembly start  [time_start_ip_NC time_start_ip_C]
    end
    
    properties 
        N1; %number of steps controlled by rate constant k1
        N2; %number of steps controlled by rate constant k2
        indir;
        outdir;
        protF; %data structure containing data to be fitted
    end
    
    methods (Abstract)
        A = modelMatrix(MO,par);
        dydt = model(MO, par);
        prot = computeProteins(MO, par, iprot)
    end
    
    methods 
        function MO = absPoreMaturation(N1, N2, tpm, ti, force, showplot, date)
            if nargin < 5
                force = 0;
            end
            if nargin < 6
                showplot = 0;
            end
            if nargin < 7
                date = 2018;
            end
            MO.setTimeStart(tpm, ti);
            MO.N1 = N1;
            MO.N2 = N2;
            if date == 2016
                MO.protA = MO.dataIn_2016(force, showplot);
            end
            if date == 2018
                MO.protA = MO.dataIn_2018(force, showplot);
            end
        end
          
        function setTimeStart(MO, tpm, ti)
            % set time of start of assembly of postmitotic and interphase 
            assert(size(tpm) > 1)
            assert(size(ti)  > 1)
            MO.tpm = round(tpm);
            MO.ti = round(ti);
        end
        function [idxm, idxd]  = findIdxs(MO)
            idxm = [1 1];
            idxd = [1 1];
%             for i=1:2
%                 it_tmp = find(MO.protF(1).time == MO.tpm(i));
%                 idxm(i) = 1;
%                 if isempty(it_tmp)
%                     idxm(i) =  find(MO.timeI(MO.tpm(i)+1:end) == MO.protF(1).time(1));
%                     idxd(i) = 1;
%                 else
%                     idxd(i) = it_tmp;
%                 end
%             end
        end
        
        function parloc = getparset(MO, parfit, ifit, par)
            parloc = par;
            parloc(ifit) = parfit;
        end
        
        function dist  = distData(MO, parfit, ifit, iprot, par)
            %% calculate distance model to data
            %   parfit - vector of parameters that will be fitted
            %   ifit   - index of parameters in vector par that will be fitted. par(ifit) = parfit;
            %   iprot  - index of proteins to fit. length(iprot) == 2
            %   par    - vector of parameters
            if length(iprot) ~= 2 || any(iprot > length(MO.protF)) || any(iprot < 1)
                error('index of proteins must be a vector of length 2, with values > 0 and < %d', length(MO.protF));
            end
            parloc = MO.getparset(parfit, ifit, par);
            prot = MO.computeProteins(parloc, iprot);
            % if useStd == 0 do not divide by standard deviation
            [idxm, idxd] = MO.findIdxs();
            distNC = [];
            distC = [];
            for i = 1:2
                if MO.useStd
                    distNC =  [distNC (MO.protF(iprot(i)).noncore(idxd(1):end,1)' - prot(i).NC(idxm(1):end))./MO.protF(iprot(i)).noncore(idxd(1):end,2)'];
                    distC =  [distC (MO.protF(iprot(i)).core(idxd(2):end,1)' - prot(i).C(idxm(2):end))./MO.protF(iprot(i)).core(idxd(2):end,2)'];
                else
                    distNC =  [distNC MO.protF(i).noncore(idxd(1):end,1)' - prot(i).NC(idxm(1):end)];
                    distC =  [distC MO.protF(i).core(idxd(2):end,1)' - prot(i).C(idxm(2):end)];
                end
            end
            dist = [distNC distC];
            %dist = [distNC;
        end
        
        %%
        % function to create a graph of the data
        function showGraph(MO, par, iprot)
            prot = MO.computeProteins(par,  iprot);
            [idxm, idxd] = MO.findIdxs()
            for i = 1:2
                setupFigure(i, [200 200 500 700]);
                clf
                subplot(3,1,1)
                title(MO.protF(iprot(i)).name)
                hold

                plot(MO.protF(iprot(i)).time, MO.protF(iprot(i)).noncore(:,1), 'bo');
                plot(MO.protF(iprot(i)).time, MO.protF(iprot(i)).core(:,1), 'go');
                plot(MO.protF(iprot(i)).time(idxd(1):end), prot(i).NC(idxm(1):end), 'c--');
                plot(MO.protF(iprot(i)).time(idxd(2):end), prot(i).C(idxm(2):end), 'r--');
                ylabel('Relative fluorescence')
                legend('Non-core data', 'core data', 'Non-Core model', 'core model')
                ylim([0 1.2])

                subplot(3,1,2)
                hold
                plot(MO.protF(iprot(i)).time, MO.protF(iprot(i)).noncore(:,1), 'bo');
                plot(MO.protF(iprot(i)).time(idxd(1):end), prot(i).NC_pm(idxm(1):end)', 'b--');
                plot(MO.protF(iprot(i)).time(idxd(1):end), prot(i).NC_ip(idxm(1):end)', 'b--', 'LineWidth', 2);
                ylabel('Relative fluorescence')
                legend('Non-Core data', 'Non-Core pm', 'Non-Core ip')
                ylim([0 1.2])
                
                subplot(3,1,3)
                hold
                plot(MO.protF(iprot(i)).time, MO.protF(iprot(i)).core(:,1), 'go');
                plot(MO.protF(iprot(i)).time(idxd(2):end), prot(i).C_pm(idxm(2):end)', 'r--');
                plot(MO.protF(iprot(i)).time(idxd(2):end), prot(i).C_ip(idxm(2):end)', 'r--', 'LineWidth', 2);
                legend('Core data', 'Core pm', 'Core ip')
                ylabel('Relative fluorescence')
                xlabel('Time (min)')
                ylim([0 1.2])
            end
        end
        
        function [A, dAdt, kg] = getSurf(MO, time)
                A = fnval(MO.protF(1).surfspl, time);
                dAdt = fnval(MO.protF(1).dsurfspl, time);
                kg = dAdt./A;
        end
        
        function y = solveModelN(MO, par, time, iprot)
            y = ode45(@MO.model,time,[1;zeros(MO.N1+MO.N2,1)],  odeset, par, iprot);
        end
        
        %%
        % read in the experimental data 
        %
        function protA = dataIn_2016(MO, force, showplot)
            if nargin < 1
                force = 0;
            end
            if nargin < 2
                showplot = 1;
            end
            
            if ispc
                indir = fullfile([getenv('HOMEDRIVE') getenv('HOMEPATH') ], 'Dropbox', 'Shotaro_PoreMaturation',...
                    'ExpData', 'LiveCellImaging');
                outdir = fullfile([getenv('HOMEDRIVE') getenv('HOMEPATH') ], 'Dropbox', 'Shotaro_PoreMaturation',...
                    'Simulations', 'LiveCellImaging');
            end
            
            if ismac
                indir = fullfile(getenv('HOME'), 'Dropbox', 'Shotaro_PoreMaturation',...
                    'ExpData', 'LiveCellImaging');
                outdir = fullfile(getenv('HOME'), 'Dropbox', 'Shotaro_PoreMaturation',...
                    'Simulations', 'LiveCellImaging');
            end
            
            MO.indir = indir;
            MO.outdir = outdir;
            matfile = fullfile(indir, 'livecelldata_2016.mat');
            
            %load matfile if matfile exists or reading is not forced
            if exist(matfile) && ~force
                load(matfile)
                MO.timeI = [0:protA(1).time(end)]
                return
            end
            
            % struct containing the experimental data
            prot = struct('name', 'Nup107', 'time',[], 'd_core',[], 'd_noncore', [], ...
            'd_core_cells', [],'d_noncore_cells', [], 'tot_core', [], 'tot_noncore', [])

            protA = [prot;prot]
            protA(2).name = 'Nup358';
            
            % location of XLS files
            xlsfile = fullfile(indir, 'HeLa4D_2016.xls');
            sheet = {'Nup107_AVG', 'Nup358_AVG'};
            
            %% Read Nup107 data
            protA(1).d_core_cells = xlsread(xlsfile, 'Nup107_AVG', 'C11:AF132');
            protA(1).d_noncore_cells = xlsread(xlsfile, 'Nup107_AVG', 'AH11:BK132');
            protA(1).d_core = [xlsread(xlsfile, 'Nup107_AVG', 'BN11:BN132') ...
                xlsread(xlsfile, 'Nup107_AVG', 'BP11:BP132')];
            protA(1).d_noncore = [xlsread(xlsfile, 'Nup107_AVG', 'BO11:BO132') ...
                xlsread(xlsfile, 'Nup107_AVG', 'BQ11:BQ132')];   
            protA(1).time = xlsread(xlsfile, 'Nup107_AVG', 'B11:B132');
            protA(1).tot_core = [xlsread(xlsfile, 'Nup107_total', 'BN11:BN132') xlsread(xlsfile, 'Nup107_total', 'BP11:BP132')];
            protA(1).tot_noncore = [xlsread(xlsfile, 'Nup107_total', 'BO11:BO132') xlsread(xlsfile, 'Nup107_total', 'BQ11:BQ132')];

            protA(2).d_core_cells = xlsread(xlsfile, 'Nup358_AVG', 'C11:AA132');
            protA(2).d_noncore_cells = xlsread(xlsfile, 'Nup358_AVG', 'AC11:BA132');
            protA(2).d_core = [xlsread(xlsfile, 'Nup358_AVG', 'BD11:BD132') ...
                xlsread(xlsfile, 'Nup358_AVG', 'BF11:BF132')];
            protA(2).d_noncore = [xlsread(xlsfile, 'Nup358_AVG', 'BE11:BE132') ...
                xlsread(xlsfile, 'Nup358_AVG', 'BG11:BG132')];   
            protA(2).time = xlsread(xlsfile, 'Nup358_total', 'B11:B132');
            protA(2).tot_core = [xlsread(xlsfile, 'Nup358_total', 'BD11:BD132') xlsread(xlsfile, 'Nup358_total', 'BF11:BF132')];
            protA(2).tot_noncore = [xlsread(xlsfile, 'Nup358_total', 'BE11:BE132') xlsread(xlsfile, 'Nup358_total', 'BG11:BG132')];
            
            %% read surface area
            protA(1).surf = xlsread(xlsfile, 'NUP107_area', 'AH11:AI132');
            protA(2).surf = xlsread(xlsfile, 'NUP358_area', 'AC11:AD132');
            % perform a spline fit to smooth the data
            protA(1).surfspl = surfSpline(protA(1).time, protA(1).surf(:,1),7, 3, 8);
            protA(2).surfspl = surfSpline(protA(2).time, protA(2).surf(:,1),7, 3, 8);
            % compute derivative of spline fit to be used when solving the
            % model that includes the area
            protA(1).dsurfspl = fnder(protA(1).surfspl, 1);
            protA(2).dsurfspl = fnder(protA(2).surfspl, 1);

            protA(1).tot_core2 =  protA(1).d_core.*repmat(fnval(protA(1).surfspl, protA(1).time),1,2);
            protA(1).tot_noncore2 =  protA(1).d_noncore.*repmat(fnval(protA(1).surfspl, protA(1).time),1,2);
            
            
            protA(2).tot_core2 =  protA(2).d_core.*repmat(fnval(protA(2).surfspl, protA(2).time),1,2);
            protA(2).tot_noncore2 =  protA(2).d_noncore.*repmat(fnval(protA(2).surfspl, protA(2).time),1,2);
            
            for i=1:2
                sP = sort(protA(i).tot_core2(:,1));
                protA(i).tot_core2 = protA(i).tot_core2/mean(sP(end-3:end));
                sP = sort(protA(i).tot_noncore2(:,1));
                protA(i).tot_noncore2 = protA(i).tot_noncore2/mean(sP(end-3:end));               
            end
            save(matfile, 'protA');
            %%
            if showplot
                figure(1)
                clf

                col = {'b', 'r'};
                for i=1:length(protA)
                subplot(1,2,i)
                hold
                errorbar(protA(i).time, protA(i).tot_core(:,1), protA(i).d_core(:,2), 'g')
                errorbar(protA(i).time, protA(i).tot_noncore(:,1), protA(i).tot_noncore(:,2), 'b')
                end
            end
        end
        
        %%
        % read in the experimental data new set
        %
        function protA = dataIn_2018(MO, force, showplot)
            if nargin < 1
                force = 0;
            end
            if nargin < 2
                showplot = 1;
            end
            
            if ispc
                indir = fullfile([getenv('HOMEDRIVE') getenv('HOMEPATH') ], 'Dropbox', 'Shotaro_PoreMaturation',...
                    'ExpData', 'LiveCellImaging');
                outdir = fullfile([getenv('HOMEDRIVE') getenv('HOMEPATH') ], 'Dropbox', 'Shotaro_PoreMaturation',...
                    'Simulations', 'LiveCellImaging');
            end
            
            if ismac
                indir = fullfile(getenv('HOME'), 'Dropbox', 'Shotaro_PoreMaturation',...
                    'ExpData', 'LiveCellImaging');
                outdir = fullfile(getenv('HOME'), 'Dropbox', 'Shotaro_PoreMaturation',...
                    'Simulations', 'LiveCellImaging');
            end
            
            MO.indir = indir;
            MO.outdir = outdir;
            matfile = fullfile(indir, 'livecelldata_2018.mat');
            
            %load matfile if matfile exists or reading is not forced
            if exist(matfile) && ~force
                load(matfile)
                MO.timeI = [0:protA(1).time(end)]
                return
            end
            
            % struct containing the experimental data. First column is avg, second column is sd
            prot = struct('name','Nup214', 'time',[], 'tot_core', [], 'tot_noncore', []);
            
            protA = repmat(prot, 8,1);
            protA(2).name = 'Nup358';
            protA(3).name = 'Nup107';
            protA(4).name = 'Nup205';
            protA(5).name = 'Nup62';
            protA(6).name = 'Seh1';
            protA(7).name = 'Tpr';
            protA(8).name = 'Nup153';
            

            
            
 
            % location of XLS files
            xlsfile = fullfile(indir, 'HeLa4D-core-noncore-normalized-summary-180104.xlsx');
            sheet = {'SUM'};
            tabl = readtable(xlsfile, 'Range', 'B16:AI249', 'ReadVariableNames', 0);
            for i=1:length(protA)
                idx = find(~isnan(tabl.(['Var' num2str(i*2)])));
                protA(i).time = tabl.Var1(idx);
                protA(i).tot_noncore = [tabl.(['Var' num2str(i*2)]) tabl.(['Var' num2str(i*2 + 1)])];
                protA(i).tot_core = [tabl.(['Var' num2str(i*2 +17)])  tabl.(['Var' num2str(i*2 + 18)])];
                protA(i).tot_core = protA(i).tot_core(idx, :);
                protA(i).tot_noncore = protA(i).tot_noncore(idx, :);
                
            end
            save(matfile, 'protA');
            %%
            if showplot
                figure(1)
                clf
                col = {'b', 'r'};
                for i=1:length(protA)
                    subplot(ceil(sqrt(length(protA))),round(sqrt(length(protA))),i)
                    hold
                    title(protA(i).name)
                    errorbar(protA(i).time, protA(i).tot_core(:,1), protA(i).tot_core(:,2), 'g')
                    errorbar(protA(i).time, protA(i).tot_noncore(:,1), protA(i).tot_noncore(:,2), 'b')
                    legend('core', 'non-core')
                end
            end
        end
    end
end

