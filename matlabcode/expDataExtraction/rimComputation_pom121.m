%%
% Use mask of rim and 3D segmented object to compute background and
% intensity
% maindir =  'C:\Users\toni\Dropbox\NPCMaturation\expdata\livecell_siRNA\images\Pom121KD\Experiment1\Pom121siRNA\LSMFiles';
% ind = [[1 6];[1 7]; [1 8];[2 6]];
% for i = 4
%     try
%         fname = fullfile(maindir, ['POM121KDLIVEIMAGING-' num2str(ind(i,1)) '_' 'Subset' num2str(ind(i,2)) '.lsm']);
%         hdf5converter_general(fname);
%     catch
%          fname = fullfile(maindir, ['POM121KDLIVEIMAGING-' num2str(ind(i,1)) '-' 'Subset' num2str(ind(i,2)) '.lsm']);
%         hdf5converter_general(fname);
%     end
% end

%% compute Pom121siRNAKD
ind = [[1 6];[1 7]; [1 8];[2 6]];
maindir =  'C:\Users\toni\Dropbox\NPCMaturation\expdata\livecell_siRNA\images\Pom121KD\Experiment1\Pom121siRNA\';
for i = 4
    segdir = fullfile(maindir,  'Results_2016_Aug_24');
    outdir = fullfile(maindir, 'BGComp');
    
    try
        lsmfile = fullfile(maindir, 'LSMFiles', ['POM121KDLIVEIMAGING-' num2str(ind(i,1)) '_' 'Subset' num2str(ind(i,2)) '.lsm']);
        [bg, img] =bgCompLiveCell(lsmfile, segdir, outdir);
    catch
        lsmfile = fullfile(maindir, 'LSMFiles', ['POM121KDLIVEIMAGING-' num2str(ind(i,1)) '-' 'Subset' num2str(ind(i,2)) '.lsm']);
        [bg, img] =bgCompLiveCell(lsmfile, segdir, outdir);
    end
    %%
    [path, name, ext] = fileparts(lsmfile);
    figure(1)
    subplot(2,1,1)
    
    plot([1:length(bg)], [smooth(bg(:,2),10) smooth(bg(:,4),10)]);
    xlabel('imaging frame')
    ylabel('cytoplasm fluorescence')
    legend('nucleus1', 'nucleus2')
    ylim([min(min(bg(:,[2 4]))) max(max(bg(:,[2 4])))])
    subplot(2,1,2)
    plot([1:length(bg)], [smooth(bg(:,1),10) smooth(bg(:,3),10)])
    ylim([min(min(bg(:,[1 3]))) max(max(bg(:,[1 3])))])
    ylabel('nucleoplasm fluorescence')
    xlabel('imaging frame')
    saveas(1, fullfile(outdir, [name '.jpg']), 'jpeg')
end


% %%
% maindir =  'C:\Users\toni\Dropbox\NPCMaturation\expdata\livecell_siRNA\images\Pom121KD\Experiment1\ScrambledsiRNA\LSMFiles';
% ind = [[1 1];[1 2]; [1 5];[2 1]];
% for i = 1:length(ind)
%     try
%         fname = fullfile(maindir, ['POM121KDLIVEIMAGING-' num2str(ind(i,1)) '_' 'Subset' num2str(ind(i,2)) '.lsm']);
%         hdf5converter_general(fname);
%     catch
%          fname = fullfile(maindir, ['POM121KDLIVEIMAGING-' num2str(ind(i,1)) '-' 'Subset' num2str(ind(i,2)) '.lsm']);
%         hdf5converter_general(fname);
%     end
% end


ind =[[1 1];[1 2]; [1 5];[2 1]];
maindir =  'C:\Users\toni\Dropbox\NPCMaturation\expdata\livecell_siRNA\images\Pom121KD\Experiment1\ScrambledsiRNA';
for i = 3:length(ind)
    segdir = fullfile(maindir,  'Results_2016_Aug_24');
    outdir = fullfile(maindir, 'BGComp');
    
    try
        lsmfile = fullfile(maindir, 'LSMFiles', ['POM121KDLIVEIMAGING-' num2str(ind(i,1)) '_' 'Subset' num2str(ind(i,2)) '.lsm']);
        [bg, img] =bgCompLiveCell(lsmfile, segdir, outdir);
    catch
        lsmfile = fullfile(maindir, 'LSMFiles', ['POM121KDLIVEIMAGING-' num2str(ind(i,1)) '-' 'Subset' num2str(ind(i,2)) '.lsm']);
        [bg, img] =bgCompLiveCell(lsmfile, segdir, outdir);
    end
    %%
    [path, name, ext] = fileparts(lsmfile);
    figure(1)
    subplot(2,1,1)
    
    plot([1:length(bg)], [smooth(bg(:,2),10) smooth(bg(:,4),10)]);
    xlabel('imaging frame')
    ylabel('cytoplasm fluorescence')
    legend('nucleus1', 'nucleus2')
    ylim([min(min(bg(:,[2 4]))) max(max(bg(:,[2 4])))])
    subplot(2,1,2)
    plot([1:length(bg)], [smooth(bg(:,1),10) smooth(bg(:,3),10)])
    ylim([min(min(bg(:,[1 3]))) max(max(bg(:,[1 3])))])
    ylabel('nucleoplasm fluorescence')
    xlabel('imaging frame')
    saveas(1, fullfile(outdir, [name '.jpg']), 'jpeg')
end