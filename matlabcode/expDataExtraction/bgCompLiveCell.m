function [bgVal, imgfinal] = bgCompLiveCell(imgfile, h5segdir, segdir, outdir, upfl, iC)
%%
% bgCompLiveCell(lsmfile, segdir, outdir)
%   - imgfile: full path to lsm file or h5 file
%   - h5segdir: path to files containing segementation from ilastik 1st channel is extracellular, 2nd
%   channel is nucleus, 3rd channel is cytoplasm
%   extrcellular
%   - segdir: path to J. Hossain segmentation mat files
%   - outdir: full path to directory containing h5 files of segmentation
%   from ilastik. ilastik file is labelled image with idx =1, outside cell,
%   idx = 2, nucleus; idx = 3, cytoplasm.
%   - upfl: [low-distribution-cutoff, high-distribution-cutoff]. Exclude
%   bright inclusions and oversegmentation parts. Default [0, 100]
%   - iC: channel of interest for POI
% compute bg in nucleus and cytoplasm using segmentation from ilastik and
% nucleus segmentation from Julius Hossain. Applied to live cell imaging of
% NUPs proteins
% Antonio Politi EMBL September 2016, December 2016 (changed to read hdf5
% too), May 2017 a threshold value for the distribution to compute average
% intensity
%%

global h5
%% input data and directories
if nargin < 1
    % imgfile can be a tif, lsm or h5
    imgfile = 'C:\Users\toni\Dropbox\NPCMaturation\expdata\livecellCalibrated\160706_gfpNUP107z26z31\Result\BGComp\h5\160706-HeLa107-4D-cell-5-t6.h5';
    %imgfile = 'W:\Shotaro\Live-cell-imaging\HeLa-FCS\160701-HeLa-Nup107\160701-HeLa-Nup107-cell-0-t6.tif';
end
if nargin < 2
    h5segdir = 'C:\Users\toni\Dropbox\NPCMaturation\expdata\livecellCalibrated\160706_gfpNUP107z26z31\Result\BGComp\h5_SimpleSegmentation';
end
if nargin < 3
    segdir = 'C:\Users\toni\Dropbox\NPCMaturation\expdata\livecellCalibrated\160706_gfpNUP107z26z31\Result\Segmentation\';
end
if nargin < 4
    outdir = 'C:\Users\toni\Dropbox\NPCMaturation\expdata\livecellCalibrated\160706_gfpNUP107z26z31\Result\BGComp\';
end

if nargin < 5
    upfl = [0 100];
end

if nargin < 6
    iC = 1;
end


[path, name, ext] = fileparts(imgfile);

if strcmp(ext,'.h5')
    h5 = 1;
else
    h5 = 0;
end

assert(exist(imgfile)>0, sprintf('bgCompLiveCell.m could not find image file %s', imgfile));
assert(exist(segdir) > 0,  sprintf('bgCompLiveCell.m  could not find segmentation directory %s\nCheck naming!', segdir));
assert(exist(h5segdir) > 0, sprintf('bgCompLiveCell.m  could not find h5dir directory %s\nCheck naming!', h5segdir));

segdir = fullfile(segdir, name, 'NucMask');
ilastfile = dir(fullfile(h5segdir, [name '_*']));
assert(length(ilastfile) == 1, ['More than one ilastik file found for ' imgfile]);
ilastfile = fullfile(h5segdir, ilastfile(1).name);
assert(exist(ilastfile)>0, ['bgCompLiveCell.m could not find ilastik file ' ilastfile]);
ilast = hdf5read(ilastfile, '/exported_data');

%% read in lsm file using loci
if h5
    reader = h5read(imgfile, '/volume/data');
    dim.Ny = size(reader,1);
    dim.Nx = size(reader,2);
    dim.Nz = size(reader,3);
    dim.Nt = size(reader,4);
    dim.Nc = size(reader,5);
    dim.voxelSize = [0.2471, 0.2471, 1];
else
    [reader, dim] = getBioformatsReader(imgfile);
end
%% Find central plane where object data has been obtained. From there we extact the background in the nucleus and cytoplasm. Exclude bright objects
bgVal = zeros(dim.Nt,6);        % contains bg value in nucleus and cytoplasm of nucleus 1 and 2. The index is obtained from mat file
imgfinal = zeros(dim.Ny, dim.Nx, dim.Nt); % imgfile containing labelled region of interest.
minArea = 100;
minSkel = 50;

mF = ones(10,10) / 100;
erodFil = zeros(3,3,3);
erodFil(:,:,1) = [0 0 0; 0 1 0; 0 0 0];
erodFil(:,:,2) = [0 1 0; 1 1 1; 0 1 0]; 
erodFil(:,:,3) = [0 0 0; 0 1 0; 0 0 0];
maxErosion = 3;
for iT = 1:dim.Nt

        try
            % the scripts adds a  digit code for time point. Default is 001
            load(fullfile(segdir, [name sprintf('%03d',iT)  '.mat']));
        catch
            display([name ' has not been segmented yet ']);
            return
        end
        display([name ' time point ' num2str(iT) '/' num2str(dim.Nt)])
        %% load mat file exclude small objects

        nucMaskNonIso = bwareaopen(nucMaskNonIso==1,minArea) +  bwareaopen(nucMaskNonIso==2,minArea)*2;

        % detect glass surface. This is a slice that has everywhere a higher
        % intensity in background
        for iZ = 1:dim.Nz
          gfpC = imfilter(getSlice(reader, iZ, iT, iC, dim), mF);
          sV = sort(gfpC(:));
          sumGfp(iZ) = mean(sV(1:20));        
        end
        [val, iZglass] = max(sumGfp);


        %% for each nuclei
        % check if indeed 2 nuclei have been segmented
        levels = unique(nucMaskNonIso);
        nucVec = 1:numNuc;
        if length(levels) < numNuc +1
            display('nuclei have not been segmented properly!')
            nucVec = max(levels);
        end
        for inuc = nucVec
            %% nucleus mask (J. Hossain segmentation)
            
            binNuc = nucMaskNonIso==inuc; 

            
            %% find slice with largest perimeter avoiding smallest segmented parts
            for iZ=1:dim.Nz
                props = regionprops(binNuc(:,:,iZ));
                try 
                    area(iZ) =  max(props.Area);
                catch
                    area(iZ) = 0;
                end
            end
            [val, iZ] = max(area);
            p = regionprops(binNuc);
            try 
                iZ = round(p.Centroid(3));
            catch
                iZ = round(p(1).Centroid(3));
            end
            %% avoid being too close to bottom of slide
            %iZn = iZ*(iZ >= (iZglass + 2)) + (iZglass+2)*(iZ< (iZglass + 2));
            iZn = iZ;
            % cytoplasmic slice is twice below nucleus slice
            %iZc = (iZ-2)*(iZ-2>=iZglass + 2) + (iZglass+2)*(iZ-2<iZglass + 2);
            iZc = iZ-1;

            %   find slice that contains center of mass (this is more prone to be sometime close to lower or upper part of nucleus
            %   props = regionprops(binNuc);
            %   iZ = round(props.Centroid(3))+1;
            
            %% find intensity inside 2D nucleus. Skeletonize nucleus and dilate skeleton 
            % use segmented nuclei instead 
            % get segmented slice. This is a labelled image
            gfpC = getSlice(reader, iZn, iT, iC, dim);
            gfpCf = imgaussian(gfpC,2);
            % find threshold value for low and high pixel use 1 eroded
            % nucleus
            gfpCM = gfpCf.*imerode(binNuc(:,:,iZn),  erodFil);
            hb = prctile(gfpCM(gfpCM > 0), upfl(2));
            lb = prctile(gfpCM(gfpCM > 0), upfl(1));
            % compute high and low boundary for the 
            gfpCM = (gfpCf  <= hb).*(gfpCf  >= lb);
            %nucIntM = skeleton((reshape(ilast(1,:,:,iZn,iT),dim.Ny, dim.Nx) ==2).*binNuc(:,:,iZn))> minSkel; %avoids problems in anaphase
            nucIntM = skeleton(imerode(binNuc(:,:,iZn),  erodFil).*gfpCM)> minSkel; %minSkel; %avoids problems in anaphase
            if sum(nucIntM(:)) == 0
                nucIntM = skeleton(binNuc(:,:,iZn).*gfpCM)> 1;
            end
            %nucIntM = skeleton(imfill(binNuc(:,:,iZn), 'holes'))>minSkel;
            % dilate once the mask of skeleton
            nucIntM = (gfpCM).*imdilate(nucIntM, strel('disk',1));
            nucInt = double(nucIntM).*double(gfpC);
            bgVal(iT, 1+(inuc-1)*3) = sum(nucInt(:))/sum(nucIntM(:));

            %% find intensity of eroded 2D nuclei
            gfpC = getSlice(reader, iZn, iT, iC, dim);
            gfpCM = (gfpCf  <= hb).*(gfpCf  >= lb);
            nucIntMEr = binNuc(:,:,iZn).*gfpCM;
            %nucIntMEr = (reshape(ilast(1,:,:,:,iT),dim.Ny, dim.Nx, dim.Nz) == 2).*binNuc;
            for iErode = 1:maxErosion
                nucIntMLoc = imerode(nucIntMEr, erodFil);
                %avoid erode up to 0
                if sum(nucIntMLoc(:)) > 200
                    nucIntMEr = nucIntMLoc;
                else
                    break
                end
            end
            nucIntEr = double(nucIntMEr).*double(gfpC);
            bgVal(iT, 2+(inuc-1)*3) = sum(nucIntEr(:))/sum(nucIntMEr(:));

            %% find intensity of cytoplasm
            segSlice = reshape(ilast(1,:,:,iZc,iT),dim.Ny, dim.Nx);

            % use both nucleus and cytoplasmic mask
            bwerode = imerode((segSlice==2) + (segSlice==3),strel('disk',10));
            bwerode = bwlabel(bwerode);
            bwerode = imdilate(bwerode, strel('disk',9)); %this should separate touching objects
            bwerode = imfill(bwerode);
            % perform additional marked seed watershed
            D = bwdist(bwerode);
            % boundary should also be a minimum
            D(:,[1 end]) = -100;
            D([1 end],:) = -100;
            DL = watershed(D + bwareaopen((segSlice==2),minArea)*(-100) );

            % find labelled area containung nucleus of interest
            labimg = bwlabel(bwerode.*(DL>0));
            labNuc = (binNuc(:,:,iZc)).*labimg; % and o
            lab = max(labNuc(:));
            %%
            % cytoplasm is cytoplasm - 2 times dilated nucleus.
            gfpC = getSlice(reader, iZc, iT, iC, dim);
            gfpCf = imgaussian(gfpC,2);
            gfpCM = (gfpCf).*(labimg==lab).*~(imdilate(imfill(binNuc(:,:,iZc), 'holes'), strel('disk',4)));
            hb = prctile(gfpCM(gfpCM > 0), 90);
            lb = prctile(gfpCM(gfpCM > 0), 10);
            gfpCM = (gfpCf  <= hb).*(gfpCf  >= lb);
            cytIntM = (gfpCM).*(labimg==lab).*~(imdilate(imfill(binNuc(:,:,iZc), 'holes'), strel('disk',4)));
            cytInt = double(cytIntM).*double(gfpC);
            bgVal(iT, 3+(inuc-1)*3) = sum(cytInt(:))/sum(cytIntM(:))

            % create output image
            imgfinal(:,:,iT) = imgfinal(:,:,iT) + binNuc(:,:,iZ)*(1+(inuc-1)*3) + nucIntM*(2+(inuc-1)*3) + (labimg==lab)*(3+(inuc-1)*3) ;
        end
        if bgVal(iT, 4) == 0
            bgVal(iT,[4 5 6]) =  bgVal(iT, [1 2 3]);
        end
        if bgVal(iT, 1) == 0
            bgVal(iT, [1 2 3]) = bgVal(iT,[4 5 6]);
        end

end
%%
mkdir(fullfile(outdir, 'bg'));
fid = fopen(fullfile(outdir, 'bg', [name '_bg.txt']),'w');
fprintf(fid,'Nuc1_nuc_skel\tNuc1_nuc_erode\tNuc1_cyt\tNuc2_nuc_skel\tNuc2_nuc_erode\tNuc2_cyt\n');
for iT=1:size(bgVal, 1)
    fprintf(fid, '%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\n',bgVal(iT, 1),bgVal(iT, 2),bgVal(iT, 3),bgVal(iT, 4),bgVal(iT, 5),bgVal(iT, 6));
end
fclose(fid);
%% tif file
tifile = fullfile(outdir, 'bg', [name '_bg.tif']);
imwrite(double(imgfinal(:,:,1)), jet(18), tifile);
for k = 2:size(imgfinal, 3)
    imwrite(double(imgfinal(:,:,k)), jet(18), tifile, 'writemode', 'append');
end

end

%%
% get a XY slice from a reader object. This could be a hdf5 file or a
% bioformat reader
%   iZ : index of Z slice
%   iT : index of time point
%   iC : index of channel
function slice = getSlice(reader, Z, iT, iC, dim)
    global h5
    if h5
        slice = reader(:,:,Z, iT, iC);
    else
        
        slice = zeros(dim.Ny, dim.Nx, length(Z));
        for iZ = 1:length(Z)
            iPlane = reader.getIndex(Z(iZ)-1,iC-1,iT-1) + 1;
            
            slice(:,:,iZ)= bfGetPlane(reader,iPlane);
        end
        
    end
end

