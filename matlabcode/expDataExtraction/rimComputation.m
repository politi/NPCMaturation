%%
% Use mask of rim and 3D segmented object to compute background and
% intensity
% maindir =  'C:\Users\toni\Dropbox\NPCMaturation\expdata\livecell_siRNA\images\Pom121KD\Experiment1\Pom121siRNA\LSMFiles';
% ind = [[1 6];[1 7]; [1 8];[2 6]];
% for i = 4
%     try
%         fname = fullfile(maindir, ['POM121KDLIVEIMAGING-' num2str(ind(i,1)) '_' 'Subset' num2str(ind(i,2)) '.lsm']);
%         hdf5converter_general(fname);
%     catch
%          fname = fullfile(maindir, ['POM121KDLIVEIMAGING-' num2str(ind(i,1)) '-' 'Subset' num2str(ind(i,2)) '.lsm']);
%         hdf5converter_general(fname);
%     end
% end
% 
% %%
% maindir =  'C:\Users\toni\Dropbox\NPCMaturation\expdata\livecell_siRNA\images\Pom121KD\Experiment1\ScrambledsiRNA\LSMFiles';
% ind = [[1 1];[1 2]; [1 5];[2 1]];
% for i = 1:length(ind)
%     try
%         fname = fullfile(maindir, ['POM121KDLIVEIMAGING-' num2str(ind(i,1)) '_' 'Subset' num2str(ind(i,2)) '.lsm']);
%         hdf5converter_general(fname);
%     catch
%          fname = fullfile(maindir, ['POM121KDLIVEIMAGING-' num2str(ind(i,1)) '-' 'Subset' num2str(ind(i,2)) '.lsm']);
%         hdf5converter_general(fname);
%     end
% end
%%
% maindir =  'C:\Users\toni\Dropbox\NPCMaturation\expdata\livecell_siRNA\images\ELYSKD\ScrambledsiRNA\LSM Files'
% for iset = [3 6 7]
%     fname = ['C:\\Users\\toni\\Dropbox\\NPCMaturation\\expdata\\livecell_siRNA\\images\\ELYSKD\\ELYSsiRNA\\LSMFiles\\ELYSsiRNA_1_subset' num2str(iset) '.lsm'];
%     hdf5converter_general(fname);
% end
% 
% %%
% for iset = [9 10 11 12 13]
%     fname = ['C:\\Users\\toni\\Dropbox\\NPCMaturation\\expdata\\livecell_siRNA\\images\\ELYSKD\\ELYSsiRNA\\LSMFiles\\ELYSsiRNA_2_subset' num2str(iset) '.lsm'];
%     hdf5converter_general(fname);
% end
%%
% first channel is outer cell
% second channel is cytoplasm 
% third channel is nucleus

%% compute ELYSsiRNAKD
ind  = [[1 1]; [1 2]; [1 3]; [1 6]; [1 7]; [2 9]; [2 10]; [2 11]; [2 12]; [2 13]];
maindir = 'C:\Users\toni\Dropbox\NPCMaturation\expdata\livecell_siRNA\images\ELYSKD\ELYSsiRNA\';
for i = 2:length(ind)
lsmfile = fullfile(maindir, 'LSMfiles', ['ELYSsiRNA_' num2str(ind(i,1)) '_subset' num2str(ind(i,2)) '.lsm']);
segdir = fullfile(maindir,  'Results_2016_Aug_24');
outdir = 'C:\Users\toni\Dropbox\NPCMaturation\expdata\livecell_siRNA\images\ELYSKD\ELYSsiRNA\BGComp';
[bg, img] =bgCompLiveCell(lsmfile, segdir, outdir);
%%
[path, name, ext] = fileparts(lsmfile);
figure(1)
subplot(2,1,1)

plot([1:75], [smooth(bg(:,2),10) smooth(bg(:,4),10)]);
xlabel('imaging frame')
ylabel('cytoplasm fluorescence')
legend('nucleus1', 'nucleus2')
ylim([min(min(bg(:,[2 4]))) max(max(bg(:,[2 4])))])
subplot(2,1,2)
plot([1:75], [smooth(bg(:,1),10) smooth(bg(:,3),10)])
ylim([min(min(bg(:,[1 3]))) max(max(bg(:,[1 3])))])
ylabel('nucleoplasm fluorescence')
xlabel('imaging frame')
saveas(1, fullfile(outdir, [name '.jpg']), 'jpeg')

end



%% compute ELYSsiRNAKD
ind  = [[1 1]; [1 2]; [1 3]; [1 6]; [1 7]; [2 9]; [2 10]; [2 11]; [2 12]; [2 13]];
maindir = 'C:\Users\toni\Dropbox\NPCMaturation\expdata\livecell_siRNA\images\ELYSKD\ELYSsiRNA\';
for i = 2:length(ind)
lsmfile = fullfile(maindir, 'LSMfiles', ['ELYSsiRNA_' num2str(ind(i,1)) '_subset' num2str(ind(i,2)) '.lsm']);
segdir = fullfile(maindir,  'Results_2016_Aug_24');
outdir = 'C:\Users\toni\Dropbox\NPCMaturation\expdata\livecell_siRNA\images\ELYSKD\ELYSsiRNA\BGComp';
[bg, img] =bgCompLiveCell(lsmfile, segdir, outdir);
%%
[path, name, ext] = fileparts(lsmfile);
figure(1)
subplot(2,1,1)

plot([1:75], [smooth(bg(:,2),10) smooth(bg(:,4),10)]);
xlabel('imaging frame')
ylabel('cytoplasm fluorescence')
legend('nucleus1', 'nucleus2')
ylim([min(min(bg(:,[2 4]))) max(max(bg(:,[2 4])))])
subplot(2,1,2)
plot([1:75], [smooth(bg(:,1),10) smooth(bg(:,3),10)])
ylim([min(min(bg(:,[1 3]))) max(max(bg(:,[1 3])))])
ylabel('nucleoplasm fluorescence')
xlabel('imaging frame')
saveas(1, fullfile(outdir, [name '.jpg']), 'jpeg')

end


%% compute scrambled
ind  = [1:7];
maindir = 'C:\Users\toni\Dropbox\NPCMaturation\expdata\livecell_siRNA\images\ELYSKD\ScrambledsiRNA\';
for i = 1:length(ind)
lsmfile = fullfile(maindir, 'LSMfiles', ['ScrambledsiRNA_subset' num2str(ind(i)) '.lsm']);
segdir = fullfile(maindir,  'Results_2016_Aug_24');
outdir = 'C:\Users\toni\Dropbox\NPCMaturation\expdata\livecell_siRNA\images\ELYSKD\ScrambledsiRNA\BGComp';
[bg, img] = bgCompLiveCell(lsmfile, segdir, outdir);
%%
[path, name, ext] = fileparts(lsmfile);

figure(1)
subplot(2,1,1)

plot([1:75], [smooth(bg(:,2),10) smooth(bg(:,4),10)]);
xlabel('imaging frame')
ylabel('cytoplasm fluorescence')
legend('nucleus1', 'nucleus2')
ylim([min(min(bg(:,[2 4]))) max(max(bg(:,[2 4])))])
subplot(2,1,2)
plot([1:75], [smooth(bg(:,1),10) smooth(bg(:,3),10)])
ylim([min(min(bg(:,[1 3]))) max(max(bg(:,[1 3])))])
ylabel('nucleoplasm fluorescence')
xlabel('imaging frame')
saveas(1, fullfile(outdir, [name '.jpg']), 'jpeg')

end