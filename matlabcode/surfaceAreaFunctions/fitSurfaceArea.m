%%
% Fit surface area changes of nuclei after anaphase to
%   a0 + a1*(1-exp(kg1*t))+kg2*t
% returns parameter of fits
% data is a column vector containing time_After_anaphase(h) mean_area std_area
% Shotaro Pore maturation project
% Antonio Politi, EMBL, March 2015
%
function parA = fitSurfaceArea(showPlot)
if ispc
    indir = fullfile([getenv('HOMEDRIVE') getenv('HOMEPATH') ], 'Dropbox', 'NPCMaturation',...
        'expdata', 'emdata');
end
if ismac
    indir = fullfile(getenv('HOME'), 'Dropbox',  'NPCMaturation',...
        'expdata', 'emdata');
end
dataSet1 = load(fullfile(indir, 'surfaceNucleus_24h_new.txt'));
dataSet2 = load(fullfile(indir, 'surfaceNucleus_2h.txt'));

useSet = 1;


if useSet == 1
    %omit first 2 time pts
    data = dataSet1;
    data(:,1) = data(:,1)*60;
    st=1;
    outputfile =  fullfile(indir,'surfaceNucleusFit_24h_new_tmp.txt');
end

if useSet == 2
    %omit first 2 time pts
    data = dataSet3;
    st = 3;
    outputfile =  fullfile(indir,'surfaceNucleusFit_2h.txt');
end
    

if nargin < 1
    showPlot = 1;
end



bounds = [[300 300 2.5/60 25/60]; [0 0 0 0];[1000 1000 1000 1000]];
%par = lsqnonlin(@distSurf, bounds(1,:), bounds(2,:), bounds(3,:), optimset,...
%    [1:4], [424.0446  161.3104    0.0722    0.3969], data(st:end,1)-data(st,1), data(st:end,:))
par = lsqnonlin(@distSurf, bounds(1,:), bounds(2,:), bounds(3,:), optimset,...
    [1:4], [424.0446  161.3104    0.0722    0.3969], data(st:end,1), data(st:end,:), data(st,1))

[surfA, dsurfA] = surfaceArea(par, data(1:end,1), data(st,1));
confInterval(par, bounds, data(:,1), data, st)
%compute the confidence interval of the parameter using a likelihood
%boundary method


if showPlot
    setupFigure(1,[100 0 500 600]);
    subplot(2,1,1)
    hold
    plot(data(:,1), data(:,2), 'k-')
    plot(data(:,1), [data(:,2)+data(:,3) data(:,2)-data(:,3)], 'Color', [0.5 0.5 0.5]);
    plot(data(1:end,1), surfA(1:end), 'b-', 'LineWidth', 2)
    xlabel('time (min)')
    ylabel('A, Nuclear surface (um2)')
    title(sprintf('A=a0+a1*(1-exp(-kg1*(t-ts)))+kg2*(t-ts)\n a0=%.1f um2, a1=%.1f um2, kg1=%.2e/min, kg2=%.2e/min, ts=%.1d min', par(1), par(2),par(3), par(4), round(data(st,1))))
    xlim([0 data(end,1)+20])
    subplot(2,1,2)
    hold
    plot(data(:,1), dsurfA./surfA, 'b-', 'LineWidth', 2)
    plot([data(1,1) data(end,1)], [mean(dsurfA(6:end)./surfA(6:end)) mean(dsurfA(6:end)./surfA(6:end))], 'k--')
    legend('fullmodel', sprintf('d logA/dt = %.2e/min', mean(dsurfA(6:end)./surfA(6:end))))
    xlabel('time (min)')
    ylabel('d logA /dt (1/min)')
    [fpath, fname, ext] = fileparts(outputfile)
    saveas(1,fullfile(fpath, [fname '.eps']), 'epsc')
    saveas(1,fullfile(fpath, [fname '.jpg']), 'jpeg')
    saveas(1,fullfile(fpath, [fname '.fig']), 'fig')

end
fid = fopen(outputfile,'w')
fprintf(fid,'%%surface area nucleus fitted to a0 + a1*(1-exp(-kg1*(t-ts)) + kg2*(t-ts). Time is time after anaphase (in min)\n%%a0(um2) \t a1(um2) \t kg1/min  \t kg2/min \t ts min\n');
fprintf(fid,'%.3e \t %.3e \t %.3e \t %.3e \t %.1d \n',par(1), par(2), par(3), par(4), data(st,1));
fclose(fid)
end

function confInterval(par, bounds, t, data, st)
    %   Profile Likelihood Confidence Intervals
    %   at CI95%
    nd = size(data(st:end,:),1);
    
    normB = sum(distSurf(par, [1:4], par, t,  data, data(st,1)).^2);
    
    %CI for 1st par

    vecFitCell = {[2 3 4], [1 3 4], [1 2 4], [1 2 3]};
    CI = [par;par;par];
    for i=1:4
        vecF = vecFitCell{i};
        maxV= 2;
        minV = 1;
        while abs(maxV-minV)>1e-3
            hV = (maxV+minV)/2;
            parH = par;
            parH(i) = par(i)*(maxV+minV)/2;
            [tmpPar, normH] = lsqnonlin(@distSurf, bounds(1,2:4),  bounds(2,vecF),  bounds(3,vecF), optimset, vecF, parH, data(st:end,1), data(st:end,:), data(st,1));
            phiH = abs(nd*(log(normH/nd) - log(normB/nd))) - 3.84;
            if phiH > 0
                maxV = hV;
            else
                minV = hV;
            end
        end
        CI(2,i) = parH(i);
        
        maxV = 1;
        minV = 0.5;
        while abs(maxV-minV)>1e-3
            hV = (maxV+minV)/2;
            parH = par;
            parH(i) = par(i)*(maxV+minV)/2;
            [tmpPar, normH] = lsqnonlin(@distSurf, bounds(1,2:4),  bounds(2,vecF),  bounds(3,vecF), optimset, vecF, parH, data(st:end,1), data(st:end,:), data(st,1));
            phiH = abs(nd*(log(normH/nd) - log(normB/nd)))-3.84;
            if phiH > 0
                maxV = hV;
            else
                minV = hV;
            end
        end
        CI(3,i) = parH(i);
        CI
    end
end
    
function dist = distSurf(parfit, ipar, parO, t, data, ts)
%distance Surface area function to data
par = parO;
par(ipar) = parfit;
surf = surfaceArea(par, t,ts);
dist = (data(:,2) - surf)./data(:,3);
end

function [surfA, dsurfA] = surfaceArea(par, t, ts)
%compute surface area
a0 = par(1);
a1 = par(2);
kg1 = par(3);
kg2 = par(4);

surfA = (a0+a1*(1-exp(-kg1*(t-ts)))+kg2*(t-ts)).*(t>=ts) + a0.*(t<ts);
dsurfA = (a1*kg1*exp(-kg1*(t-ts)) + kg2).*(t>=ts)+0.*(t<ts);

end