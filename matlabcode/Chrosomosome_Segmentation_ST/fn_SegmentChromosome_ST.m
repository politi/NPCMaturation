function fn_SegmentChromosome_ST(fp, fn, channel)
% This function segments chromosome mass from the second channel and save
% it into a mat file
% Version data: 2015-11-11
% Author: Julius Hossain, EMBL, Heidelberg, Germany: julius.hossain@embl.de
% fp = 'U:\Antonio_ki67_segmentation\151007_1330_meta\';
% fn = 'TR1_2_W0001_P0001_T0001.lsm';
outDir = [fp 'Results' filesep];
disp(['PROCESSING:' fp]);
close all; clc
%addpath 'cstruct'
%addpath 'lsm'

%Filtering parameters
sigmaBlur = 1.5; %Sigma for Gaussian filter
hSizeBlur = 2; %Kernel size for Gaussian filter
bFactor2D = 0.25; %Contribution of 2D threshold
minObjVol = 20;  %Maximum volume of a blob that is considered for merging
outDirMat = outDir; %[outDir 'Mat\'];
outDirRecons = [outDir 'Reconstructed_Volume\'];
outDirSlice = [outDir 'ChrBoundary_OverSelectedSlice\'];

if ~exist(outDirMat)
    mkdir(outDirMat);
end

if ~exist(outDirRecons)
    mkdir(outDirRecons);
end
if ~exist(outDirSlice)
    mkdir(outDirSlice);
end

lsminf = lsminfo([fp fn]); %Reads the lsm header
dx = lsminf.DIMENSIONS(1); %Image height
dy = lsminf.DIMENSIONS(2); % Image width
NzOrig = lsminf.DIMENSIONS(3); %Number of Z slices in lsm file
Nc = lsminf.NUMBER_OF_CHANNELS; %Number of channels
detCoords = zeros(2,3);
tfin = lsminf.TIMESTACKSIZE; %Number of timepoints
ncVolume = zeros(tfin, 1);
minEigen = zeros(tfin, 1);
detCH = zeros(tfin, 1);

zinit = 1; %Set higher than one if you want to some of the lower slices
%Size of voxels in x, y and z in micro meter
voxelSizeX = lsminf.VoxelSizeX * 1e6;
voxelSizeY = lsminf.VoxelSizeY * 1e6;
voxelSizeZ = lsminf.VoxelSizeZ * 1e6;

zFactor = round(voxelSizeZ/voxelSizeX); % zFactor-1 intermediate slice(s) will be generated
voxelSizeZ = voxelSizeZ/zFactor; % Update voxelSizeZ based on the number of intermediate slices
voxelSize = voxelSizeX * voxelSizeY * voxelSizeZ; %Volume of a voxel in cubic micro meter
Nz = (NzOrig-zinit+1) * zFactor - zFactor + 1; %Number of z slice after intepolation
chrStackOrig = zeros(dx,dy,NzOrig-zinit+1); % Allocate memomry for original stack

%Reads input stack
for zplane=zinit:NzOrig
    stacklsm = tiffread31([fp fn], 2*(zplane-zinit+1)-1);
    %stacklsm = tiffread([fp fn],2*((tpoint-1)*NzOrig+zplane-zinit+1)-1);
    if Nc >= channel
        chrStackOrig(:,:,zplane-zinit+1) = stacklsm.data{channel};
    else
        disp('Chromosome channel (ch-2) is not found');
        break;
    end
    clear stacklsm
end

%chrStackOrig = imreadBF([fp fn],zinit:NzOrig,tpoint,2);
%Diplay input image stack
%displaySubplots(chrStackOrig, 'Display input image stack', disRow, disCol, NzOrig, 1, 2);
disp(['Processing: ' fp fn]);

chrStack = fn_PC_GenIntermediateSlices(chrStackOrig, zFactor);
clear chrStackOrig;


chrRegion = zeros(dx,dy,Nz);%Store the detected chromosome region
bordImg = zeros(dx,dy,Nz);
bordImg(1,:,:) = 1; bordImg(end,:,:) = 1;
bordImg(:,1,:) = 1; bordImg(:,end,:) = 1;
%Display filtered data
%displaySubplots(chrStack, 'Filtered chromosome', disRow, disCol, Nz, zFactor, 2);

%% Detection of chromosome
chrStackBack = chrStack;
chrStack = imgaussian(chrStack, sigmaBlur, hSizeBlur);

[chrThresh3D, chrThresh2D] = fn_PC_GetThresholds(chrStack);

for i = 1:Nz
    chrRegion(:,:,i) = double(chrStack(:,:,i) >= (chrThresh3D * (1-bFactor2D) + chrThresh2D(i) * bFactor2D));
end


chrRegion = fn_RemoveObjectsAtBoundary(chrRegion, 18, minObjVol, bordImg, voxelSize);
chrVol = sum(sum(sum(chrRegion))) *voxelSize;
chrRegion = fn_SmoothAndEqualize(chrRegion, chrVol, voxelSize);
for i=1:size(chrRegion,3)
    chrRegion(:,:,i) = imfill(chrRegion(:,:,i));
end
%%  End of masking chrStack

chrMask_Iso = chrRegion;
chrMask = fn_PC_RemoveIntermediateSlices(chrRegion, zFactor);
fileNameMat = [outDirMat fn(1:end-4) '.mat'];
save(fileNameMat, 'chrMask', 'chrMask_Iso');
p = find(chrRegion);
z=floor((p-1)/(dx*dy))+1;
selSlice = round(sum(z)/length(z));
disSliceChr  = chrStackBack(:,:,selSlice);
h = figure; imagesc(disSliceChr);
axis off; hold on;
c = contours(chrRegion(:,:,selSlice),[0,0]);
zy_plot_contours(c,'linewidth',1,'color', 'red');

fileNameSlice = [outDirSlice fn(1:end-4) '.bmp'];
saveas(h, fileNameSlice,'bmp');

[X,Y,Z]=meshgrid((1:dy)*voxelSizeY,(1:dx)*voxelSizeX,(1:Nz)*voxelSizeZ);
hVol = figure('Name', strcat('Reconstructed 3D volume'));
isosurface(X,Y,Z,chrRegion,0.9);
alpha(0.5)
axis equal

fileNameRecons = [outDirRecons fn(1:end-4) '.jpg'];
saveas(hVol, fileNameRecons, 'jpg');
delete(hVol);
clear all;
end