function [outStack] = fn_SmoothAndEqualize(bwStack, refVol, voxelSize)
%This function smooth 3D bwStack and equalize to a given reference volume
smoothStack  = smooth3(bwStack(:,:,:) , 'box', 7);
outStack = double(smoothStack(:,:,:) >= 0.5);
outVol = sum(sum(sum(outStack))) *voxelSize;
thDist = 0.49;
while outVol <= refVol *0.995 && thDist >= 0.01
    outStack = double(smoothStack(:,:,:) >= thDist);
    outVol = sum(sum(sum(outStack))) *voxelSize;
    thDist = thDist -0.01;
end
end

