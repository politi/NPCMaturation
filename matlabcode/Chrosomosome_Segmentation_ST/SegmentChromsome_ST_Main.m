% Version data: 2015-11-11
% Author: Julius Hossain, EMBL, Heidelberg, Germany: julius.hossain@embl.de
% modified 21.04.2016 Antonio Politi introduce channel parameter
%This scripts search for folders containing high resoluation images and
%call the function that segments chromosomes and save output in a mat file
clc; clear all; close all;
rootDir = 'Z:\AntonioP_elltier1\CelllinesImaging\160413\mEGFP-Nup107\';
fn = dir([rootDir 'HR5.lsm']);
channel = 3;
for i=1:length(fn)
    try
        fn_SegmentChromosome_ST(rootDir, fn(i).name, channel);
    catch
        disp(['Could not process the cell: ' fn(i).name]);
    end
end

% rootDir = 'U:\Antonio_151214\toSegment\Ki67_1336_meta\';
% fn = dir([rootDir '*.lsm']);
% 
% for i=1:length(fn)
%     try
%         fn_SegmentChromosome_ST(rootDir, fn(i).name);
%     catch
%         disp(['Could not process the cell: ' fn(i).name]);
%     end
% end