function biggestobject=findbiggestobject_bwconcomp(image,connection)
twoDLabel = bwconncomp(image,connection);
numPixels = cellfun(@numel,twoDLabel.PixelIdxList);
[biggest, idx] = max(numPixels);
biggestobject = image;
biggestobject(:,:) = 0;
biggestobject(twoDLabel.PixelIdxList{idx}) = 1;
