function [bwImage] = fn_RemoveObjectsAtBoundary(bwImage, conn, minObjVol, bordImg, voxelSize)
% This function remove small object that are not on boundary of the image
threeDLabel = bwconncomp(bwImage,conn);
numPixels = cellfun(@numel,threeDLabel.PixelIdxList);
tempVol = zeros(size(bwImage));

for i = 1 : numel(numPixels)
    tempVol(:,:,:) = 0;
    tempVol(threeDLabel.PixelIdxList{i}) = 1;
    if numPixels(i) * voxelSize < minObjVol || sum(sum(sum(and(tempVol, bordImg)))) > 0
        bwImage(threeDLabel.PixelIdxList{i}) = 0;
    end
end

end