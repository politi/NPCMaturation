%%
% dealyedMaturation model with production proportional to free area
% V = k*(A(t)*rho0-Total_number_pores)
% par(1): ts, start of interphase assembly 
% par(2): k, maximal rate of interphase assembly
% par(3): rho0, steady state interphase density
% par(4): kM, initiation of maturation
% par(5): tauM, maturation time
% par(6): kd, decay of Mature pore
% par(7): number of minipores at ts
% par(8): number of mature pores at 0


classdef delayedMaturationSteadyRho < absDelayedMaturation
   properties

   end
   methods 
        function MO = delayedMaturationSteadyRho(surf, useSpline, force)
            MO@absDelayedMaturation(surf, useSpline, force)
            MO.dataF = MO.dataA;
            for i =1:2
                MO.dataF(i).noncore = MO.dataF(i).d_noncore;
                MO.dataF(i).core_i = MO.dataF(i).d_core_i;
                MO.dataF(i).core_o = MO.dataF(i).d_core_o;
            end
            MO.N = 3;
            MO.parnames = 'ts k rho0 kM tauM kd P01 M01 P02 M02';
            MO.name = mfilename('class')
            MO.LB = [0, 0, 8, 0.1, 10, 0, 0, 0,0,0]; %Low boundary values
            MO.HB = [15, 1, 15, 10, 70, 1, 5000, 5000, 5000, 5000]; %High boundary values
        end
        
        function obs = getObservable(MO,sol)
            obs(:,1) = sol(1,:)'+ sol(2,:)';
            obs(:,2) =  sol(3,:)';
        end
        
        %%
        % solve equations for one set of parameters and provide the 
        function [soli, solo] = solve(MO, par, timeint)
            soli = dde23(@MO.dde,[par(5)],@MO.ddehist1,timeint, ddeset, par);  
            solo = dde23(@MO.dde,[par(5)],@MO.ddehist2,timeint, ddeset, par); 
        end
        
        %%
        % get production rate of minipores and mature pores coming from PM
        % only
        function [prodRate, maturePM] = getProdRate(MO, par, timeint) 
           [soli, solo] = solve(MO, par, [par(1) timeint(end)]);
                        
            A = MO.getSurf(timeint);
            yi = deval(soli, timeint);
            yo = deval(solo, timeint);
            obsi = MO.getObservable(yi);
            obso = MO.getObservable(yo);
            
            k =  par(2)*(timeint >= par(1));
            prodRate = [(k.*(A*par(3) - obsi(:,1)-obsi(:,2)))./A (k.*(A*par(3) - obso(:,1)-obso(:,2)))./A];
            maturePM = [par(8)*exp(-par(6)*(timeint - par(1)))./A par(10)*exp(-par(6)*(timeint - par(1)))./A];
            
        end
        
        function s = ddehist1(MO,t,par)
            % Constant history function for DDEX1.
            s = [0, 0, par(8)];
            if t == par(1)
                s = [par(7), 0, par(8)];
            end
        end
        
        function s = ddehist2(MO,t,par)
            % Constant history function for DDEX1.
            s = [0, 0, par(10)];
            if t == par(1)
                s = [par(9), 0, par(10)];
            end
        end
        function dydt = dde(MO, t, y, Z, par)
            % par(1): ts, start of interphase assembly 
            % par(2): k, maximal rate of interphase assembly
            % par(3): rho0, steady state interphase density
            % par(4): kM, initiation of maturation
            % par(5): tauM, maturation time
            % par(6): kd, decay of Mature pore
            % par(7): number of minipores at ts
            % par(8): number of mature pores at 0
            % Delay Differential equations function for DDEX1.
            ylag = Z(:,1);
            if t > par(1)
                k = par(2);
            else
                k = 0;
            end
            A = MO.getSurf(t);
            dydt = [ k*(A*par(3) - y(1) - y(2) -y(3)) - par(4)*y(1)
                     par(4)*y(1) - par(4)*ylag(1)
                     par(4)*ylag(1) - par(6)*y(3)];
        end
        
        function [par, norm] = testFit(MO)
            ts = 10;
            At0 = MO.getSurf(ts);
            P01 = 4.493*At0;
            M01 = 4.79*At0;
            P02 = 3.935*At0;
            M02 = 9.048*At0;
            if nargin < 2
                par = [ts, 0.014,  12, 1.136, 40, 4.2e-04, P01, M01, P02, M02];
            end
            ifit = [2:5 7:10];
            %ifit = [2:5 8 10];
            
            [parf, norm] = MO.fitModel(ifit, par)
            
            par(ifit) = parf;
            MO.plotResult(par)
        end

            
   end
end