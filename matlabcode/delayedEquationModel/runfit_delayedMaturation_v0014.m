%%
%fit just Mature pore
clear;
clear all;
fclose all
MO = delayedMaturation('24h', 0,0)
MO.useIntermediate = 0;
tsv = [5 7 10 12 15];
nrFitvec = [10 10 10 10 10];
MO.outdir = fullfile(MO.outdir, 'delay')
%%
for its = 3 %1:length(tsv)
ts = tsv(its);
nrfits = nrFitvec(its);
At0 = MO.getSurf(ts);
P01 = 0*4.493*At0;
M01 = 4.79*At0;
P02 = 0*3.935*At0;
M02 = 9.048*At0;
MO.useIntermediate = 0;
par = [ts, 0.014,  1.136, 40, 4.2e-04, P01, M01, P02, M02];
ifit = [3:4 7 9];
parfile = ['delayedMaturation_MaturePore_noPrecursor_v0.014_ts' num2str(ts)]
denfile =  fullfile(MO.outdir,[parfile '_den.txt']);
parfile = [parfile '.txt'];
MO.writeDensities(MO.getBestfit(fullfile(MO.outdir, parfile)),denfile);

MO.runFit(par, ifit, nrfits, 0.5, parfile)
MO.plotBestfit(fullfile(MO.outdir, parfile));

parf = MO.getBestfit(fullfile(MO.outdir, parfile));
[dist, distI, distM] = MO.distMO(parf(ifit), ifit, parf);
sum(distM.^2)
%%
P01 = 4.493*At0;
P02 = 3.935*At0;
MO.useIntermediate = 0;
par = [ts, 0.014, 1, 40, 4.2e-4, P01, M01, P02, M02];
ifit = [3:4 6:9];
parfile = ['delayedMaturation_MaturePore_Precursor_v0.014_ts' num2str(ts)];
denfile =  fullfile(MO.outdir,[parfile '_den.txt']);
parfile = [parfile '.txt'];
MO.writeDensities(MO.getBestfit(fullfile(MO.outdir, parfile)),denfile);
MO.runFit(par, ifit, nrfits, 0.5, parfile )
MO.plotBestfit(fullfile(MO.outdir, parfile ))
parf = MO.getBestfit(fullfile(MO.outdir, parfile));
[dist, distI, distM] = MO.distMO(parf(ifit), ifit, parf);
sum(distM.^2)
%%
P01 = 4.493*At0;
P02 = 3.935*At0;
MO.useIntermediate = 1;
par = [ts, 0.015, 1, 40, 4.2e-4, P01, M01, P02, M02];
ifit = [3:4 6:9];
parfile = ['delayedMaturation_v0.015_ts' num2str(ts)];
denfile =  fullfile(MO.outdir,[parfile '_den.txt']);
parfile = [parfile '.txt'];

MO.runFit(par, ifit, nrfits, 0.5, parfile)
MO.plotBestfit(fullfile(MO.outdir, parfile));
parf = MO.getBestfit(fullfile(MO.outdir, parfile));
[dist, distI, distM] = MO.distMO(parf(ifit), ifit, parf);
sum(distM.^2)
MO.writeDensities(MO.getBestfit(fullfile(MO.outdir, parfile)),denfile);

%%
MO.useIntermediate = 1;
ts = tsv(its);
nrfits = nrFitvec(its);
At0 = MO.getSurf(ts);
P01 = 0*4.493*At0;
M01 = 4.79*At0;
P02 = 0*3.935*At0;
M02 = 9.048*At0;
par = [ts, 0.014,  1.136, 40, 4.2e-04, P01, M01, P02, M02];
ifit = [3:4 7 9];
parfile = ['delayedMaturation_noPrecursor_v0.014_ts' num2str(ts)];
denfile =  fullfile(MO.outdir,[parfile '_den.txt']);
parfile = [parfile '.txt'];
MO.writeDensities(MO.getBestfit(fullfile(MO.outdir, parfile)),denfile);

MO.runFit(par, ifit, nrfits, 0.5, parfile)
MO.plotBestfit(fullfile(MO.outdir, parfile));
parf = MO.getBestfit(fullfile(MO.outdir, parfile));
[dist, distI, distM] = MO.distMO(parf(ifit), ifit, parf)
sum(distM.^2)
end

%%

parfile = ['delayedMaturation_v0.015_ts' num2str(ts)];
parfile = [parfile '.txt'];
parf = MO.getBestfit(fullfile(MO.outdir, parfile));

[soli, solo] = MO.solve(parf, [parf(1) 1200]);

%%
timeMature = [180:10:1200];
A = MO.getSurf(timeMature);
yi = deval(soli, timeMature);
yo = deval(solo, timeMature);
mean(yi(3,:)./A)
%%