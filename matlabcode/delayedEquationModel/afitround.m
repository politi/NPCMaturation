function [dist, distI, distM] = afitround(MO, par, ifit, nrfits, parfile)
    denfile =  fullfile(MO.outdir,[parfile '_den.txt']);
    prodratefile = fullfile(MO.outdir, [parfile '_prodrate.txt']);
    fractionMaturefile = fullfile(MO.outdir, [parfile '_fracM.txt']);
    parfile = [parfile '.txt'];

    MO.runFit(par, ifit, nrfits, 0.5, parfile)
    MO.plotBestfit(fullfile(MO.outdir, parfile), 1, 1);
    parf = MO.getBestfit(fullfile(MO.outdir, parfile));
    [dist, distI, distM] = MO.distMO(parf(ifit), ifit, parf);
    dist = sum(dist.^2);
    distI = sum(distI.^2);
    distM = [sum(distM.^2) sum(distM(1:12).^2) sum(distM(13:24).^2)];
    MO.writeDensities(parf,denfile);
    MO.writeProdRate(parf, prodratefile);
    MO.writeFractionMatureIP(parf, fractionMaturefile);
    %%
    [soli, solo] = MO.solve(parf, [parf(1) 1200]);
    timPM = [180:1:1200];
    yi = deval(soli, timPM);
    yo = deval(solo, timPM);
    A = MO.getSurf(timPM);
    mean(yi(3,:)./A*0.68 + yo(3,:)./A*0.32)
    std(yi(3,:)./A*0.68 + yo(3,:)./A*0.32)
    mean((yi(1,:)+yi(2,:))./A*0.68 + (yi(1,:)+yi(2,:))./A*0.32)
    std((yi(1,:)+yi(2,:))./A*0.68 + (yi(1,:)+yi(2,:))./A*0.32)
end