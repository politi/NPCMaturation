clear;
clear all;
fclose all
ts = 10;

MO = linearChainApproximationMultiPar('24h', 0, 0, 5);
At0 = MO.getSurf(ts);
P01 = 4.493*At0;
M01 = 4.79*At0;
P02 = 3.935*At0;
M02 = 9.048*At0;
%%
Nval = [5 10 20 30 40 50];
nrfits = 5;
for its = 1:length(Nval)
    parfile = ['linearChainApproximationMultiPar_N' num2str(Nval(its)) '.txt'];
    MO = linearChainApproximationMultiPar('24h', 0, 0, Nval(its));
    MO.useIntermediate = 1;
    par = [ts, 0.014, Nval(its)/20*ones(1,Nval(its)-1), 4.2e-4, P01, M01, P02, M02];
    ifit = [2:length(par)-5 length(par)-3:length(par)];
    MO.outdir = fullfile(MO.outdir, 'linearChain');
    denfile =  fullfile(MO.outdir,['linearChainApproximationMultiPar_N' num2str(Nval(its)) '_den.txt'])
    MO.runFit(par, ifit, nrfits, 0.5, parfile );
    MO.writeDensities(MO.getBestfit(fullfile(MO.outdir, parfile)),denfile);

end

%%
clear;
clear all;
fclose all
ts = 10;

MO = linearChainApproximationMultiPar('24h', 0, 0, 5);
At0 = MO.getSurf(ts);
P01 = 4.493*At0;
M01 = 4.79*At0;
P02 = 3.935*At0;
M02 = 9.048*At0;
Nval = [5 10 20 30 40 50];
nrfits = 5;
for its = 1:length(Nval)
    parfile = ['linearChainApproximationMultiPar_N' num2str(Nval(its)) '_v0.015'];
    denfile =  fullfile(MO.outdir, [parfile '_den.txt']);
    parfile = [parfile '.txt'];
    MO = linearChainApproximationMultiPar('24h', 0, 0, Nval(its));
    MO.useIntermediate = 1;
    par = [ts, 0.015, Nval(its)/20*ones(1,Nval(its)-1), 4.2e-4, P01, M01, P02, M02];
    ifit = [3:length(par)-5 length(par)-3:length(par)];
    MO.outdir = fullfile(MO.outdir, 'linearChain');
    MO.runFit(par, ifit, nrfits, 0.5, parfile );
    MO.writeDensities(MO.getBestfit(fullfile(MO.outdir, parfile)),denfile);

end