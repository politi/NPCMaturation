%%
% linearChainApproximation model with production proportional to free area
% linear steps
% V = v*A
% par(1): ts, start of interphase assembly 
% par(2): v, maximal rate of interphase assembly
% par(3): mean rate constant 
% par(4): std of rate constants
% par(5): kd, degradation rate constant of mature pores
% par(5): number of minipores at ts inner core
% par(6): number of mature pores at ts inner core
% par(7): number of minipores at ts outer core
% par(8): number of mature pores at ts outer core

classdef linearChainApproximationMultiPar < absDelayedMaturation
    methods 
        function MO = linearChainApproximationMultiPar( surf, useSpline, force, N)
            MO@absDelayedMaturation(surf, useSpline, force)
            MO.dataF = MO.dataA;
            for i =1:2
                MO.dataF(i).noncore = MO.dataF(i).d_noncore;
                MO.dataF(i).core_i = MO.dataF(i).d_core_i;
                MO.dataF(i).core_o = MO.dataF(i).d_core_o;
            end
            MO.N = N;
            MO.parnames = 'ts v ';
            
            for i=1:MO.N-1
               MO.parnames = [MO.parnames 'k' num2str(i) ' '];
            end
            MO.parnames = [MO.parnames 'kd P01 M01 P02 M02'];
            
            MO.LB = [0,  0];
            MO.HB = [15, 1];
            for i=1:MO.N-1
                MO.LB = [MO.LB, 0.01];
                MO.HB = [MO.HB, 10];
            end
            MO.LB = [MO.LB, 0,   0,    0,    0,    0];
            MO.HB = [MO.HB, 0.1, 5000, 5000, 5000, 5000];
        end
        
        function obs = getObservable(MO, sol)
            obs(:,1) = sum(sol(1:end-1,:),1);
            obs(:,2) = sol(end,:);
        end
        
        function [soli, solo] = solve(MO, par, timeint)

            pval = par(2:end-4); 
            soli = ode45(@MO.odeFun, timeint, [par(end-3) zeros(1,MO.N-2) par(end-2)], odeset, pval);  
            solo = ode45(@MO.odeFun, timeint, [par(end-1) zeros(1,MO.N-2) par(end)], odeset, pval); 
        end
        
        function dydt = odeFun(MO, t, y, par)
            A = MO.getSurf(t);
            M1 = diag(-1*ones(1,MO.N));
            M2 = diag(ones(1,MO.N),-1);
            M = M1*diag(par(2:end)) + M2(1:end-1, 1:end-1)*diag(par(2:end));
            
            dydt = M*y  + [par(1)*A zeros(1,MO.N-1)]';
        end
        
   
end
end