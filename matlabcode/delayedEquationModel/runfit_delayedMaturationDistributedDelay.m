%%
%fit just Mature pore
clear;
clear all;
fclose all
MO = delayedMaturationDistributedDelay('24h', 0,0)
MO.useIntermediate = 0;
tsv = [5 7 10 12 15];
nrFitvec = [10 10 10 10 10];
MO.outdir = fullfile(MO.outdir, 'delay')
ts = 10
At0 = MO.getSurf(ts);
P01 = 4.493*At0;
M01 = 4.79*At0;
P02 = 3.935*At0;
M02 = 9.048*At0;



% MO2 = delayedMaturation('24h', 0,0)
% par = [ts, 0.01, 1, tauM,  0, P01, M01, P02, M02];
% [mati, mato] = MO2.solve(par, [10:0.1:120])
% 
% plot(mati.x, mati.y(3,:), 'r-')

%%
for its = 3 %1:length(tsv)
%%
ts = tsv(its);
nrfits = nrFitvec(its);
MO.useIntermediate = 0;
At0 = MO.getSurf(ts);
P01 = 0*4.493*At0;
M01 = 4.79*At0;
P02 = 0*3.935*At0;
M02 = 9.048*At0;
MO.useIntermediate = 0;
par = [ts, 0.014,  1.136, 40, 10, 4.2e-04, P01, M01, P02, M02];
ifit = [2:5 8 10];
parfile = ['delayedMaturationDistrubutedDelay_MaturePore_noPrecursor_ts' num2str(ts)];
[dist, distI, distM] = afitround(MO, par, ifit, nrfits, parfile)

%%
P01 = 4.493*At0;
P02 = 3.935*At0;
MO.useIntermediate = 0;
par = [ts, 0.014, 1, 40, 10, 4.2e-4, P01, M01, P02, M02];
ifit = [2:5 7:10];
parfile = ['delayedMaturationDistrubutedDelay_MaturePore_Precursor_ts' num2str(ts)];
[dist, distI, distM] = afitround(MO, par, ifit, nrfits, parfile)


%%
P01 = 4.493*At0;
P02 = 3.935*At0;
MO.useIntermediate = 1;
par = [ts, 0.014, 1, 40, 10, 4.2e-4, P01, M01, P02, M02];
ifit = [2:5 7:10];
parfile = ['ddelayedMaturationDistrubutedDelay_ts' num2str(ts)];
[dist, distI, distM] = afitround(MO, par, ifit, nrfits, parfile)


%%
MO.useIntermediate = 1;
ts = tsv(its);
nrfits = nrFitvec(its);
At0 = MO.getSurf(ts);
P01 = 0*4.493*At0;
M01 = 4.79*At0;
P02 = 0*3.935*At0;
M02 = 9.048*At0;
par = [ts, 0.014,  1.136, 40, 10, 4.2e-04, P01, M01, P02, M02];
ifit = [2:5 8 10];
parfile = ['delayedMaturationDistrubutedDelay_noPrecursor_ts' num2str(ts)];
afitround(MO, par, ifit, nrfits, parfile);

end


