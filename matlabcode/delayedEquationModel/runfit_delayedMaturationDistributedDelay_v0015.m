%%
%fit just Mature pore
clear;
clear all;
fclose all
MO = delayedMaturationDistributedDelay('24h', 0,0)
MO.useIntermediate = 0;
tsv = [5 7 10 12 15];
nrFitvec = [10 10 4 10 10];
MO.outdir = fullfile(MO.outdir, 'delay')
ts = 10
At0 = MO.getSurf(ts);
P01 = 4.493*At0;
M01 = 4.79*At0;
P02 = 3.935*At0;
M02 = 9.048*At0;

tauM = 40;
par = [ts, 0.01, 1, tauM, 20, 4.2e-4, P01, M01, P02, M02];
[soli, solo] = MO.solve(par, [10:0.1:120])
figure(1)
clf
plot(soli.x, soli.y)
hold
MO2 = delayedMaturation('24h', 0,0)
par = [ts, 0.01, 1, tauM,  4.2e-4, P01, M01, P02, M02];
[soli, solo] = MO2.solve(par, [10:0.1:120])
plot(soli.x, soli.y)

%%
for its = 3 %1:length(tsv)
%%
ts = tsv(its);
nrfits = nrFitvec(its);
MO.useIntermediate = 0;
At0 = MO.getSurf(ts);
P01 = 0*4.493*At0;
M01 = 4.79*At0;
P02 = 0*3.935*At0;
M02 = 9.048*At0;
MO.useIntermediate = 0;
% par = [ts, 0.015,  1.136, 40, 10, 4.2e-04, P01, M01, P02, M02];
% ifit = [3:5 8 10];
% parfile = ['delayedMaturationDistrubutedDelay_MaturePore_noPrecursor_ts' num2str(ts) '_v0.015'];
% [dist, distI, distM] = afitround(MO, par, ifit, nrfits, parfile)

%%
% P01 = 4.493*At0;
% P02 = 3.935*At0;
% MO.useIntermediate = 0;
% par = [ts, 0.015, 1, 40, 10, 4.2e-4, P01, M01, P02, M02];
% ifit = [3:5 7:10];
% parfile = ['delayedMaturationDistrubutedDelay_MaturePore_Precursor_ts' num2str(ts) '_v0.015'];
% [dist, distI, distM] = afitround(MO, par, ifit, nrfits, parfile)


%%
P01 = 4.493*At0;
P02 = 3.935*At0;
MO.useIntermediate = 1;
par = [ts, 0.015, 1, 40, 1, 4.2e-4, P01, M01, P02, M02];
ifit = [3:4 7:10];
parfile = ['delayedMaturationDistrubutedDelay_ts' num2str(ts) '_v0.015_dtau1'];
[dist, distI, distM] = afitround(MO, par, ifit, nrfits, parfile)

P01 = 4.493*At0;
P02 = 3.935*At0;
MO.useIntermediate = 1;
par = [ts, 0.015, 1, 40, 5, 4.2e-4, P01, M01, P02, M02];
ifit = [3:4 7:10];
parfile = ['delayedMaturationDistrubutedDelay_ts' num2str(ts) '_v0.015_dtau5'];
[dist, distI, distM] = afitround(MO, par, ifit, nrfits, parfile)

P01 = 4.493*At0;
P02 = 3.935*At0;
MO.useIntermediate = 1;
par = [ts, 0.015, 1, 40, 10, 4.2e-4, P01, M01, P02, M02];
ifit = [3:4 7:10];
parfile = ['delayedMaturationDistrubutedDelay_ts' num2str(ts) '_v0.015_dtau10'];
[dist, distI, distM] = afitround(MO, par, ifit, nrfits, parfile)

P01 = 4.493*At0;
P02 = 3.935*At0;
MO.useIntermediate = 1;
par = [ts, 0.015, 1, 40, 15, 4.2e-4, P01, M01, P02, M02];
ifit = [3:4 7:10];
parfile = ['delayedMaturationDistrubutedDelay_ts' num2str(ts) '_v0.015_dtau15'];
[dist, distI, distM] = afitround(MO, par, ifit, nrfits, parfile)

P01 = 4.493*At0;
P02 = 3.935*At0;
MO.useIntermediate = 1;
par = [ts, 0.015, 1, 40, 20, 4.2e-4, P01, M01, P02, M02];
ifit = [3:4 7:10];
parfile = ['delayedMaturationDistrubutedDelay_ts' num2str(ts) '_v0.015_dtau20'];
[dist, distI, distM] = afitround(MO, par, ifit, nrfits, parfile)

P01 = 4.493*At0;
P02 = 3.935*At0;
MO.useIntermediate = 1;
par = [ts, 0.015, 1, 40, 30, 4.2e-4, P01, M01, P02, M02];
ifit = [3:4 7:10];
parfile = ['delayedMaturationDistrubutedDelay_ts' num2str(ts) '_v0.015_dtau30'];
[dist, distI, distM] = afitround(MO, par, ifit, nrfits, parfile)
%%
MO.useIntermediate = 1;
ts = tsv(its);
nrfits = nrFitvec(its);
At0 = MO.getSurf(ts);
P01 = 0*4.493*At0;
M01 = 4.79*At0;
P02 = 0*3.935*At0;
M02 = 9.048*At0;
par = [ts, 0.015,  1.136, 40, 10, 4.2e-04, P01, M01, P02, M02];
ifit = [3:5 8 10];
parfile = ['delayedMaturationDistrubutedDelay_noPrecursor_ts' num2str(ts) '_v0.015'];
afitround(MO, par, ifit, nrfits, parfile);

end


