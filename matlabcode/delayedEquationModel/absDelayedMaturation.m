%%
% abstract Class for delayed pore maturation
%
classdef absDelayedMaturation < handle
    properties (Access = protected)
        dataA; %data structure containing array of all data
    end
    properties
        name;                % name of the model. This is the name of the class
        indir;               % input directory of the data
        outdir;              % output directory of the data
        dataF; %data structure containing data to be fitted
        useSpline;           % use spline fit to surface area or combination of exponential and linear function
        useStd = 0;          % scale each data point with its std
        useIntermediate = 1; % if 0 fit only mature pore
        LB;                  % Lower boundary for fitting data
        HB;                  % Higher boundary for fitting data
        parnames;            % name of parameters
        N;                   % number of variables
    end
    methods (Abstract)
        [soli, solo] = solve(MO, par, timeint) %implements the solver of the model
        [obs] = getObservable(MO,sol)          %implements the observable of the model (intermediates and mature pores)
        [prodRate, maturePM] = getProdRate(MO, par, timeint) 
    end
    
    methods
        %%
        % absDelayedMaturation(surf, useSpline, force)
        % constructor
        % surf: gives which surface data should be used. 24h, or 2h
        % useSpline: specify if spline fit of surface is used
        % force: force reading in of input data
        function MO = absDelayedMaturation(surf, useSpline, force)
            if nargin < 1
                surf = '24h';
            end
            if nargin < 2
                MO.useSpline = 0;
            else
                MO.useSpline = useSpline;
            end
            
            if nargin < 3
                force = 0;
            end
            MO.dataA = MO.dataIn(surf, force);
        end
        
        %%
        % fitModel(ifit, par)
        % Run fit of model
        % ifit: index of parameter(s) to fit
        % par: parameter values
        function [parf, norm] = fitModel(MO, ifit, par, options)
            if nargin < 4
                options = optimset('Display', 'iter');
            end
            lb = MO.LB(ifit);
            hb = MO.HB(ifit);
            
            [parf, norm] = lsqnonlin(@MO.distMO, par(ifit), lb, hb, ...
                options, ifit, par);
        end
        
        
        %%
        % distMO(MO, parf, ifit, par)
        % compute vector of distance model to data
        % parf: value of parameters to be fitted
        % ifit: index of parameters to be fitted
        % par: vector containing all parameters
        function [dist, distI, distM] = distMO(MO, parf, ifit, par)
            parloc = par;
            parloc(ifit) = parf;
            [soli, solo] = solve(MO, parloc, [par(1) 120]);
            A = MO.getSurf(MO.dataF(1).time);
            yi = deval(soli, MO.dataF(1).time);
            yo = deval(solo, MO.dataF(1).time);
            obsi = MO.getObservable(yi);
            obso = MO.getObservable(yo);
            
            distI =  obsi(:,1)./A - MO.dataF(1).core_i(:,1);
            distI = [distI; obso(:,1)./A - MO.dataF(1).core_o(:,1)];
            
            distM = obsi(:,2)./A - MO.dataF(2).core_i(:,1);
            distM = [distM; obso(:,2)./A - MO.dataF(2).core_o(:,1)];
            
            if MO.useIntermediate
                dist = [distI;distM];
                stdVec = [MO.dataF(1).core_i(:,2);MO.dataF(1).core_o(:,2);MO.dataF(2).core_i(:,2);MO.dataF(2).core_o(:,2)];
            else
                dist = [distM];
                stdVec = [MO.dataF(2).core_i(:,2);MO.dataF(2).core_o(:,2)];
            end
            
            if MO.useStd
                distI = distI./stdVec(1:length(distI));
                distM = distM./stdVec(length(distI)+1:length(distM));
                dist = dist./stdVec;
            else
                distI= distI/mean(stdVec(1:length(distI)));
                if length(stdVec) < length(distI)+length(distM)
                    distM = distM/mean(stdVec);
                else
                    
                    distM = distM/mean(stdVec(length(distI)+1:end));
               
                end
                dist = dist/mean(stdVec);
            end
        end
        
        %%
        % runFit(par, ifit, maxNrfit, fact, outfile)
        % performs multiple fits and write out the results
        % par: Values of all paramaters
        % ifit: indexes of parameters to fit
        % maxNrfit: maximal number of rounds
        % fact: set the initial values of the parameters to fit
        % (randomly distributed par_ini +- fact*rand*par_ini)
        % outfile: name of output file
        function [par, norm] = runFit(MO, par, ifit, maxNrfit, fact, outfile)
            try
                
                parFileName = fullfile(MO.outdir, outfile); % full output file name
                
                try
                    parfitT = load(parFileName);
                    inifit = size(parfitT,1);       %where to start the fitting procedure
                    fid = fopen(parFileName, 'a');
                catch
                    fid = fopen(parFileName, 'w');
                    fprintf(fid,'%% Model %s\n',MO.name);
                    fprintf(fid,'%% N %d\n', MO.N);
                    parnamesA = strsplit( MO.parnames, ' ');
                    fprintf(fid, '%% useStd %d useIntermediate %d useSpline %d\n', MO.useStd, MO.useIntermediate, MO.useSpline );
                    fprintf(fid, '%% fittedIds %s\n', num2str(ifit));
                    fprintf(fid, '%% fittedPar %s\n', strjoin(parnamesA(ifit), ' '));
                    fprintf(fid, '%% factParvariability %.2f\n', fact);
                    fprintf(fid, '%% %s\tnorm\tnrpara\n', strjoin(parnamesA,'\t'));
                    inifit = 0;
                end
                
                for nrfit = inifit+1:maxNrfit
                    parfit = par;
                    parfit(ifit) = parfit(ifit)+ fact*parfit(ifit).*(1-2*rand(1,length(ifit)));
                    parfit = (parfit > MO.HB).*(MO.HB) + (parfit <= MO.LB).*(MO.LB) + (parfit <= MO.HB).*(parfit > MO.LB).*parfit;
                    [parf, norm] = MO.fitModel(ifit, parfit, optimset('Display', 'iter'));
                    parfit(ifit) = parf;
                    for i=1:length(parfit)-1
                        fprintf(fid, '%.3e\t', parfit(i));
                    end
                    fprintf(fid, '%.3e\t%.3e\t%d\n', parfit(end), norm, length(ifit));    
                end
                fclose(fid);
            catch
                fclose(fid);
            end
            fclose all;
        end
        
        %%
        % par: parameter values
        % ifit: indexes of parameters to fit
        % iC: index of paramater to systematically vary
        % bound: Boundary where to search
        % step: number of steps
        % outfile: output file
        %%
        function findCI(MO, par, ifit, iC, step, outfile, bounds)
            NrDataPts = size(MO.dataF(1).time,1)*2;
            if nargin < 7
            %% find boundary of parameter values
                options = optimset('Display', 'iter', 'MaxFunEvals', 100)
                
                
                parloc = par;
                [parf, norm] =  MO.fitModel( ifit, parloc);
                parL = par(iC)/20;
                parH = par(iC);
                parloc(iC) = parL;
                [parf, normL] =  MO.fitModel(ifit, parloc, options);
                logproL = MO.LogPro(normL, norm, NrDataPts);
                if (abs(logproL) > chi2inv(0.95, 1))  
                    while abs(parL - parH) > par(iC)/5
                        parL2 = (parH + parL)/2;
                        parloc(iC) = parL2;
                        [parf, normL] =  MO.fitModel(ifit, parloc, options);
                        logproL = MO.LogPro(normL, norm, NrDataPts)
                        if abs(logproL) < chi2inv(0.95, 1)
                            parH = parL2;
                        else
                            parL = parL2;
                        end
                    end
                else
                    lowPar = parL;
                end
                lowPar = parL;
                parL = par(iC);
                parH = 20*par(iC);
                parloc(iC) = parH;
                [parf, normL] =  MO.fitModel(ifit, parloc, options);
                logproL = MO.LogPro(normL, norm, NrDataPts);
                if abs(logproL) > chi2inv(0.95, 1)
                    while abs(parL - parH) > par(iC)/5
                        parH2 = (parH + parL)/2;
                        parloc(iC) = parH2;
                        [parf, normL] =  MO.fitModel(ifit, parloc, options);
                        logproL = MO.LogPro(normL, norm, NrDataPts)
                        if abs(logproL) < chi2inv(0.95, 1)
                            parL = parH2;
                        else
                            parH = parH2;
                        end
                    end
                else
                    highPar = parH;    
                end
                highPar = parH; 
            else
                lowPar = bounds(1);
                highPar = bounds(2);
            end
            lowPow = floor(log10(lowPar/par(iC))*10)/10;
            highPow = ceil(log10(highPar/par(iC))*10)/10;
            parRange = par(iC)*10.^[lowPow*[step:-1:1]/step highPow*[0:step]/step];
            %try to find lower border
            for i = 1:length(parRange)
                parloc = par;
                parloc(iC) = parRange(i);
                MO.runFit(parloc, ifit, i, 0, outfile);
            end
            
            pCI = load(fullfile(MO.outdir, outfile));
            %%
            CI = pCI(:,iC).*(MO.LogPro(pCI(:,end-1), min(pCI(:,end-1)), NrDataPts)<chi2inv(0.95,1));
            CITM = (1./pCI(:,3) + pCI(:,4)).*(MO.LogPro(pCI(:,end-1), min(pCI(:,end-1)), NrDataPts)<chi2inv(0.95,1))
            CI = [min(CI(CI>0)) max(CI(CI>0))]
            CITM = [min(CITM(CITM>0)) max(CITM(CITM>0))]
        end
        
        function logPro = LogPro(MO, norm1,norm2, nrpts)
           logPro = nrpts*log(norm1/nrpts) - nrpts*log(norm2/nrpts);
        end
        
        function par = getBestfit(MO, filename)
            try
                parV = load(filename);
            catch
                error(sprintf('could not load %s\n', filename));
            end
            if parV(1, end) == parV(2,end)
                [val, ip] = min(parV(:,end-1));
                par = parV(ip, 1:end-2);
            else
                [val, ip] = min(parV(:,end));
                par = parV(ip, 1:end-1);
            end
        end
        
        function Den = writeDensities(MO, par, outfile)
            timeplot = [par(1):0.1:120]';
            A = MO.getSurf(timeplot);
            [soli, solo] = solve(MO, par,[par(1) 120]);
            yi = deval(soli, timeplot);
            yo = deval(solo, timeplot);
            obsi = MO.getObservable(yi);
            obso = MO.getObservable(yo);
            Den = [timeplot obsi(:,1)./A  obsi(:,2)./A obso(:,1)./A  obso(:,2)./A];
            
            fid = fopen(outfile, 'w');
            if fid > 0
                fprintf(fid,'%s\t%s\t%s\t%s\t%s\n','time', 'inter_i', 'mature_i', 'inter_o', 'mature_o')
                for i=1:length(timeplot)
                    fprintf(fid, '%d\t%.2e\t%.2e\t%.2e\t%.2e\n',Den(i,1),  Den(i,2),Den(i,3),Den(i,4),Den(i,5));
                end
                fclose(fid);
            else
                error('cant open outputfile');
            end
        end
        
        function writeProdRate(MO, par, outfile, timeint)
            if nargin < 4
                timeint = [10:0.1:120]';
            end
            [prodrate, maturePM] = MO.getProdRate(par, timeint);
            fid = fopen(outfile, 'w');
            fprintf(fid, 'time\tProdDensity_inner\tProdDensity_outer\n');
            timeint = [0;9.95;10; timeint];
            prodrate =  [[0;0;par(end-3)/0.1/MO.getSurf(10); prodrate(:,1)] [0;0;par(end-1)/0.1/MO.getSurf(10); prodrate(:,2)] ];
            for i=1:length(timeint)
                fprintf(fid, '%.2f\t%.3e\t%.3e\n',timeint(i), prodrate(i,1), prodrate(i,2) );
            end
            fclose(fid);
        end
        
        function writeFractionMatureIP(MO, par, outfile, timeint)
            if nargin < 4
                timeint = [10:0.1:1200]';
            end
            [soli, solo] = MO.solve(par, [par(1) timeint(end)]);
            [prodRate, maturePM] = MO.getProdRate(par, timeint);
            A = MO.getSurf(timeint');
            yi = deval(soli, timeint);
            yo = deval(solo, timeint);
            maturei = (yi(3,:)./A)';
            matureo = (yo(3,:)./A)';
            fid = fopen(outfile, 'w');
            fprintf(fid, 'time\tFractionMatureIpInner\tFractionMatureIpOuter\n');
            for i=1:length(timeint)
                fprintf(fid, '%.2f\t%.3e\t%.3e\n',timeint(i), (maturei(i)- maturePM(i,1))/maturei(i), ...
                    (matureo(i) - maturePM(i,2))/matureo(i) );
            end
            fclose(fid)
        end
            
        %%
        function  plotBestfit(MO, filename, ifig, save)
            if nargin < 3
                ifig = 1;
            end
            MO.plotResult(MO.getBestfit(filename), ifig);
            if save
                [pathstr, filen, exte] = fileparts(filename);
                saveas(ifig, fullfile(pathstr, [filen '_inner.pdf']));
                saveas(ifig+1, fullfile(pathstr, [filen '_outer.pdf']));
            end
        end
        %%
        % plot the model and data given the paramter par
        function [par, norm] = plotResult(MO, par, ifig)
            if nargin < 3
                ifig = 1;
            end
            set(0,'DefaultTextInterpreter','none');
            set(0,'defaultAxesFontName', 'Arial');
            [soli, solo] = solve(MO, par,[par(1) 1200]);
            
            timeplot = [par(1):1:120 ]';
            A = MO.getSurf(timeplot);
            yi = deval(soli, timeplot);
            yo = deval(solo, timeplot);
            obsi = MO.getObservable(yi);
            obso = MO.getObservable(yo);
            barwidth = 1;
            figsize = [550 350]/2;
            Mpc = [30 71 126]/255;
            mpc = [237 30 36]/255;
            Mpc_mod = Mpc;
            mpc_mod = mpc;
            %%
            setupFigure(ifig, [50 600 figsize],'on', 'pixels', 8);
            plot(timeplot, obsi(:,1)./A, '--', 'LineWidth', 2, 'Color', mpc_mod )
            plot(timeplot, obsi(:,2)./A, '--', 'LineWidth', 2, 'Color', Mpc_mod );
            hb = bar(MO.dataF(1).time, [MO.dataF(2).core_i(:,1) MO.dataF(1).core_i(:,1) ], 'BarWidth', barwidth)
            xlim([0 120]);
            ylim([0 13]);
            set(hb(2), 'FaceColor', mpc, 'EdgeColor', mpc)
            set(hb(1), 'FaceColor', Mpc, 'EdgeColor', Mpc)
            xlabel('Time after AO (min)')
            ylabel('Density (pores/um2)')
            set(gca, 'YTick', [0 4 8 12], 'XTick', [0:20:120], 'TickDir','out', 'LineWidth', 1.5,'FontName', 'Arial')
            
            setupFigure(ifig+1, [50 600 figsize],'on', 'pixels', 8);
            outerD =  [obso(:,1)./A  obso(:,2)./A];
            plot(timeplot, obso(:,1)./A, '--', 'LineWidth', 2, 'Color', mpc_mod )
            plot(timeplot, obso(:,2)./A, '--', 'LineWidth', 2, 'Color', Mpc_mod );
            hb = bar(MO.dataF(1).time, [MO.dataF(2).core_o(:,1) MO.dataF(1).core_o(:,1) ], 'BarWidth', barwidth)
            xlim([0 120]);
            ylim([0 13]);
            set(hb(2), 'FaceColor', mpc, 'EdgeColor', mpc)
            set(hb(1), 'FaceColor', Mpc, 'EdgeColor', Mpc)
            xlabel('Time after AO (min)')
            ylabel('Density (pores/um2)')
            set(gca, 'YTick', [0 4 8 12], 'XTick', [0:20:120], 'TickDir','out', 'LineWidth', 1.5,'FontName', 'Arial')

        end
        function dataA = dataIn(MO, surf, force)
            %read in mature and pre
            if nargin < 1
                force = 0;
            end
            if ispc
                indir = fullfile([getenv('HOMEDRIVE') getenv('HOMEPATH') ], 'Dropbox', 'NPCMaturation',...
                    'expdata', 'emdata');
                outdir = fullfile([getenv('HOMEDRIVE') getenv('HOMEPATH') ], 'Dropbox', 'NPCMaturation',...
                    'results', 'emdata');
            end
            if ismac
                indir = fullfile(getenv('HOME'), 'Dropbox', 'NPCMaturation',...
                    'expdata', 'emdata');
                outdir = fullfile(getenv('HOME'), 'Dropbox', 'NPCMaturation',...
                    'results', 'emdata');
            end
            MO.indir = indir;
            MO.outdir = outdir;
            matfile = fullfile(MO.indir,['emdata' surf '.mat']);
            if exist(matfile) && ~force
                load(matfile)
                return
            end
            % first entry is minipore second entry is mature pore
            data = struct('time', [], 'd_noncore', [], 'd_core_i', [], 'd_core_o', [], 't_core_i', [], 't_core_o', []);
            dataA = [data; data];
            xlsfile = fullfile(indir, 'Minipore-number-to-Antonio-141022.xlsx');
            dataA(2).time = xlsread(xlsfile, 'Onlyvalues', 'C4:C15');
            dataA(2).t_noncore = xlsread(xlsfile, 'Onlyvalues', 'D4:E15');
            dataA(2).t_core_i = xlsread(xlsfile, 'Onlyvalues', 'F4:G15');
            dataA(2).t_core_o = xlsread(xlsfile, 'Onlyvalues', 'H4:I15');
            dataA(2).d_noncore = xlsread(xlsfile, 'Onlyvalues', 'D20:E31');
            dataA(2).d_core_i = xlsread(xlsfile, 'Onlyvalues', 'F20:G31');
            dataA(2).d_core_o = xlsread(xlsfile, 'Onlyvalues', 'H20:I31');
            
            dataA(1).time = xlsread(xlsfile, 'Onlyvalues', 'K4:K15');
            dataA(1).t_noncore = xlsread(xlsfile, 'Onlyvalues', 'L4:M15');
            dataA(1).t_core_i = xlsread(xlsfile, 'Onlyvalues', 'N4:O15');
            dataA(1).t_core_o = xlsread(xlsfile, 'Onlyvalues', 'P4:Q15');
            dataA(1).d_noncore = xlsread(xlsfile, 'Onlyvalues', 'L20:M31');
            dataA(1).d_core_i = xlsread(xlsfile, 'Onlyvalues', 'N20:O31');
            dataA(1).d_core_o = xlsread(xlsfile, 'Onlyvalues', 'P20:Q31');
            
            %% read and create surface area
            switch surf
                case '24h'
                    surfdatafile = fullfile(MO.indir, 'surfaceNucleus_24h_new.txt');
                    surfdata = load(surfdatafile);
                    dataA(1).surf = [surfdata(:,1)*60  surfdata(:,2:3)];
                    dataA(1).surf = [0 surfdata(1,2:3); dataA(1).surf];
                    
                    % perform a spline fit to smooth the data
                    dataA(1).surfspl = surfSpline( dataA(1).surf(:,1),  dataA(1).surf(:,2),7, 3, 8);
                    dataA(1).dsurfspl = fnder(dataA(1).surfspl, 1);
                    dataA(1).surfPar = load(fullfile(MO.indir, 'surfaceNucleusFit_24h_new.txt'));
                case '2h'
                    surfdatafile = fullfile(MO.indir, 'surfaceNucleus_2h.txt');
                    surfdata = load(surfdatafile);
                    dataA(1).surf = [surfdata(2:end,1)  surfdata(2:end,2:3)];
                    dataA(1).surf = [[0:1:surfdata(2,1)-1]' repmat(surfdata(2,2:3),7,1) ; dataA(1).surf];
                    % perform a spline fit to smooth the data
                    dataA(1).surfspl = surfSpline(dataA(1).surf(:,1),  dataA(1).surf(:,2),15, 3, 8);
                    dataA(1).dsurfspl = fnder(dataA(1).surfspl, 1);
                    dataA(1).surfPar = load(fullfile(MO.indir, 'surfaceNucleusFit_2h.txt'));
                otherwise
                    error('Usage class(surf, force, showplot), surf= 2h or 24h string')
            end
            save(matfile, 'dataA');
        end
        
        
        function [A, dAdt, kg] = getSurf(MO, time)
            if MO.useSpline
                A = fnval(MO.dataF(1).surfspl, time);
                dAdt = fnval(MO.dataF(1).dsurfspl, time);
                kg = dAdt./A;
            else
                par = struct('a0', MO.dataA(1).surfPar(1), 'a1',  MO.dataA(1).surfPar(2), ...
                    'kg1', MO.dataA(1).surfPar(3),  'kg2', MO.dataA(1).surfPar(4), 'ts',  MO.dataA(1).surfPar(5));
                A = par.a0 + par.a1*(1-exp(-par.kg1*(time-par.ts))) + par.kg2*(time-par.ts);
                dAdt = (par.a1*par.kg1*exp(-par.kg1*time)+par.kg2).*(time>=par.ts);
                kg = dAdt./A;
            end
        end
        
    end
end
