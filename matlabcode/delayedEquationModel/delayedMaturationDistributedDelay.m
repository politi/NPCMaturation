%%
% dealyedMaturation model with production proportional to free area
% V = v*A.
% delay is uniform distributed along tauM1 and tauM2
% par(1): ts, start of interphase assembly 
% par(2): v, maximal rate of interphase assembly
% par(3): kM, initiation of maturation
% par(4): tauM1, lowest maturation time 
% par(5): DtauM, tauM1 + DtauM gives the highest maturation time tauM2
% par(6): kd, decay of Mature pore
% par(7): number of minipores at ts inner core
% par(8): number of mature pores at ts inner core
% par(9): number of minipores at ts outer core
% par(10): number of mature pores at ts outer core

classdef delayedMaturationDistributedDelay < absDelayedMaturation
   methods 
        function MO = delayedMaturationDistributedDelay(surf, useSpline, force)
            MO@absDelayedMaturation(surf, useSpline, force)
            MO.dataF = MO.dataA;
            for i =1:2
                MO.dataF(i).noncore = MO.dataF(i).d_noncore;
                MO.dataF(i).core_i = MO.dataF(i).d_core_i;
                MO.dataF(i).core_o = MO.dataF(i).d_core_o;
            end
            MO.parnames = 'ts v kM tauM dtauM kd P01 M01 P02 M02';
            MO.LB = [0,  0,  0.1, 12, 0.1, 0,  0,    0,    0,    0]; %Low boundary values
            MO.HB = [15, 1,  10,  70, 30,  1,  5000, 5000, 5000, 5000]; %High boundary values
            MO.N = 3;
            MO.name = mfilename('class')
        end
        
        
       function obs = getObservable(MO,sol)
            obs(:,1) = sol(1,:)'+ sol(2,:)';
            obs(:,2) =  sol(3,:)';
        end
        %%
        % solve equations for one set of parameters and provide the 
        function [soli, solo] = solve(MO, par, timeint)
            prei = ode15s(@MO.prec, timeint, par(7),odeset, par);
            soli = ode15s(@MO.mat, timeint, [par(7) 0 par(8)], odeset, par, prei);
            
            preo = ode15s(@MO.prec,timeint, par(9), odeset, par);  
            solo = ode15s(@MO.mat, timeint, [par(9) 0 par(10)], odeset, par, preo);
            
        end
        
        
     
        %%
        % get production rate of minipores and mature pores coming from PM
        % only
        function [prodRate, maturePM] = getProdRate(MO, par, timeint) 
            A = MO.getSurf(timeint);
            prodRate = [par(2)*(timeint>=par(1)) par(2)*(timeint>=par(1))];
            maturePM = [par(8)*exp(-par(6)*(timeint - par(1)))./A par(10)*exp(-par(6)*(timeint - par(1)))./A];
        end

        function dydt = prec(MO,t,y, par)
            if t >= par(1)
                v = par(2);
            else
                v = 0;
            end
            A = MO.getSurf(t);
            dydt = [v*A - par(3)*y(1)];
        end
        
        function p = precA(MO, t, par)
           p = par(2)/par(3)-(par(2)/par(3)-par(7))*exp(-par(3)*(t-par(1)));
        end
        
        function dydt = mat(MO,t,y, par, pre)
            if t >= par(1)
                v = par(2);
            else
                v = 0;
            end
            if t < par(4) + par(1)
                dPre = 0;
            end
            
            
            %% Analytical solution of simplest integral without area effect
%             if t >= par(4) + par(1) && t < par(5) + par(4) + par(1)
%                 dPre= 1/par(5)*(par(2)*(t-par(4)-par(1))+par(7) - MO.precA(t-par(4), par));
%             end
%             if t >= par(5) + par(4) + par(1)
%                 dPre= 1/par(5)*(par(2)*par(5) + MO.precA(t-par(4)-par(5),par)- MO.precA(t-par(4), par));
%             end
            A = MO.getSurf(t);
            tauM2 = par(4) + par(5)/2;
            tauM1 = par(4) - par(5)/2;
            if t >= tauM1 + par(1) && t < tauM2 + par(1)
                
                pre4 = deval(pre, t - tauM1 );
                pre5 = deval(pre, par(1));
                %compute difference of integrals in surface
                dPre = 1/par(5)*(pre5-pre4 + v*integral(@MO.getSurf, par(1), t-tauM1));
            end
            if t >= tauM2 + par(1)
                pre4 = deval(pre, t-tauM1);
                pre5 = deval(pre, t-tauM2);
                dPre = 1/par(5)*(pre5 - pre4 + v*integral(@MO.getSurf, t-tauM2, t-tauM1));
            end
            dydt = [v*A - par(3)*y(1)
                par(3)*deval(pre,t) - dPre
                dPre - par(6)*y(3)];
            
        end
            

        
        function [par, norm] = testFit(MO)
            ts = 10;
            At0 = MO.getSurf(ts);
            P01 = 4.493*At0;
            M01 = 4.79*At0;
            P02 = 3.935*At0;
            M02 = 9.048*At0;
            if nargin < 2
                par = [ts, 0.014,  1.136, 40, 4.2e-04, P01, M01, P02, M02];
            end
            ifit = [3:4 6:9];
            [parf, norm] = MO.fitModel(ifit, par)
            
            par(ifit) = parf;
            MO.plotResult(par)
        end
            
   end
end