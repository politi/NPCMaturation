%%
% linearChainApproximation model with production proportional to free area
% linear steps
% V = v*A
% par(1): ts, start of interphase assembly 
% par(2): v, maximal rate of interphase assembly
% par(3): mean rate constant 
% par(4): std of rate constants
% par(5): kd, degradation rate constant of mature pores
% par(5): number of minipores at ts inner core
% par(6): number of mature pores at ts inner core
% par(7): number of minipores at ts outer core
% par(8): number of mature pores at ts outer core

classdef linearChainApproximation < absDelayedMaturation
    methods 
        function MO = linearChainApproximation( surf, useSpline, force, N)
            MO@absDelayedMaturation(surf, useSpline, force)
            MO.dataF = MO.dataA;
            for i =1:2
                MO.dataF(i).noncore = MO.dataF(i).d_noncore;
                MO.dataF(i).core_i = MO.dataF(i).d_core_i;
                MO.dataF(i).core_o = MO.dataF(i).d_core_o;
            end
            MO.parnames = 'ts v k stdk kd P01 M01 P02 M02';
            MO.LB = [0,  0,  0.01, 0,  0,    0,    0,    0,    0]; %Low boundary values
            MO.HB = [15, 1,  10,   10, 1,    5000, 5000, 5000, 5000]; %High boundary values
            MO.N = N;
        end
        
        function obs = getObservable(MO, sol)
            obs(:,1) = sum(sol(1:end-1,:),1);
            obs(:,2) = sol(end,:);
        end
        %%
        % get production rate of minipores and mature pores coming from PM
        % only
        function [prodRate, maturePM] = getProdRate(MO, par, timeint) 
            A = MO.getSurf(timeint);
            prodRate = [par(2)*(timeint>=par(1)) par(2)*(timeint>=par(1))];
            maturePM = [par(7)*exp(-par(5)*(timeint - par(1)))./A par(9)*exp(-par(5)*(timeint - par(1)))./A];
        end
        
        function [soli, solo] = solve(MO, par, timeint)
            m = par(3);
            v = par(4);
            mu = log((m^2)/sqrt(v+m^2));
            sigma = sqrt(log(v/(m^2)+1));
            pval = [par(2) lognrnd(mu,sigma,1,MO.N-1) par(5)]; 
            soli = ode45(@MO.odeFun, timeint, [par(6) zeros(1,MO.N-2) par(7)], odeset, pval);  
            solo = ode45(@MO.odeFun, timeint, [par(8) zeros(1,MO.N-2) par(9)], odeset, pval); 
        end
        
        function dydt = odeFun(MO, t, y, par)
            A = MO.getSurf(t);
            M1 = diag(-1*ones(1,MO.N));
            M2 = diag(ones(1,MO.N),-1);
            M = M1*diag(par(2:end)) + M2(1:end-1, 1:end-1)*diag(par(2:end));
            
            dydt = M*y  + [par(1)*A zeros(1,MO.N-1)]';
        end
        
   
end
end