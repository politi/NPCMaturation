%%
%fit just Mature pore
clear;
clear all;
fclose all
MO = delayedMaturation('24h', 0,0)
MO.outdir = fullfile(MO.outdir, 'delay')
MO.useIntermediate = 0;
tsv = [5 7 10 12 15];
nrFitvec = [10 10 10 10 10];

%%
for its = 3 %1:length(tsv)
ts = tsv(its);
MO.useIntermediate = 0;
nrfits = nrFitvec(its);
At0 = MO.getSurf(ts);
P01 = 0*4.493*At0;
M01 = 4.79*At0;
P02 = 0*3.935*At0;
M02 = 9.048*At0;
MO.useIntermediate = 0;
par = [ts, 0.015,  1.136, 40, 4.2e-04, P01, M01, P02, M02];
ifit = [3:4 7 9];

parfile = ['delayedMaturation_MaturePore_noPrecursor_v0.015_ts' num2str(ts)]
[dist, distI, distM] = afitround(MO, par, ifit, nrfits, parfile)
%%
P01 = 4.493*At0;
P02 = 3.935*At0;
MO.useIntermediate = 0;
par = [ts, 0.015, 1, 40, 4.2e-4, P01, M01, P02, M02];
ifit = [3:4 6:9];
parfile = ['delayedMaturation_MaturePore_Precursor_v0.015_ts' num2str(ts)];
[dist, distI, distM] = afitround(MO, par, ifit, nrfits, parfile)

P01 = 4.493*At0;
P02 = 3.935*At0;
MO.useIntermediate = 0;
par = [ts, 0, 1, 40, 4.2e-4, P01, M01, P02, M02];
ifit = [3:4 6:9];
parfile = ['delayedMaturation_MaturePore_Precursor_v0_ts' num2str(ts)];
[dist, distI, distM] = afitround(MO, par, ifit, nrfits, parfile)

%%
P01 = 4.493*At0;
P02 = 3.935*At0;
MO.useIntermediate = 1;
par = [ts, 0.015, 1, 40, 4.2e-4, P01, M01, P02, M02];
ifit = [3:4 6:9];
parfile = ['delayedMaturation_v0.015_ts' num2str(ts)];
[dist, distI, distM] = afitround(MO, par, ifit, nrfits, parfile)
%%
P01 = 4.493*At0;
P02 = 3.935*At0;
MO.useIntermediate = 1;
par = [ts, 0, 1, 40, 4.2e-4, P01, M01, P02, M02];
ifit = [3:4 6:9];
parfile = ['delayedMaturation_v0_ts' num2str(ts)];
afitround(MO, par, ifit, nrfits, parfile);
%%
MO.useIntermediate = 1;
ts = tsv(its);
nrfits = nrFitvec(its);
At0 = MO.getSurf(ts);
P01 = 0*4.493*At0;
M01 = 4.79*At0;
P02 = 0*3.935*At0;
M02 = 9.048*At0;
par = [ts, 0.015,  1.136, 40, 4.2e-04, P01, M01, P02, M02];
ifit = [3:4 7 9];
parfile = ['delayedMaturation_noPrecursor_v0.015_ts' num2str(ts)];
afitround(MO, par, ifit, nrfits, parfile);




end



