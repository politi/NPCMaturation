%%
%fit just Mature pore
clear;
clear all;
fclose all
MO = delayedMaturationExponentialV('24h', 0,0)
MO.useIntermediate = 0;
tsv = [5 7 10 12 15];
nrFitvec = [5 5 32 5 5];
MO.outdir = fullfile(MO.outdir, 'delay')
%%
for its = 3 %1:length(tsv)
    %%
ts = tsv(its);
nrfits = nrFitvec(its);
At0 = MO.getSurf(ts);
P01 = 0*4.493*At0;
M01 = 4.79*At0;
P02 = 0*3.935*At0;
M02 = 9.048*At0;
MO.useIntermediate = 0;
par =  [ts, 2, 1, 0.015,  0.1, 40, 4.2e-04, P01, M01, P02, M02];
ifit = [2:3 5:6 9 11];
parfile = ['delayedMaturationExponentialV_MaturePore_noPrecursor_ts' num2str(ts) '_v0.015']
afitround(MO, par, ifit, nrfits, parfile);
%%
P01 = 4.493*At0;
P02 = 3.935*At0;
MO.useIntermediate = 0;
par =  [ts, 2, 0.1, 0.015,  0.1, 40, 4.2e-04, P01, M01, P02, M02];
ifit = [2:3 5:6 8:11];
parfile = ['delayedMaturationExponentialV_MaturePore_Precursor_ts' num2str(ts) '_v0.015'];
afitround(MO, par, ifit, nrfits, parfile);
%%
P01 = 4.493*At0;
P02 = 3.935*At0;
MO.useIntermediate = 1;
par =  [ts, 2, 10, 0.015,  0.1, 40, 4.2e-04, P01, M01, P02, M02];
ifit = [2:3 5:6 8:11];
parfile = ['delayedMaturationExponentialV_ts' num2str(ts) '_v0.015'];
afitround(MO, par, ifit, nrfits, parfile);
%%
MO.useIntermediate = 1;
ts = tsv(its);

At0 = MO.getSurf(ts);
P01 = 0*4.493*At0;
M01 = 4.79*At0;
P02 = 0*3.935*At0;
M02 = 9.048*At0;
par =  [ts, 8.993, 2, 0.015,  2, 20, 4.2e-04, P01, M01, P02, M02];
ifit = [2:3 5:6 9 11];
parfile = ['delayedMaturationExponentialV_noPrecursor_ts' num2str(ts) '_v0.015'];
[dist, distI, distM] = afitround(MO, par, ifit, 35, parfile)
end


