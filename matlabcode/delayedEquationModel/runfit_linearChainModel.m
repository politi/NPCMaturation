clear;
clear all;
fclose all
ts = 10;

MO = linearChainApproximation('24h', 0, 0, 5);
At0 = MO.getSurf(ts);
P01 = 4.493*At0;
M01 = 4.79*At0;
P02 = 3.935*At0;
M02 = 9.048*At0;

Nval = [5 10 20 30 40 50 100];
nrfits = 10;
%%
for its = 1:length(Nval)
    parfile = ['linearChainApproximation_N' num2str(Nval(its))];
    MO = linearChainApproximation('24h', 0, 0, Nval(its));
    MO.useIntermediate = 1;
    par = [ts, 0.014, Nval(its)/42, 0,  4.2e-4, P01, M01, P02, M02];
    ifit = [2:3 length(par)-3:length(par)];
    MO.outdir = fullfile(MO.outdir, 'linearChain');
    [dist, distI, distM] = afitround(MO, par, ifit, nrfits, parfile)
 end

%%
clear;
clear all;
fclose all
ts = 10;
MO = linearChainApproximation('24h', 0, 0, 5);
At0 = MO.getSurf(ts);
P01 = 4.493*At0;
M01 = 4.79*At0;
P02 = 3.935*At0;
M02 = 9.048*At0;
par = [ts, 0.015, 0.25,  0, 4.2e-4, P01, M01, P02, M02];
ifit = [3 length(par)-3:length(par)];
Nval = [5 10 20 30 40 50 100];
nrfits = 3;
%%
for its = 1:length(Nval)
    parfile = ['linearChainApproximation_N' num2str(Nval(its)) '_v0.015'];
    MO = linearChainApproximation('24h', 0, 0, Nval(its));
    MO.useIntermediate = 1;
    MO.outdir = fullfile(MO.outdir, 'linearChain');
    [dist, distI, distM] = afitround(MO, par, ifit, nrfits, parfile)
end