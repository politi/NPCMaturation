%%
% dealyedMaturation model with production proportional to free area
% V = v*A
% par(1): ts, start of interphase assembly 
% par(2): v, maximal rate of interphase assembly
% par(3): kM, initiation of maturation
% par(4): tauM, maturation time
% par(5): kd, decay of Mature pore
% par(6): number of minipores at ts inner core
% par(7): number of mature pores at ts inner core
% par(7): number of minipores at ts outer core
% par(8): number of mature pores at ts outer core

classdef delayedMaturation < absDelayedMaturation
   methods 
        function MO = delayedMaturation(surf, useSpline, force)
            MO@absDelayedMaturation(surf, useSpline, force)
            MO.dataF = MO.dataA;
            for i =1:2
                MO.dataF(i).noncore = MO.dataF(i).d_noncore;
                MO.dataF(i).core_i = MO.dataF(i).d_core_i;
                MO.dataF(i).core_o = MO.dataF(i).d_core_o;
            end
            MO.parnames = 'ts v kM tauM kd P01 M01 P02 M02';

            MO.LB = [0, 0,  0.001, 0, 0, 0, 0,0,0]; %Low boundary values
            MO.HB = [15, 1,  10, 70, 1, 10000, 10000, 10000, 10000]; %High boundary values
            MO.name = mfilename('class')
            MO.N = 3;
        end
        %%
        % solve equations for one set of parameters and provide the 
        function [soli, solo] = solve(MO, par, timeint)
            soli = dde23(@MO.dde,[par(4)],@MO.ddehist1, timeint, ddeset, par);  
            solo = dde23(@MO.dde,[par(4)],@MO.ddehist2, timeint, ddeset, par); 
        end
        
        %%
        % get production rate of minipores and mature pores coming from PM
        % only
        function [prodRate, maturePM] = getProdRate(MO, par, timeint) 
            A = MO.getSurf(timeint);
            prodRate = [par(2)*(timeint>=par(1)) par(2)*(timeint>=par(1))];
            maturePM = [par(7)*exp(-par(5)*(timeint - par(1)))./A par(9)*exp(-par(5)*(timeint - par(1)))./A];
        end
        
        function obs = getObservable(MO,sol)
            obs(:,1) = sol(1,:)'+ sol(2,:)';
            obs(:,2) =  sol(3,:)';
        end
        
        function s = ddehist1(MO,t,par)
            % Constant history function for DDEX1.
            s = [0, 0, par(7)];
            if t == par(1)
                s = [par(6), 0, par(7)];
            end
        end
        
        function s = ddehist2(MO,t,par)
            % Constant history function for DDEX1.
            s = [0, 0, par(9)];
            if t == par(1)
                s = [par(8), 0, par(9)];
            end
        end
        
        
        function dydt = dde(MO, t, y, Z, par)
            % par(1): ts, start of interphase assembly 
            % par(2): v, maximal rate of interphase assembly
            % par(3): kM, initiation of maturation
            % par(4): tauM, maturation time
            % par(5): kd, decay of Mature pore
            % par(6): number of minipores at ts
            % par(7): number of mature pores at 0
            % Delay Differential equations function for DDEX1.
            ylag = Z(:,1);
            if t > par(1)
                v = par(2);
            else
                v = 0;
            end
            A = MO.getSurf(t);
            dydt = [ v*A - par(3)*y(1)
                     par(3)*y(1) - par(3)*ylag(1)
                     par(3)*ylag(1) - par(5)*y(3)];
        end
        
        function [par, norm] = testFit(MO)
            ts = 10;
            At0 = MO.getSurf(ts);
            P01 = 4.493*At0;
            M01 = 4.79*At0;
            P02 = 3.935*At0;
            M02 = 9.048*At0;
            if nargin < 2
                par = [ts, 0.014,  1.136, 40, 4.2e-04, P01, M01, P02, M02];
            end
            ifit = [2:4 6:9];
            [parf, norm] = MO.fitModel(ifit, par)
            
            par(ifit) = parf;
            MO.plotResult(par)
        end
        

    
end
 
end