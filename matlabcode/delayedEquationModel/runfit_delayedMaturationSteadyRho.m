%%
%fit just Mature pore
clear;
clear all;
fclose all
MO = delayedMaturationSteadyRho('24h', 0,0)
MO.useIntermediate = 0;
tsv = [5 7 10 12 15];
nrFitvec = [10 10 10 10 10];
MO.outdir = fullfile(MO.outdir, 'delay')
%%
for its =3
ts = tsv(its);
nrfits = nrFitvec(its);
At0 = MO.getSurf(ts);
P01 = 0*4.493*At0;
M01 = 4.79*At0;
P02 = 0*3.935*At0;
M02 = 9.048*At0;
MO.useIntermediate = 0;
par = [ts, 0.1, 11,  1.136, 40, 4.2e-04, P01, M01, P02, M02];
ifit = [2:5 8 10];
parfile = ['delayedMaturationSteadyRho_MaturePore_noPrecursor_ts' num2str(ts)];
[dist, distI, distM] = afitround(MO, par, ifit, nrfits, parfile)

%%
P01 = 4.493*At0;
P02 = 3.935*At0;
MO.useIntermediate = 0;
par = [ts, 0.1, 11,  1.136, 40, 4.2e-04, P01, M01, P02, M02];
ifit = [2:5 7:10];
parfile = ['delayedMaturationSteadyRho_MaturePore_Precursor_ts' num2str(ts)];
[dist, distI, distM] = afitround(MO, par, ifit, nrfits, parfile)
%%
P01 = 4.493*At0;
P02 = 3.935*At0;
MO.useIntermediate = 1;
par = [ts, 0.1, 11,  1.136, 40, 4.2e-04, P01, M01, P02, M02];
ifit = [2:5 7:10];
parfile = ['delayedMaturationSteadyRho_ts' num2str(ts)];
afitround(MO, par, ifit, nrfits, parfile);

%%
MO.useIntermediate = 1;
ts = tsv(its);
nrfits = nrFitvec(its);
At0 = MO.getSurf(ts);
P01 = 0*4.493*At0;
M01 = 4.79*At0;
P02 = 0*3.935*At0;
M02 = 9.048*At0;
par = [ts, 0.1, 11,  1.136, 40, 4.2e-04, P01, M01, P02, M02];
ifit = [2:5 8 10];
parfile = ['delayedMaturationSteadyRho_noPrecursor_ts' num2str(ts)];
afitround(MO, par, ifit, nrfits, parfile);
end


%%
%%
%fit just Mature pore
clear;
clear all;
fclose all
%%
MO = delayedMaturationSteadyRho('24h', 0,0)
MO.useIntermediate = 0;
tsv = [5 7 10 12 15];
nrFitvec = [10 10 10 10 10];
MO.outdir = fullfile(MO.outdir, 'delay')
ts = 10;
parfile = ['delayedMaturationSteadyRho_noPrecursor_ts' num2str(ts)];
parfile = [parfile '.txt'];
%parf = MO.getBestfit(fullfile(MO.outdir, parfile));

%[soli, solo] = MO.solve(parf, [parf(1) 1200]);

%timeMature = [180:10:1200];
%A = MO.getSurf(timeMature);
%yi = deval(soli, timeMature);
%yo = deval(solo, timeMature);
%mean(yi(3,:)./A)

parf = MO.getBestfit(fullfile(MO.outdir, parfile));
[prodRate, maturePM] = MO.getProdRate(parf, [10:1:120]')


plot([0 9.85 9.95 10:1:120], [0;0;parf(7)/0.1/MO.getSurf(10);prodRate(:,1)]);
ylim([0,0.4])
