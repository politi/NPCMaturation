%% Script to batch process all data from Shotaro Otsuka and extract ACF and ACFs fits
maindir = 'W:\Shotaro\Live-cell-imaging\HeLa-FCS';
outfolder = 'W:\Shotaro\Live-cell-imaging\HeLa-FCS\ACF';
dataFolders = getAllFolders(maindir, '^\d+(_|-).+', 0);
%% these folder are messed up as the outer name changed!
%160706-HeLa-gfpNUP107z26z31
%161102-HeLa-gfpNUP214z2z12
%161111-HeLa-gfpNUP107z26z31
%171019-HeLa-gfpNUP205z81_gfpNUP107z26z31
%171026-HeLa-gfpNUP205z81_gfpNUP107z26z31
%171102-HeLa-gfpNUP62z199_gfpNUP107z26z31
%171108-HeLa-gfpNUP62z199_gfpNUP107z26z31
for iF = 2 %:length(dataFolders)
    
    folder = dataFolders{iF};
    resfiles = getAllFiles(folder, 'res', '2c_opt.res', 0);
    if strcmp(folder, 'W:\Shotaro\Live-cell-imaging\HeLa-FCS\171114-HeLa-gfpNUP205z81_gfpNUP107z26z31')
        resfiles = getAllFiles(fullfile(folder, 'Calibration_copied_removeGFPcell9'), 'res', '2c_opt.res', 0);
    end
    if strcmp(folder, 'W:\Shotaro\Live-cell-imaging\HeLa-FCS\160701-HeLa-gfpNUP107z26z31')
        resfiles = getAllFiles(fullfile(folder, '160701_gfpNUP107z26z31'), 'res', '2c_opt.res', 0);
    end
    
    if isempty(resfiles)
        resfiles = getAllFiles(fullfile(folder, 'Calibration'), 'res', '2c_opt.res', 0);
    end
    if isempty(resfiles)
        display(sprintf('%d %s\n', iF, folder))
        continue
    end
    
    %% Create outputs
    if ~exist(outfolder, 'dir')
        mkdir(outfolder)
    end
    [path, fname] = fileparts(folder);
    sprintf('Create output of ACF for %s', resfiles{1})
    writeCF2txt(resfiles, '2c_opt', fullfile(outfolder, [fname '_2c_opt']), 'Ch', 1, 'Chisq_lim', 1.5, 'Rsq_lim', 0.8);
    
end
%%
