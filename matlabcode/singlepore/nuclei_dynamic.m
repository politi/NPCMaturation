classdef nuclei_dynamic <  handle
    properties
        regions = 2;    % number of regions e.g. core, non-core
        ass_paths = 2;  % number of different assembly paths, e.g. interphase and postmitotic assembly
        MO;             % Model for single NPC assembly
        par_ini;        % A matrix. Column wise region (par_ini_reg1, par_ini_reg2, ...)
        par_kin;        % A matrix. Column wise type of kinetics (par_kin_type1, par_kin_type2, ...)
        frac;           % Fraction of different type of kinetics [[frac_typ1_region1;frac_typ2_regio1; ], [frac_typ1_region2;....]] 
        par_offset;     % An offset
        par_ini_hb;
        par_ini_lb;
        par_kin_hb;
        par_kin_lb;
        frac_lb;
        frac_hb;
        par_offset_lb;
        par_offset_hb;
        usestd = 0;
        name;
    end
    
    methods
        function ND = nuclei_dynamic(single_npc_model, par_ini, par_kin, frac, par_offset)
            assert(size(par_ini, 2) == ND.regions*ND.ass_paths, ...
                sprintf('Parameter for initialization dynamics (npar, ass_paths*regions). Model considers %d x %d = %d different pathways and regions ', ND.ass_paths, ND.regions, ND.ass_paths*ND.regions));
            assert(size(par_kin, 2) == ND.ass_paths, sprintf('Parameter for NUPs kinetics considers (npar, ass_paths). Model considers %d different kinetics', ND.ass_paths));
            assert(all(size(frac) == [ND.ass_paths, ND.regions]), sprintf('Fraction has size (nrkin, regions). Model considers %d different kinetics and %d regions', ND.ass_paths, ND.regions)); 
            if nargin > 4
                assert(size(par_offset, 2) == ND.regions, sprintf('Parameter for offset (npar, nrregions). Model considers %d different regions', ND.regions));
                ND.par_offset = par_offset;
            else
                ND.par_offset = zeros(1, ND.regions);
            end
            % Handle of class
            clsHdl = str2func(single_npc_model);
            
            % initiate class
            ND.MO = clsHdl(par_ini(:,1), par_kin(:,1));
            ND.name = ND.MO.name;
            ND.par_ini = par_ini;
            ND.par_kin = par_kin;
            ND.frac = frac;
            ND.par_ini_lb = repmat(ND.MO.par_ini_lb, 1,  ND.regions*ND.ass_paths);
            ND.par_ini_hb = repmat(ND.MO.par_ini_hb, 1,  ND.regions*ND.ass_paths);
            ND.par_kin_lb = repmat(ND.MO.par_kin_lb, 1,  ND.ass_paths);
            ND.par_kin_hb = repmat(ND.MO.par_kin_hb, 1,  ND.ass_paths);
                    % low and high boundary 
            ND.frac_lb = zeros(ND.ass_paths, ND.regions);
            ND.frac_hb = ones(ND.ass_paths, ND.regions);
            ND.par_offset_lb = zeros(ND.regions, 1);
            ND.par_offset_hb = 0.5*ones(ND.regions, 1);
        end
        
        function mat = offset(ND, time)
            % An offset to the data of the two regions. This can be a constant offset or a time dependent offset like kinetochore appearance
            for ir = 1:ND.regions
                mat(:,ir) = ND.par_offset(ir);
            end
        end
        
        function [ distvec, norm, tot_kin] = dist(ND, time, time_data, data)
            
            % Data is a vector containing region1, std1, region2, std2
            
            tot_kin = ND.nuclei_kin(time);
            % resample so that it matches the data points 
            tot_kin = tot_kin(ismember(time, time_data), :);
            % normalize the data
            for ir = 1:ND.regions
                tot_kin(:,ir) = tot_kin(:,ir)/tot_kin(end, ir);
            end
            distvec = [];
            if ND.usestd 
                for ir = 1:ND.regions
                    distvec = [distvec; (tot_kin(:,ir) - data(:, 1 + (ir-1)*ND.regions))./data(:,2 + (ir-1)*ND.regions)];
                end
            else
                for ir = 1:ND.regions
                    distvec = [distvec; (tot_kin(:,ir) - data(:, 1 + (ir-1)*ND.regions))];
                end
            end
             
            norm = sum(distvec.^2);
        end
        
        
        function [distvec, norm, tot_kin] = dist_par(ND, par, par_idx, time, time_data, data)
            
            par_vec = ND.get_parvec();
            par_vec(par_idx) = par;
            ND.set_par_fromvec(par_vec);
            
            % Data is a vector containing region1, std1, region2, std2
            [distvec, norm, tot_kin] = ND.dist(time, time_data, data);
           
        end
        
        
        function [tot_kin, kin_ini_conv_mom] = nuclei_kin(ND, time)
            % column wise time
            if size(time,2)> 1
                time = time';
            end
            dt = time(2)-time(1);
            offset = ND.offset(time);
            kin_ini_conv_mom = zeros(length(time), ND.ass_paths, ND.regions);
            tot_kin = zeros(length(time), ND.regions);
            for ir = 1:ND.regions
                for ip = 1:ND.ass_paths
                    ND.MO.setpar_ini(ND.par_ini(:, (ir-1)*ND.ass_paths + ip));
                    ND.MO.setpar_kin(ND.par_kin(:,ip));
                    kin_ini_conv_mom(:, ip, ir) = ND.MO.kin_ini_conv_mom(time, 1)*dt;
                end
            end
            for ir = 1:ND.regions
                tot_kin(:, ir) = offset(:,ir); 
                for ip = 1:ND.ass_paths
                    tot_kin(:, ir) = ND.frac(ip, ir)*kin_ini_conv_mom(:, ip, ir) + tot_kin(:, ir);
                end
            end  
        end
        
        function par_tot = get_parvec(ND, par_ini, par_kin,  frac, par_offset)
            %% Create a parameter vector from entries. Use default values if some entries are missing
            par_tot = [];
            if nargin < 2
                par_ini = ND.par_ini;
            end
            if nargin < 3
                par_kin = ND.par_kin;
            end
            if nargin < 4
                frac = ND.frac;
            end
            if nargin < 5
                par_offset = ND.par_offset;
            end
            
            % linearize the vector
            for i = 1:size(par_ini, 2)
                par_tot = [par_tot; par_ini(:,i)];
            end
            
            for i = 1:size(par_kin, 2)
                par_tot = [par_tot; par_kin(:,i)];
            end
            
            for i = 1:size(frac,2)
                par_tot = [par_tot; frac(:,i)];
            end
            
            for i = 1:size(par_offset,2)
                par_tot = [par_tot; par_offset(:,i)];
            end
        end
        
        function par_tot = get_parvec_hb(ND)
            par_tot = ND.get_parvec(ND.par_ini_hb, ND.par_kin_hb, ND.frac_hb, ND.par_offset_hb);
        end
        
        function par_tot = get_parvec_lb(ND)
            par_tot = ND.get_parvec(ND.par_ini_lb, ND.par_kin_lb, ND.frac_lb, ND.par_offset_lb);
        end    
        
        function par_tot = set_par_fromvec(ND, par_tot)
            %% assign value to parameters from vector
            a = size(ND.par_ini,1);
            b = size(ND.par_ini,2);
            for i = 1:b
               ND.par_ini(:, i) = par_tot(1 + (i-1)*a:a*i);
            end
            
            idx_offset = a*b;
            a = size(ND.par_kin,1);
            b = size(ND.par_kin,2);
            
            for i = 1:b
               ND.par_kin(:, i) = par_tot(idx_offset + 1 + (i-1)*a:idx_offset + a*i);
            end
            
            idx_offset = idx_offset + a*b;
            a = size(ND.frac,1);
            b = size(ND.frac,2);
            
            for i = 1:b
               ND.frac(:, i) = par_tot(idx_offset + 1 + (i-1)*a: idx_offset + a*i);
            end
            
            idx_offset = idx_offset + a*b;
            a = size(ND.par_offset,1);
            b = size(ND.par_offset,2);
            
            for i = 1:b
                ND.par_offset(:, i) = par_tot(idx_offset + 1 + (i-1)*a: idx_offset + a*i);
            end
        end
        
        
        function test_get_set_parvec(ND)
            %% test if linearization of parameter vectors is correct or not
            
            par_ini = [[2; 1], [4; 1], ... % Core interphase, core post-mitotic initiation
                       [30; 30], [6; 20]];  % non-core interphase,  non-core post-mitotic initiation
            par_kin =  [[1.5; 20], ... % interphase, postmitotic kinetics
                        [1; 15]]; % postmitotic kinetics
            frac = [[0.1; 0.9], ... % fraction interphase, positmitotic core region
                    [0.5; 0.5]];  % fraction interphase, positmitotic noncore region
            par_offset = [0, 0];
            par_vec = ND.get_parvec(par_ini, par_kin, frac, par_offset);
            ND.set_par_fromvec(par_vec);
            assert(all(reshape(par_ini == ND.par_ini,size(par_ini,2)*size(par_ini,1),1)));
            assert(all(reshape(par_kin == ND.par_kin,size(par_kin,2)*size(par_kin,1),1)));
            assert(all(reshape(frac == ND.frac, size(frac,2)*size(frac,1),1)));
            assert(all(reshape(par_offset == ND.par_offset, size(par_offset,2)*size(par_offset,1),1)));
        end
        
        function lsqnonlin_fit(ND, par_idx, time, time_data, data)
            
            %% Run a lsq_non linear fit
            par = ND.get_parvec();
            par_hb = ND.get_parvec_hb; 
            par_lb = ND.get_parvec_lb;
            
            parfit = par(par_idx);
            parfit_lb = par_lb(par_idx);
            parfit_hb = par_hb(par_idx);
            options = optimset('Display', 'iter', 'MaxIter', 100);
            parfit = lsqnonlin(@ND.dist_par, parfit, parfit_lb, parfit_hb, options, ...
                par_idx, time, time_data, data);
            
            par = ND.get_parvec();
            par(par_idx) = parfit;
            ND.set_par_fromvec(par);
        end
        
        function plot_combined_dist(ND, time)
            for ir = 1:ND.regions
                for ip = 1:ND.ass_paths
                    ND.MO.setpar_ini(ND.par_ini(:, (ir-1)*ND.ass_paths + ip));
                    ND.MO.setpar_kin(ND.par_kin(:,ip));
                    ND.MO.plot_combined_dist(time, ip + ND.ass_paths*(ir-1));
                    figure(ip + ND.ass_paths*(ir-1))
                    title(sprintf('region %d, path %d', ir, ip));
                end
            end
        end
    end
end
