classdef single_npc_logn_logistic < abs_single_npc
   % par_ini(1) : Mean of distribution
   % par_ini(2) : std of distribution
    methods
        function MO = single_npc_logn_logistic(par_ini, par_kin)
            display('single_npc_logn_logistic: Log normal initiation, logistic kinetics')
            MO = MO@abs_single_npc(par_ini, par_kin);
            MO.assert_par_kin(par_kin);
            MO.assert_par_ini(par_ini);
        end
        
        function bool = assert_par_ini(MO, par)
            
            bool = (length(par) == 2);
            if bool == 0 
                display('par_ini length == 2, mean and std of initiation lognormal distribution.');
            end
            
        end
        function bool = assert_par_kin(MO, par)
            bool = (length(par) == 2);
            if bool == 0 
                display( 'par_kin length == 2, slope and offset of lognormal distribution');
            end
        end
        
        function vec = ini_pdf(MO, time)
            %% g(t) probability density function for the initiation. Returns a vector
            % convert parmaters to entries for log-normal distribution
            mu = log((MO.par_ini(1)^2)/sqrt(MO.par_ini(2)+MO.par_ini(1)^2));
            sigma = sqrt(log(MO.par_ini(2)/(MO.par_ini(1)^2)+1));
            % [M, V]= lognstat(mu, sigma); M == par_ini(1), V == par_ini(2)
           
            pd = makedist('Lognormal',mu, sigma);  % Create object for distribution
            vec = pdf(pd, time);
        end
        
        function vec = ini_cdf(MO, time)
            %% int_0^t g(t) dt  cumulative distribution function for the initiation. Returns a vector 
            % convert parmaters to entries for log-normal distribution
            mu = log((MO.par_ini(1)^2)/sqrt(MO.par_ini(2)+MO.par_ini(1)^2));
            sigma = sqrt(log(MO.par_ini(2)/(MO.par_ini(1)^2)+1));
            
            % Create object for distribution
            pd = makedist('Lognormal',mu, sigma);
            
            vec = cdf(pd, time);
        end

        
        function mat = kin_pdf(MO, time)
            %% NOT IMPLEMENTED f(x,t) probability density function for the kinetics where x is occupancy. Should return a matrix
            display('Distribution of NUP kinetics has not been defined')
            mat = [];
        end
        
        function vec = kin_mom_pdf(MO, time, imom)
            %% Moments of the distribution int_0^\infty f(x,t) x^imom. F(t) = kin_mom_pdf(time, 1)
            if imom == 1
                % create offset so that vec(1) at time = 0 is equal par(2)
                tmin = log(MO.par_kin(2)/(1 - MO.par_kin(2)))*MO.par_kin(1); % tmnin = 0 is par(2) = 0.4
                time = time + tmin;  % this is badly defined
                vec = exp(time/MO.par_kin(1))./(1 + exp(time/MO.par_kin(1)));
            else
                display('Only first moment of kinetics has been defined');
            end
        end
    end
end