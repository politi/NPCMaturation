%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MCMC fitting/simulated annealing fitting     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function MCMC_fun_classes(MO, par_idx, time, time_data, data, filename, Tmax, maxstep)
MCMCSteps = 50000;
T_lambda = 0.995;
t = clock;
J = mod(round(cumprod([10000,60,60,24,31,12])*t(6:-1:1)'),2^32);
rng(J);
%maximal size of changes in one parameter
minstep = 1e-5;

DISPLAY = 1;




Npar = length(par_idx);
%initiate MCMC run


partofit_values = MO.get_parvec;
parbest = partofit_values(par_idx);
partofit_values = partofit_values(par_idx);
lbound = MO.get_parvec_lb;
hbound = MO.get_parvec_hb;

lbound = lbound(par_idx);
hbound = hbound(par_idx);

[res, mlogL] = MO.dist_par(partofit_values, par_idx, time, time_data, data);
%-log(Likelihood)
mlogL = 1/2*mlogL;
bestmL = mlogL;

poww = 1;
%compute width of steps
%width = width_comp(dist, partofit, partofit_names, par, data, bg, dataname,vara modelfun, mlogL, varargin{:});


widthp = parbest;%.*width;
widthl = 0.9^poww*widthp;

widthl(widthl>maxstep) = maxstep;

T = Tmax;

if T_lambda > 1
    error(' lamba, rate for deacreae in temperature < 1')
end

acceptance = 0;
acceptance50 = 0;
fittedpar = [];
fid = fopen(filename, 'w');
fprintf(fid, '%% MCMC run for model %s\n', MO.name);
fprintf(fid, '%% idx fitted %d', par_idx(1));
for i = 2:length(par_idx)
    fprintf(fid, ' %d', par_idx(i));
end
fprintf(fid, '\n');
fclose(fid);
for i = 1:MCMCSteps
    
    T = Tmax*T_lambda^i;
    
    if(floor(i/100)-i/100+1==1) %compute step size every 1000 points
        %width = width_comp(dist, partofit, partofit_names, par, data, bg, dataname, modelfun, mlogL, varargin);
    end
    %widthp = partofit_values;%.*width;
    %acceptance should remain between 10 and 30%
    if(floor(i/50)-i/50+1==1)
        acceptance50=acceptance/50;
        llim_accept=0.2;%+0.6/(par.fit.Tmax + 1)*par.fit.T;
        hlim_accept=0.3;%+0.5/(par.fit.Tmax + 1)*par.fit.T;
        if DISPLAY
            sprintf('step %i Chi2 %.2f acceptance %.2f width_m %.2f poww %i llim_accept %.2f hlim_accept %.2f Temperature %f\n',...
                i, 2*mlogL, acceptance/50, widthl(1),poww,...
                llim_accept,hlim_accept, T)
        end
        if(acceptance/50 < llim_accept) & T < 0.1
            poww = poww+1;
            if poww > 5
                poww = 5;
            end
        end
        if(acceptance/50 > hlim_accept) & T < 0.1
            poww = poww - 1;
            if poww < -5
                 poww = -5;  
            end
        end  
        acceptance=0;
    end
    widthl = widthp*0.9^poww;
    widthl(widthl>maxstep) = maxstep;
    widthl(widthl<minstep) = minstep;

    
    %check that boundaries are respected
    success = 0;
    while ( ~success )
        partofit_values_new = partofit_values + widthl.*randn(size(widthl,1),size(widthl,2));
        
        if ~isempty(lbound) && ~isempty(hbound)
            partofit_values_new(partofit_values_new < lbound) = 1*lbound(partofit_values_new < lbound) + 2*rand*lbound(partofit_values_new < lbound);
            partofit_values_new(partofit_values_new > hbound) = 1*hbound(partofit_values_new > hbound) - 0.2*rand*hbound(partofit_values_new > hbound);
        else
            error('MCMC_fun provide boundaries for parameters');
        end
        
        try
            [res, mlogL_new] = MO.dist_par(partofit_values_new, par_idx, time, time_data, data);
            success = 1;
        catch
            display(['MCMC_fun.m: integration of ' MO.type ' failed at MCMC_step ' num2str(i) ' with present set of parameters try a different one']);
            success = 0;
            partofit_values(partofit_values < 100*lbound) = 100*lbound(partofit_values < 100*lbound);
        end
    end
    
    
    mlogL_new = 1/2*mlogL_new;
    
    if DISPLAY
        %display([i; partofit_new; mlogL_new]);
    end
    %Update best set
    if bestmL > mlogL_new
        bestmL  = mlogL_new;
        parbest = partofit_values_new;
    end
    
    if(mlogL >= mlogL_new||(log(rand)<1/( 1 + T )*(mlogL - mlogL_new))) %accept if parameters are better or with certain probability rand
        partofit_values    = partofit_values_new;
        mlogL       = mlogL_new;
        acceptance  = acceptance+1;
    end
    
    
    if((floor(i/10)-i/10+1==1)&& T < 1e-3 )%save every 10 points
        %sprintf('step i %d mlogL %f',i,mlogL)
        fid = fopen(filename, 'A');
        par = MO.get_parvec();
        par(par_idx) = partofit_values;
        for ipar = 1:length(par)
            fprintf(fid, '%.2f\t', par(ipar));
        end
        fprintf(fid, '%.2f\t%.2f\t%.2f\t%.2f\n', 2*mlogL, acceptance50, T, poww);
        fclose(fid);
    end
   
end
% print best set at the end in case it has been lost between updates
fid = fopen(filename, 'A');
par = MO.get_parvec();
par(par_idx) = parbest;
for ipar = 1:length(par)
    fprintf(fid, '%.2f\t', par(ipar));
end
fprintf(fid, '%.2f\t%.2f\t%.2f\t%.2f\n',  2*bestmL, acceptance50, T, poww);
fclose(fid);


end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Compute relative width of steps in the parameterspace   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function width = width_comp(dist, partofit, partofit_names, par, data, bg, dataname, modelfun, mlogL, varargin)
Npar = length(partofit);
jac = zeros(1,Npar);
for j = 1:Npar
    partofit_width = partofit;
    partofit_width(j) = partofit_width(j) + 0.05*partofit_width(j);
    [res, mlogL_width] = dist(partofit_width, partofit_names, par, data, bg, dataname, modelfun, varargin{:});
    mlogL_width = 1/2*mlogL_width;
    jac(j) = abs((mlogL-mlogL_width)/0.05);
end
width=1-jac/(sum(jac));
%Normalize vector
width=width'/norm(width);
end
