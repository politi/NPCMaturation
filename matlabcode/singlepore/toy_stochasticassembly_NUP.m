function toy_stochasticassembly_NUP
N = 32; % number of units
npores = 100;
thist = [1, 7, 14];
time = [0:0.1:100];
ku = 0.5;
cm = colormap;
cmap_va = [1, 20, 40];
outdir = 'C:\Users\toni\Dropbox\NPCMaturation\matlabcode\singlepore\example_plots'
%%
k_nc = 0.2;

assembly_nocoop = getsinglepore(k_nc, ku, 1, N, npores,time);

%%
k_c = 0.002; % rate constant
alpha = 1.5; % cooperativity factor
assembly_coop = getsinglepore(k_c, ku, alpha, N, npores, time);


%%
setupFigure(1, [200 200 250 200])
clf
hold
plot(time, assembly_nocoop, '-', 'Color', [0.6 0.6 0.6]);
plot(time, mean(assembly_nocoop,2), '-k', 'LineWidth', 1.2);
for i = 1:length(thist)
    plot([thist(i) thist(i)], [0 N/2], '--', 'LineWidth', 2, 'Color', cm(cmap_va(i), :));
end
xlabel('Time [min]')
ylabel('# NUPs/NPC')
saveas(1, fullfile(outdir, 'single_pore_nocoop.png'))
%%
setupFigure(2, [200 200 250 200])
clf
hold
plot(time, assembly_coop, '-', 'Color', [0.6 0.6 0.6]);
plot(time, mean(assembly_coop,2), '-k', 'LineWidth', 1.2);
for i = 1:length(thist)
    plot([thist(i) thist(i)], [0 N/2], '--', 'LineWidth', 2, 'Color', cm(cmap_va(i), :));
end
xlabel('Time [min]')
ylabel('# NUPs/NPC')
%plot(cumsum(tau,2), [1:N], 'o');
saveas(2, fullfile(outdir, 'single_pore_coop.png'))
%% Align on half maxima
setupFigure(5, [200 200 400 300])
offset = zeros(1,npores);
assembly_coop_noise = assembly_coop + 0*rand(length(time), npores)
for i = 1:npores
    offset(i) =  mean(time(find((assembly_coop_noise(:,i) >= 15).*(assembly_coop_noise(:,i) <= 17))))
end

time_offset = repmat(time', 1, npores) - offset;
plot(time_offset, assembly_coop_noise)
%% Create histogram
setupFigure(3, [200 200 400 250]);
clf
hold
subplot(3,1,1)
N = 32; % number of units
npores = 200;
assembly_coop = getsinglepore(k_c, ku, alpha, N, npores, time);
h = histogram(assembly_coop(time == thist(1),:),  'BinEdges', [-0.5:1:32.5], ...
    'Normalization', 'Probability',  'FaceColor', cm(cmap_va(1), :))
xlim([-0.5 N+0.5])

subplot(3,1,2)
h = histogram(assembly_coop(time == thist(2),:),  'BinEdges', [-0.5:1:32.5], ...
    'Normalization', 'Probability',  'FaceColor', cm(cmap_va(2), :))
xlim([-0.5 N+0.5])

subplot(3,1,3)
h = histogram(assembly_coop(time == thist(3),:),  'BinEdges', [-0.5:1:32.5], ...
    'Normalization', 'Probability',  'FaceColor', cm(cmap_va(3), :))
xlim([-0.5 N+0.5]);

xlabel('#NUPs / NPC')

%%
setupFigure(4, [200 200 250 300])
clf
hold
subplot(3,1,1)
N = 32; % number of units
npores = 200;
assembly_coop = getsinglepore(k_c, ku, alpha, N, npores, time);
h = histogram(assembly_coop(time == thist(1),:),  'BinEdges', [-0.5:1:32.5], ...
    'Normalization', 'Probability', 'FaceColor', cm(cmap_va(1), :) )
xlim([-0.5 N+0.5])

subplot(3,1,2)
h = histogram(assembly_coop(time == thist(2),:),  'BinEdges', [-0.5:1:32.5], ...
    'Normalization', 'Probability', 'FaceColor', cm(cmap_va(2), :))
xlim([-0.5 N+0.5])

subplot(3,1,3)
h = histogram(assembly_coop(time == thist(3),:),  'BinEdges', [-0.5:1:32.5], ...
    'Normalization', 'Probability', 'FaceColor', cm(cmap_va(3), :))
xlim([-0.5 N+0.5])
xlabel('#NUPs / NPC')
saveas(4, fullfile(outdir, 'hist_coop.png'))
%%
setupFigure(6, [200 200 250 200])
u = 15;
s = 30;
time = [0:0.1:50]
%mu = log((MO.par_ini(1)^2)/sqrt(MO.par_ini(2)+MO.par_ini(1)^2));
%sigma = sqrt(log(MO.par_ini(2)/(MO.par_ini(1)^2)+1));
mu = log(u^2/sqrt(s+u^2));
sigma = sqrt(log(s/u^2+1));

% Create object for distribution
pd = makedist('Lognormal', mu, sigma);

vec = pdf(pd, time);
plot(time, vec, 'k-', 'LineWidth', 1.2)
xlabel('Time AO (min)')
ylabel('Probability')
saveas(6, fullfile(outdir, 'initiation_pron.png'))
end

function assembly = getsinglepore(k, ku, alpha, N, npores, sampling_time)
single_rates = k.*alpha.^[1:N];
%single_rates = k./[1:N];

%single_rates = single_rates.*(single_rates < ku) + ku.*(single_rates >= ku);
single_rates = [N:-1:1].*single_rates;

tau = zeros(npores, N);
tau(:, 1) =  exprnd(1/single_rates(1), npores,1);
% determine
for i = 2:32
    tau(:, i) =  exprnd(1/single_rates(i), npores,1);
end

event_times = cumsum(tau,2);
% resampling

assembly = zeros(length(sampling_time), npores);
for i = 1:npores
    for j = 1:size(event_times,2)
        if j > 1
            time_idx = find((sampling_time <= event_times(i,j)).*(sampling_time > event_times(i,j-1)));
        else
            
            time_idx = find(sampling_time <= event_times(i,j));
        end
        
        assembly(time_idx, i) = j-1;
        
    end
    time_idx = find(sampling_time > event_times(i, end));
    assembly(time_idx, i) = N;
    
end
end