classdef abs_single_npc < handle
    properties (Access = protected)
        par_ini; 
        par_kin;
    end
    
    methods     
        function MO = abs_single_npc(par_ini, par_kin)
            MO.par_ini = par_ini;
            MO.par_kin = par_kin;
        end
        
        function setpar_ini(MO, par)
            assert(MO.assert_par_ini(par), 'Number of parameters or range for par_ini is not correct');
            MO.par_ini = par; 
        end
        
        function setpar_kin(MO, par)
            assert(MO.assert_par_kin(par), 'Number of parameters or range for par_kin is not correct');
            MO.par_kin = par;
        end
        
        function X = kin_ini_conv_mom(MO, time, imom)
            % This is X(t) = int_0^\infty g(t') \int_0^\infty f(x, t - t') x^imom d x dt'
            X = conv(MO.ini_pdf(time), MO.kin_mom_pdf(time, imom));
            X = X(1:length(time));
        end
        
        function plot_ini_cdf(MO, time, ifig)
            figure(ifig);
            clf;
            plot(time, MO.ini_cdf(time));
            xlabel('Time (s)');
            ylabel('Initiation cdf');
        end
        
        function plot_ini_pdf(MO, time, ifig)
            figure(ifig)
            clf;
            plot(time, MO.ini_pdf(time));
            xlabel('Time (s)');
            ylabel('Initiation pdf');
        end
        
        function plot_kin_mom_pdf(MO, time, imom, ifig)
            figure(ifig);
            clf;
            plot(time, MO.kin_mom_pdf(time, imom));
            xlabel('Time (s)');
            ylabel(sprintf('NUP kinietics moment %d', imom));
        end
        
        function plot_combined_dist(MO, time, ifig)
            dt = time(2)-time(1);
            figure(ifig);
            clf;
            subplot(3,1,1)
            plot(time, MO.kin_mom_pdf(time, 1));
            xlabel('Time (min)');
            ylabel('NUP kinetics 1st moment');
            
            subplot(3,1,2)
            plot(time, MO.ini_pdf(time));
            xlabel('Time (min)');
            ylabel('Initiation pdf');
            
            subplot(3,1,3)
            kin_ini_conv = MO.kin_ini_conv_mom(time, 1);
            plot(time, kin_ini_conv(1:length(time))*dt);
            xlabel('Time (min)');
            ylabel('Average NPCs occupacy');
        end
        
        function par = get_par_ini(MO)
            par = MO.par_ini;
        end
        
        function par = get_par_kin(MO)
            par = MO.par_kin;
        end
    end
    
    methods(Abstract)
        bool = assert_par_kin(MO, par);
        bool = assert_par_ini(MO, par);
        vec = ini_cdf(MO, time) % int_0^t g(t) dt  cumulative distribution function for the initiation. Returns a vector 
        vec = ini_pdf(MO, time) % g(t) probability density function for the initiation. Returns a vector
        mat = kin_pdf(MO, time) % f(x,t) probability density function for the kinetics where x is occupancy. Should return a matrix
        vec = kin_mom_pdf(MO, time, imom) % Moments of the distribution int_0^\infty f(x,t) x^imom. F(t) = kin_mom_pdf(time, 1)
    end
        

end