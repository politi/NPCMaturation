%% A demo model to show the effect of single pore kinetics on overall assembly curve
function main()
addpath('C:\Users\toni\Documents\MATLAB\general')
addpath(genpath('C:\Users\toni\Code\MitoSys_POI_processing\IO\bioformats'))
addpath(genpath('C:\Users\toni\Code\ConfocalGN'))

%% lognormal function
% par_ini = [20,100];
% par_kin = [0.5];
% time = [0:0.1:60];
% N = 3;
% alpha = 1/100;
% pdf_fun = pdf_ini(time, par_ini , 'lognorm');
% time_app = time_appear(N, par_ini, 'lognorm');
% kin_fun = kin_pdf(time, par_kin, alpha);
% kin_conv = conv(kin_fun, pdf_fun);
% N = length(time_app);
% [totkin, singlekin] = addpores(time, time_app, kin_fun);
% makefigure(1, N, time, kin_fun, pdf_fun, totkin, singlekin, time_app);
% saveas(1, 'C:\Users\toni\Dropbox\NPCMaturation\results\singlepore\pores_N=3.png')
% ColOrd = get(gca,'ColorOrder');
% %%
% N = 100;
% pdf_fun = pdf_ini(time, par_ini , 'lognorm');
% time_app = time_appear(N, par_ini, 'lognorm');
% kin_fun = kin_pdf(time, par_kin, alpha);
% kin_conv = conv(kin_fun, pdf_fun);
% N = length(time_app);
% [totkin, singlekin] = addpores(time, time_app, kin_fun);
% makefigure(2, N, time, kin_fun, pdf_fun, totkin, singlekin, time_app);
% saveas(2, 'C:\Users\toni\Dropbox\NPCMaturation\results\singlepore\pores_N=100.png')
%
% %%
% makefigure(3, N, time, kin_fun, pdf_fun, totkin, singlekin, time_app);
% plot(time, kin_conv(1:length(time))/10, '-', 'Color', ColOrd(6,:), 'LineWidth', 2);
% saveas(3, 'C:\Users\toni\Dropbox\NPCMaturation\results\singlepore\pores_N=100_conv.png');

% Example extreme case of kinetics
N = 100;
alpha = 1/100;
par_ini = [32, 100];

par_kin = [0.5];
time = [0:0.1:90];
pdf_fun1 = pdf_ini(time, par_ini , 'lognorm');
kin_fun1 = kin_pdf(time, par_kin, alpha);
kin_conv1 = conv(kin_fun1, pdf_fun1);
time_app1 = time_appear(N, par_ini, 'lognorm');
[totkin1, singlekin1] = addpores(time, time_app1, kin_fun1);

par_ini = [15,50];
par_kin = [4];
pdf_fun2 = pdf_ini(time, par_ini , 'lognorm');
kin_fun2 = kin_pdf(time, par_kin, alpha);
kin_conv2 = conv(kin_fun2, pdf_fun2);
time_app2 = time_appear(N, par_ini, 'lognorm');
[totkin2, singlekin2] = addpores(time, time_app2, kin_fun2);

setupFigure(4, [100 0 400 500])
clf
subplot(4,3,1)
plot(time, [kin_fun1*32; kin_fun2*32],  'LineWidth', 1.5)

xlim([0 30]);
ylabel('# proteins ')
subplot(4,3,4:6)
plot(time, [pdf_fun1; pdf_fun2],  'LineWidth', 1.5)
ylabel('Probability')
subplot(4,1,3:4)
plot(time, [kin_conv1(1:length(time))/kin_conv1(length(time)); kin_conv2(1:length(time))/kin_conv2(length(time))], ...
    'LineWidth', 1.5);

xlabel('Time');
ylabel('Relative intensity');
saveas(4, 'C:\Users\toni\Dropbox\NPCMaturation\results\singlepore\pores_mix_sol.png');

dens = 2; %denisty #NPC/um2
pixelsize = 0.01;
area = N/dens; %width in um2
nrpixels = round(sqrt(area)/pixelsize);
vec_XY = round([1 + (nrpixels-1)*rand(N, 2)]);
createtifimage(time, singlekin1, nrpixels, vec_XY, pixelsize, 2, 32, 'C:\Users\toni\Dropbox\NPCMaturation\matlabcode\singlepore\example_plots\pore_appearance_kin1.tif')
createtifimage(time, singlekin2, nrpixels, vec_XY, pixelsize, 2, 32, 'C:\Users\toni\Dropbox\NPCMaturation\matlabcode\singlepore\example_plots\pore_appearance_kin2.tif')

display('end')

    function createtifimage(time, singlekin,  nrpixels, vec_XY, pixelsize, dt, bitdepth, file_out)
        % create a matrix of pixel intensities to then generate an image from it
 
        nrpores = size(singlekin,2);
        time_lowres = [0:dt:time(end)];
        idx_time = find(ismember(time, time_lowres));
        
        stack_in = zeros(nrpixels, nrpixels, length(time_lowres));
        % create pairs of positions
        
        
        for i = 1:length(idx_time)
            for ip = 1:nrpores
                stack_in(vec_XY(ip,1), vec_XY(ip,2), i) = singlekin(idx_time(i), ip);
            end
        end
        conf.pix = [pixelsize pixelsize  pixelsize];
        conf.psf = [0.1 0.1 0.1];
        for i = 1:length(idx_time)
            
            truth.img = stack_in(:,:,i);
            sample.noise = [0 0 0]';
            sample.sig = 1;
            truth.pix = conf.pix;
            [res, truth, sample] = confocal_generator(truth, conf ,sample);
            stack(:,:,i) = res.img;
        end
        switch bitdepth
            case 32
                stack_out = zeros(nrpixels, nrpixels, length(time_lowres));
                stack_out = stack;
            case 16
                
                stack_out = zeros(nrpixels, nrpixels, length(time_lowres), 'uint32');
                stack_out = int16(floor(stack/max(stack(:))*2^bitdepth));
            case 8
                
                stack_out = zeros(nrpixels, nrpixels, length(time_lowres), 'uint8');
                stack_out = int8(floor(stack/max(stack(:))*2^bitdepth));
            otherwise
                error('Bit depth must be 8 or 16');
        end
        %% create metadata
        metadata = createMinimalOMEXMLMetadata(stack_out);
        pixelSizeXY = ome.units.quantity.Length(java.lang.Double(pixelsize), ome.units.UNITS.MICROMETER);
        pixelSizeZ = ome.units.quantity.Length(java.lang.Double(pixelsize), ome.units.UNITS.MICROMETER);
        metadata.setPixelsPhysicalSizeX(pixelSizeXY, 0);
        metadata.setPixelsPhysicalSizeY(pixelSizeXY, 0);
        metadata.setPixelsPhysicalSizeZ(pixelSizeZ, 0);
        metadata.setChannelName('NUP', 0, 0);
        metadata.setPixelsTimeIncrement(ome.units.quantity.Time(java.lang.Double(dt*60), ome.units.UNITS.SECOND), 0);
        
        % this is very slow
        %bfsave(stack, files_out.img, 'metadata', metadata);
        %data = reshape(stack, width, height);
        options = struct('overwrite' , true);
        saveastiff(stack_out, file_out, options);
        %swapomexml(file_out, metadata)
    end
%%
% %%
% N = 1000;
% %par_ini = [32, 100];
% %par_kin = [0.5];
% par_ini = [32, 50];
% par_kin = [4];
% dt = 0.01;
% tmax = 20;
% time_sp = [0:dt:tmax];
% alpha = 1/100;
% pdf_fun1 = pdf_ini(time_sp, par_ini , 'lognorm');
% plot(time_sp, cdf_ini(time_sp, par_ini, 'lognorm'), 'g', time_sp, pdf_ini(time_sp, par_ini , 'lognorm'), 'r')
% %%
% kin_fun1 = kin_pdf(time_sp, par_kin, alpha); % this contains an offset
% kin_conv1 = conv(kin_fun1, pdf_fun1);
% time_app1 = time_appear(N, par_ini, 'lognorm');
% [totkin1, singlekin1] = addpores(time_sp, time_app1, kin_fun1);
%
%
% % dF = 1/100;
% % F = [0:dF:1-dF];
% %
% % clear('P')
% %
% % time = [0:dt:3*tmax];
% % pdf_fun1 = pdf_ini(time, par_ini , 'lognorm');
% % for i = 1:length(F)-1
% %     if i == 1
% %         t1 = tmax  + log(alpha/(1-alpha))*par_kin - log(F(i+1)/(1-F(i+1)))*par_kin;
% %         %P(1) =  sum(pdf_fun1(time > t1)*dt);
% %         P(1) = 1 - cdf_ini(t1, par_ini, 'lognorm');
% %     else
% %         t1 = tmax  + log(alpha/(1-alpha))*par_kin - log(F(i+1)/(1-F(i+1)))*par_kin;
% %         t2 = tmax  + log(alpha/(1-alpha))*par_kin - log(F(i)/(1-F(i)))*par_kin;
% %         idx = find((time > t1).*(time < t2));
% %         P(i) = sum(pdf_fun1(idx)*dt);
% %     end
% % end
% % % this are all the values between 1-dF and 1
% % t2 = tmax + log(alpha/(1-alpha))*par_kin - log(F(end)/(1-F(end)))*par_kin;
% % P(end+1) = cdf_ini(t2, par_ini, 'lognorm');
% %
% % figure(1)
% % clf
% % subplot(2,1,1)
% %
% % histogram(singlekin1(time_sp == tmax, :), [-0.1:0.01:1.1], 'Normalization', 'cdf')
% % hold
% % scatter(F, cumsum(P))
% % xlim([-0.1 1])
% % ylim([0 1])
% % ylabel('Probability');
%
%
% %%
% par_ini = [15,50];
% par_kin = [4];
% pdf_fun2 = pdf_ini(time, par_ini , 'lognorm');
%
% kin_fun2 = kin_pdf(time, par_kin, alpha);
% kin_conv2 = conv(kin_fun2, pdf_fun2);
% time_app2 = time_appear(N, par_ini, 'lognorm');
% [totkin2, singlekin2] = addpores(time, time_app2, kin_fun2);
%
% setupFigure(4, [100 0 400 500])
% clf
% subplot(4,3,1)
% plot(time, [kin_fun1*32; kin_fun2*32],  'LineWidth', 1.5)
%
% xlim([0 30]);
% ylabel('# proteins ')
% subplot(4,3,4:6)
% plot(time, [pdf_fun1; pdf_fun2],  'LineWidth', 1.5)
% ylabel('Probability')
% subplot(4,1,3:4)
% plot(time, [kin_conv1(1:length(time))/kin_conv1(length(time)); kin_conv2(1:length(time))/kin_conv2(length(time))], ...
%       'LineWidth', 1.5);
%
% xlabel('Time');
% ylabel('Relative intensity');
% %saveas(4, 'C:\Users\toni\Dropbox\NPCMaturation\results\singlepore\pores_mix_sol.png');

%%
% par_ini = [32, 100];
% par_kin = [0.5];
% dt = 0.1;
% par_ini = [15,50];
% par_kin = [4];
% tmax = 20;
% time = [0:dt:90];
%
% %% histogram of pores
% setupFigure(5, [100 0 400 500])
% clf
% subplot(3,2,1)
%
% histogram(singlekin1(time == 20, :)*32,  'Normalization', 'Probability');
% xlim([-1 33])
% ylim([0 1])
% ylabel('Probability');
% subplot(3,2,3)
% histogram(singlekin1(time == 30, :)*32, 'BinMethod','integers', 'Normalization', 'Probability');
% xlim([-1 33])
% ylim([0 1])
% ylabel('Probability');
% subplot(3,2,5)
% histogram(singlekin1(time == 50, :)*32,  'BinMethod','integers', 'Normalization', 'Probability');
% xlim([-1 33])
% ylim([0 1])
% ylabel('Probability');
% xlabel('# of NUPs');
%
% ColOrd = get(gca,'ColorOrder');
% subplot(3,2,2)
% histogram(singlekin2(time == 20, :)*32, 'BinMethod','integers', 'Normalization', 'Probability', 'FaceColor', ColOrd(2,:));
% xlim([-1 33])
% ylim([0 1])
% ylabel('Probability');
% subplot(3,2,4)
% histogram(singlekin2(time == 30, :)*32, 'BinMethod','integers', 'Normalization', 'Probability','FaceColor', ColOrd(2,:));
% xlim([-1 33])
% ylim([0 1])
% ylabel('Probability');
% subplot(3,2,6)
% histogram(singlekin2(time == 50, :)*32,  'BinMethod','integers', 'Normalization', 'Probability', 'FaceColor', ColOrd(2,:));
% xlim([-1 33])
% ylim([0 1])
% ylabel('Probability');
% xlabel('# of NPC');
% xlabel('# of NUPs')
% ylabel('Probability')
%saveas(5, 'C:\Users\toni\Dropbox\NPCMaturation\results\singlepore\hist_NUPsnumber.png');


%%
% figure(6)
% clf
% subplot(3,2,1)
% edges = [0:17]
% [h1, edge] = histcounts(singlekin1(time == 20, :)*16, 'BinEdges',edges, 'Normalization', 'Probability');
% hold
% [h2, edge] = histcounts(singlekin1(time == 30, :)*16, 'BinEdges',edges, 'Normalization', 'Probability');
% [h3, edge] = histcounts(singlekin1(time == 50, :)*16,  'BinEdges',edges, 'Normalization', 'Probability');
% bar(edges(1:end-1),[h1;h2;h3]')
% ylabel('Probability');
% subplot(2,1,2)
%
% edges = [0:17]
% [h1, edge] = histcounts(singlekin2(time == 20, :)*16, 'BinEdges',edges, 'Normalization', 'Probability');
% hold
% [h2, edge] = histcounts(singlekin2(time == 30, :)*16, 'BinEdges',edges, 'Normalization', 'Probability');
% [h3, edge] = histcounts(singlekin2(time == 50, :)*16,  'BinEdges',edges, 'Normalization', 'Probability');
% bar(edges(1:end-1),[h1;h2;h3]')
% ylabel('Probability');
% xlabel('Number of NUPs')
% ylabel('Probability')
end


function swapomexml(filename, metadata)
% swap metadata of file tiff file. Write out to tagstruct.ImageDescription
filein = loci.common.RandomAccessInputStream(filename);
fileout = loci.common.RandomAccessOutputStream(filename);
saver = loci.formats.tiff.TiffSaver(fileout, filename);
saver.overwriteComment(filein, metadata.dumpXML);
saver.close();
filein.close();
fileout.close();
end

function makefigure(hfig, N, time, kin_fun, pdf_fun, totkin, singlekin, time_app)
setupFigure(hfig, [100 0 400 500])
clf
subplot(4,3,1)
plot(time, kin_fun*32, '-k', 'LineWidth', 1.5)
ColOrd = get(gca,'ColorOrder');
xlim([0 10]);
ylabel('# proteins ')
subplot(4,3,4:6)
plot(time, pdf_fun, '-', 'LineWidth', 1.5, 'Color', ColOrd(2,:))
ylabel('Probability')
subplot(4,1,3:4)

plot(time, totkin/N, 'LineWidth', 1.5,  'Color', ColOrd(1,:));
hold
plot(time, singlekin/N, '--k');

plot([time_app; time_app], repmat([0;0.1],1,size(time_app,2)), '-', 'Color', ColOrd(2,:));
xlabel('Time');
ylabel('Relative intensity');
end

function [totkin, singlekin] = addpores(time, time_app, kin)
%% Add intensities of pores. This is very cumbersome
time_app = round(time_app, 1);
totkin = zeros(length(time),1);
singlekin = zeros(length(time),length(time_app));
for i = 1:length(time_app)
    pos = find(round(time,1) == time_app(i));
    totkin(pos:end) = totkin(pos:end) + kin(1:end - (pos-1))';
    singlekin(pos:end, i) = kin(1:end -(pos-1))';
end
end

function [t,y] = kin(dt, par, alpha)
%% function generating a kinetic trace of a pore starts with alpha of signal
tmin = log(alpha/(1-alpha))*par(1);
tmax = log((1-alpha)/alpha)*par(1);
t = [tmin:dt:tmax];
y = 1  - 1./(1 + exp(t/par(1)));
end

function [y] = kin_pdf(time, par, alpha)
%% function generating a kinetic trace of a pore in a time interval to be used for convolution
% trace is offsetted by tmin. So that y(time = 0) = alpha.
tmin = log(alpha/(1-alpha))*par(1);
time = time + tmin;
y = 1  - 1./(1 + exp(time/par(1)));
end

function inipdf = pdf_ini(time, par, dist)
%% function generating the probability fo initiaion
switch dist
    case 'lognorm'
        mu = log((par(1)^2)/sqrt(par(2)+par(1)^2));
        sigma = sqrt(log(par(2)/(par(1)^2)+1));
        [M, V]= lognstat(mu,sigma);
        pd = makedist('Lognormal',mu, sigma);
        inipdf = pdf(pd, time);
    case 'genlog'
        inipdf = par(3)/par(2)*exp(-(time-par(1))/par(2)).*(1+exp(-(time-par(1))/par(2))).^(-par(3)-1);
end
end

function inicdf = cdf_ini(time, par, dist)
%% function generating the probability
switch dist
    case 'lognorm'
        mu = log((par(1)^2)/sqrt(par(2)+par(1)^2));
        sigma = sqrt(log(par(2)/(par(1)^2)+1));
        [M, V]= lognstat(mu,sigma)
        pd = makedist('Lognormal',mu, sigma);
        inicdf = cdf(pd, time)
    case 'genlog'
        inicdf = (1+exp(-(time-par(1))/par(2))).^(-par(3));
end
end

function time = invcdfgenlog(prob, par)
%% inverse gen log cdf
time = par(1) - log((1./prob).^(1/par(3))-1)*par(2);
end

function time = time_appear(n, par, dist)
%% function generating a list of pore events
switch dist
    case 'lognorm'
        mu = log((par(1)^2)/sqrt(par(2)+par(1)^2));
        sigma = sqrt(log(par(2)/(par(1)^2)+1));
        [M, V]= lognstat(mu,sigma)
        time = lognrnd(mu, sigma, 1,n);
    case 'genlog'
        prob = rand(1,n);
        time = invcdfgenlog(prob, par);
        % remove negative values
        time(time<0) = [];
end
end