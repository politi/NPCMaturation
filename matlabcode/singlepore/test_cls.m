% nup = single_npc_logn_logistic([32, 50], [1, 1e-3]);
% % nup.plot_ini_cdf([0:0.1:100], 1)
% % nup.plot_ini_pdf([0:0.1:100], 2)
% % nup.plot_kin_mom_pdf([0:0.1:100], 1, 3)
% % nup.plot_combined_dist([0:0.1:100], 4)
% 
% %%
% % order is always core, non-core, interphase, post-mitotic
% par_ini = [[5; 1], [5; 1], ... % Core interphase, core post-mitotic
%             [10; 1], [1; 5]]; % non-core interphase non-core post-mitotic
% par_kin =  [[15; 1e-2], ...
%     [15; 1e-2]]; % interphase, postmitotic kinetics
% frac = [[0.1; 0.9], [0.5; 0.5]];  
% par_offset = [0, 0];
% single_npc_model = 'single_npc_logn_logistic';
% clear('ND')
% ND = nuclei_dynamic(single_npc_model, par_ini,  par_kin, frac, par_offset);
% time = [0:0.01:140];
% ND.nuclei_dist(time, protA(1).time, [protA(1).tot_core, protA(1).tot_noncore])
% 
% load('C:\Users\toni\Dropbox\NPCMaturation\expdata\livecell\livecelldata.mat')
% [norm, distvec, tot_kin] = ND.nuclei_dist(time, protA(1).time, [protA(1).tot_core, protA(1).tot_noncore])
% figure(1)
% clf
% hold
% plot(protA(1).time, [protA(2).tot_core(:,1), protA(2).tot_noncore(:,1)], 'o')
% plot(protA(1).time, tot_kin)
% 

%%
par_ini = [[30; 30], [6; 20],  ... % Core interphase, core post-mitotic initiation; region1
            [10; 1], [10; 1]];  % non-core interphase,  non-core post-mitotic initiation; region2
par_kin =  [[1.5; 20], ... % interphase kinetics, path1
            [1; 15]]; % postmitotic kinetics, path2
frac = [[0.5; 0.5], ... % fraction interphase, positmitotic core region
        [0.1; 0.9]];  % fraction interphase, positmitotic noncore region
par_offset = [0, 0];
single_npc_model = 'single_npc_logn_hill';
clear('ND')

ND = nuclei_dynamic(single_npc_model, par_ini,  par_kin, frac, par_offset);
time = [0:0.01:140];
load('C:\Users\toni\Dropbox\NPCMaturation\expdata\livecell\livecelldata.mat')
[norm, distvec, tot_kin] = ND.dist(time, protA(2).time, [protA(2).tot_core, protA(2).tot_noncore]);


norm_man = [(protA(2).tot_core(:,1) - tot_kin(:,1))./protA(2).tot_core(:,2); (protA(2).tot_noncore(:,1) - tot_kin(:,2))./protA(2).tot_noncore(:,2)]
norm_man  = sum(norm_man.^2)
figure(2)
clf
hold
plot(protA(2).time, [protA(2).tot_core(:,1), protA(2).tot_noncore(:,1)], 'o')
plot(protA(2).time, tot_kin)
ND.set_par_fromvec(parprot2)
[norm, distvec, tot_kin2] = ND.dist(time, protA(2).time, [protA(2).tot_core, protA(2).tot_noncore]);
plot(protA(2).time, tot_kin2, '--')


%%
par_ini = [[3; 5], [10; 10],  ...   % non-core interphase,  non-core post-mitotic initiation
        [30; 30], [6; 20]].*rand(2,4);         % Core interphase, core post-mitotic initiation
par_kin =  [[1; 15],  ...           % interphase, postmitotic kinetics
            [1.5; 20]].*rand(2,2);             % postmitotic kinetics
frac = [[0.5; 0.5], ...             % fraction interphase, positmitotic core region
        [0.1; 0.9]];                % fraction interphase, positmitotic noncore region
ND = nuclei_dynamic(single_npc_model, par_ini,  par_kin, frac, par_offset);
ND.usestd = 1;

ND.lsqnonlin_fit([1:12], time, protA(1).time, [protA(1).tot_core, protA(1).tot_noncore]);
parprot1 = ND.get_parvec;

%%
par_ini = [[3; 5], [10; 10],  ...   % non-core interphase,  non-core post-mitotic initiation
        [30; 30], [6; 20]].*rand(2,4);         % Core interphase, core post-mitotic initiation
par_kin =  [[1; 15],  ...           % interphase, postmitotic kinetics
            [1.5; 20]].*rand(2,2);             % postmitotic kinetics
frac = [[0.5; 0.5], ...             % fraction interphase, positmitotic core region
        [0.1; 0.9]];                % fraction interphase, positmitotic noncore region

par_offset = [0, 0];
single_npc_model = 'single_npc_logn_hill';
clear('ND')

ND = nuclei_dynamic(single_npc_model, par_ini,  par_kin, frac, par_offset);
ND.usestd = 1;

ND.lsqnonlin_fit([1:12], time, protA(2).time, [protA(2).tot_core, protA(2).tot_noncore]);
parprot2 = ND.get_parvec;

%%
ND.set_par_fromvec(parprot1)
[norm, distvec, tot_kin] = ND.dist(time, protA(1).time, [protA(1).tot_core, protA(1).tot_noncore]);
figure(1)
clf
hold
plot(protA(1).time, [protA(1).tot_core(:,1), protA(1).tot_noncore(:,1)], 'o')
plot(protA(1).time, tot_kin)

ND.set_par_fromvec(parprot2)
[norm, distvec, tot_kin] = ND.dist(time, protA(2).time, [protA(2).tot_core, protA(2).tot_noncore]);
figure(2)
clf
hold
plot(protA(2).time, [protA(2).tot_core(:,1), protA(2).tot_noncore(:,1)], 'o')
plot(protA(2).time, tot_kin)

%%
ND.set_par_fromvec(parprot2)
ND.plot_combined_dist(time)

%%
ND.set_par_fromvec(parprot1)
ND.plot_combined_dist(time)

%%
ND.usestd = 1;
parprot1(end-1)  = 0.1;
parprot1(end) = 0.05;
parprot1(end-2) = 0.9;
parprot1(end-3) = 0.1;
parprot1(end-4) = 0.5;
parprot1(end-5) = 0.5;


ND.set_par_fromvec(parprot1)
time = [0:0.05:140];
MCMC_fun_classes(ND, [1:12],  time,  protA(1).time, [protA(1).tot_core, protA(1).tot_noncore], 'NUP107_MCMC_usestd1_2.dat', 0, 0.5)


%%
ND.usestd = 1;
ND.set_par_fromvec(parprot2)
time = [0:0.05:140];
MCMC_fun_classes(ND, [1:12],  time,  protA(2).time, [protA(2).tot_core, protA(2).tot_noncore], 'NUP358_MCMC_usestd1.dat', 0, 0.2)

%%
data_NUP107 = load('NUP107_MCMC_usestd1_2.dat');
%[val, pos] = min(data_NUP107(:,end-3))
ND.set_par_fromvec(mean(data_NUP107(:, 1:18),1))
%ND.plot_combined_dist(time)
%ND.set_par_fromvec(parprot1)
[norm, distvec, tot_kin] = ND.dist(time, protA(1).time, [protA(1).tot_core, protA(1).tot_noncore]);
colors = colormap();
setupFigure(1, [0 0 400 300])
clf
hold
errorbar(protA(1).time, protA(1).tot_core(:,1), protA(1).tot_core(:,2), '.', 'Color', colors(40,:))
errorbar(protA(1).time, protA(1).tot_noncore(:,1), protA(1).tot_noncore(:,2), '.',  'Color',colors(20,:))
plot(protA(1).time, tot_kin(:,1), '-', 'LineWidth', 2, 'Color', colors(40,:))
plot(protA(1).time, tot_kin(:,2), '-', 'LineWidth', 2, 'Color', colors(20,:))
title('NUP3107')
xlabel('Time [min]')
ylabel('Fraction total')
legend('Core data', 'Non-core data', 'Core model', 'Non-core model', 'Location', 'SouthEast')
xlim([0 120])
ylim([0 1.2])
saveas(1, 'NUP107_MCMC_usestd1_2.png')

%%

data_NUP358 = load('NUP358_MCMC_usestd1.dat');

ND.set_par_fromvec(mean(data_NUP358(:, 1:18),1))
%ND.plot_combined_dist(time)
[norm, distvec, tot_kin] = ND.dist(time, protA(2).time, [protA(2).tot_core, protA(2).tot_noncore]);

colors = colormap();
setupFigure(1, [0 0 400 300])
clf
hold
errorbar(protA(1).time, protA(2).tot_core(:,1), protA(2).tot_core(:,2), '.', 'Color', colors(40,:))
errorbar(protA(1).time, protA(2).tot_noncore(:,1), protA(2).tot_noncore(:,2), '.',  'Color',colors(20,:))
plot(protA(1).time, tot_kin(:,1), '-', 'LineWidth', 2, 'Color', colors(40,:))
plot(protA(1).time, tot_kin(:,2), '-', 'LineWidth', 2, 'Color', colors(20,:))
title('NUP358')
xlabel('Time [min]')
ylabel('Fraction total')
legend('Core data', 'Non-core data', 'Core model', 'Non-core model', 'Location', 'SouthEast')
xlim([0 120])
ylim([0 1.2])
saveas(1, 'NUP358_MCMC_usestd1.png')


%%
 np = single_npc_logn_hill( ND.par_ini(:,1), ND.par_kin(:,1));
setupFigure(1, [0 0  400 500])
 clf
 subplot(2,1,1)
 hold
 subplot(2,1,2)
 hold
for i = 1000:50:5000
   ND.set_par_fromvec(data_NUP358(i, 1:18))
   ip = 1;
   ir = 1;

   col =  colors(40/ir,:);

   np.setpar_ini(ND.par_ini(:, (ir-1)*ND.ass_paths + ip));
   np.setpar_kin(ND.par_kin(:,ip));
   subplot(2,1,1)
   %plot(time, np.kin_mom_pdf(time, 1));
   plot(time, np.kin_ini_conv_mom(time, 1)*(time(2)-time(1)), 'Color', col);
   
   
   subplot(2,1,2)
   
   plot(time,  np.ini_pdf(time), 'Color', col);
   xlim([0 120])
end
subplot(2,1,1)
ylabel('Fraction bound single pore')
subplot(2,1,2)
ylabel('Initiation probability')
xlabel('Time [min]')
saveas(1, sprintf('NUP358_MCMC_usestd1_reg%d_path%d.png', ir, ip))
%histogram(data_NUP107(:,5))


%%
np = single_npc_logn_hill( ND.par_ini(:,1), ND.par_kin(:,1));
setupFigure(1, [0 0  400 500])
 clf
 subplot(2,1,1)
 hold
 subplot(2,1,2)
 hold
for i = 1000:50:5000
   ND.set_par_fromvec(data_NUP107(i, 1:18))
   ip = 1;
   ir = 1;

   col =  colors(40/ir,:);

   np.setpar_ini(ND.par_ini(:, (ir-1)*ND.ass_paths + ip));
   np.setpar_kin(ND.par_kin(:,ip));
   subplot(2,1,1)
   %plot(time, np.kin_mom_pdf(time, 1));
   plot(time, np.kin_ini_conv_mom(time, 1)*(time(2)-time(1)), 'Color', col);
   subplot(2,1,2)
   plot(time,  np.ini_pdf(time), 'Color', col);
   xlim([0 120])
end
subplot(2,1,1)
ylabel('Fraction bound single pore')
subplot(2,1,2)
ylabel('Initiation probability')
xlabel('Time [min]')
saveas(1, sprintf('NUP107_MCMC_usestd1_reg%d_path%d.png', ir, ip))