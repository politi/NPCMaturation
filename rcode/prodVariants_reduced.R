# ----


library("ggplot2")

if (Sys.info()['sysname'] == 'Darwin'){
  indatadir <-'/Users/toni/Dropbox/NPCMaturation/expdata/emdata'
  indir <- '/Users/toni/Dropbox/NPCMaturation/results/emdata/delay'
  outdir <- '/Users/toni/Dropbox/NPCMaturation/results/emdata/figures'
} else {
  indatadir <- 'C:\\Users\\toni\\Dropbox\\NPCMaturation\\expdata\\emdata'
  indir <- 'C:\\Users\\toni\\Dropbox\\NPCMaturation\\results\\emdata\\delay'
  outdir <- 'C:\\Users\\toni\\Dropbox\\NPCMaturation\\results\\emdata\\figures'
}


setwd(indir)

fnames = c('delayedMaturation_noPrecursor_ts10.txt', 
           'delayedMaturationExponentialV_noPrecursor_ts10_v0.015.txt',            
           'delayedMaturation_v0.015_ts10.txt')

varNames = c('Var 1',  'Var 2', 'Var 3')
inData = list()
seq_plot = c(1, 2, 3)
for (i in seq_along(fnames) ) {
  
  tmpCsv = read.csv(fnames[i], skip=6, sep='\t')
  tmpCsv['model'] = varNames[i]
  idx = which.min(tmpCsv$norm)
  inData = rbind(inData, tmpCsv[idx, c('tauM', 'kM', 'norm', 'model', 'nrpara')])
}

inData$model = factor(inData$model, levels = varNames[seq_plot])

#----- 
setwd(outdir)
pChi2<- ggplot(inData, aes(x = model, y = norm, fill = model)) + geom_bar(stat = "identity") + coord_cartesian(ylim= c(20,55)) +
  theme_classic(base_size = 8)  + xlab('') + ylab('Sum of squared \n residuals') + scale_fill_brewer(palette="Set2")+ 
  theme(legend.position = 'none', axis.line.x = element_line(color="black", size = 0.5),
        axis.line.y = element_line(color="black", size = 0.5)) + 
  theme(axis.text.x = element_blank(), 
        axis.text.y = element_text(size=8), 
        plot.margin = unit(c(5,5,0,0), "mm")) 
pChi2
gp <- ggplot_build(pChi2)
cp <- unique(gp$data[[1]]["fill"])

ggsave(filename="production_rates_Chi2.pdf", plot=pChi2,  width = 71, height = 30, units = 'mm')


ptaum<- ggplot(inData, aes(x = model, y = 1/kM + tauM, fill = model)) + geom_bar(stat = "identity") + 
  coord_cartesian(ylim= c(0,50), expand = c(0),  xlim = c(0.4,3.6)) + scale_fill_brewer(palette="Set2")+ 
  theme_classic(base_size = 8)  + xlab('') + ylab('Maturation time (min)\n') + 
  theme(legend.position = 'none', axis.line.x = element_line(color="black", size = 0.5),
        axis.line.y = element_line(color="black", size = 0.5)) + 
  theme(axis.text.x = element_text(size=8, angle = 45, vjust = 0.5), 
        axis.text.y = element_text(size=8), 
        plot.margin = unit(c(5,5,0,0), "mm")) 
ptaum

ggsave(filename="production_rates_tauM.pdf", plot=ptaum, width = 71, height = 43, units = 'mm')



# ----
testPairs = rbind(c(1,2), c(1,3))
for(i in seq(1,2,1)) {
  print(1 - pchisq(abs(inData$norm[testPairs[i,1]]-inData$norm[testPairs[i,2]]),abs(inData$nrpar[testPairs[i,2]]-inData$nrpar[testPairs[i,1]])))
}
#----
setwd(indir)
fnames = fnames = c('delayedMaturation_noPrecursor_ts10_prodrate.txt', 
                      'delayedMaturationExponentialV_noPrecursor_ts10_v0.015_prodrate.txt',            
                      'delayedMaturation_v0.015_ts10_prodrate.txt')

inData = list()
seq_plot = c(1, 2, 3)
for (i in seq_along(fnames) ) {

  tmpCsv = read.csv(fnames[i], skip=0, sep='\t')
if (i== 2) {
  tmpCsv['ProdDensity_inner'] = tmpCsv['ProdDensity_inner']*0.5
  }
  inData = rbind(inData, tmpCsv)
}

inData$model = factor(inData$model, levels = varNames[seq_plot])

p <- ggplot(inData, aes(time, ProdDensity_inner, colour = model)) + geom_line(size = 1)  + theme_classic(base_size = 8) + 
  coord_cartesian(ylim = c(0, 0.2), xlim = c(0, 50)) + facet_wrap(~ model, ncol = 1) + xlab('Time after AO (min)') +
  ylab(expression(paste("Production density (pores/", mu, m^2, "/min)"))) + 
  theme(legend.position = 'none', 
        strip.background = element_blank(), axis.line.y = element_blank(), #element_line(color="black", size = 0.5),
        axis.line.x = element_blank(),
        strip.text.x = element_text(size = 8, angle = 0),
        legend.key = element_blank(),
        #axis.text.x = element_blank(), 
        #axis.text.y = element_blank(),
        #axis.ticks.x=element_blank(),
        #axis.ticks.y=element_blank(),
        plot.margin = unit(c(5,5,0,0), "mm")) + scale_colour_manual(values=cp$fill[seq_plot])
p

setwd(outdir)

ggsave(filename="production_rates.pdf", plot=p, width = 50, height = 60, units = 'mm') 





# ----



addOpt <- function(p) {
  p <- p + xlab(' Time after AO (min)')  +
    scale_x_continuous(expand = c(0, 0), limits = c(0,120), breaks=seq(0,120,20)) + 
    scale_y_continuous(expand = c(0, 0), limits = c(0, 13), breaks=seq(0,13,4)) + 
    theme_classic(base_size = 8) + scale_colour_brewer(palette="Set2") + 
    theme(legend.position = 'none', axis.line.x = element_line(color="black", size = 0.5),
          axis.line.y = element_line(color="black", size = 0.5)) + 
    theme(axis.text.x = element_text(size=8), 
          axis.text.y = element_text(size=8), 
          plot.margin = unit(c(5,5,0,0), "mm"))
  return(p)
}

setwd(indatadir)

denData = read.csv('density_innercore.txt', skip=0, sep='\t')
denDataouter = read.csv('density_outercore.txt', skip=0, sep='\t')

setwd(indir)
if (Sys.info()['sysname'] == 'Darwin'){
  setwd('/Users/toni/Dropbox/NPCMaturation/results/emdata/delay')
} else {
  setwd('C:\\Users\\toni\\Dropbox\\NPCMaturation\\results\\emdata\\delay')
}
fnames = c('delayedMaturation_noPrecursor_ts10_den.txt', 
                    'delayedMaturationExponentialV_noPrecursor_ts10_v0.015_den.txt',            
                    'delayedMaturation_v0.015_ts10_den.txt')

inData = list()
seq_plot = c(1, 2, 3, 4, 5)
for (i in seq_along(fnames) ) {
  
  tmpCsv = read.csv(fnames[i], skip=0, sep='\t')
  tmpCsv['model'] = varNames[i]
  inData = rbind(inData, tmpCsv)
}

inData$model = factor(inData$model, levels = varNames[seq_plot])


# ----
setwd(outdir)
pint_inter <- ggplot() + geom_bar(data=denData, stat = 'identity', width = 1.5, aes(time, int_avg, fill = 'red')) + 
  geom_line(data = inData, size = 0.8, lty = 2,  aes(time, inter_i, group = model,  colour = model)) + ylab(expression(paste("Intermediate density (pores/", mu, m^2, ")")))

pint_mature <- ggplot() + geom_bar(data=denData, stat = 'identity', width = 2, aes(time, mat_avg, fill = '#1E477E')) + 
  geom_line(data = inData, size = 0.8, lty = 2, aes(time, mature_i, group = model, colour = model))+ ylab(expression(paste("Mature density (pores/", mu, m^2, ")")))

pint_inter <- addOpt(pint_inter) + scale_fill_manual(values = c('red'))
pint_mature <- addOpt(pint_mature) + scale_fill_manual(values = c('#1E477E'))



pint_inter
pint_mature


ggsave(filename="prodVariants_inter_inner_den.pdf", plot=pint_inter, width = 71, height = 44, units = 'mm')
ggsave(filename="prodVariants_mature_inner_den.pdf", plot=pint_mature, width = 71, height = 44, units = 'mm')

