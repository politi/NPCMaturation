require('ggplot2')
require('fcsresfun')
require('tools')

# load the data ----
afile <- 'W:\\Shotaro\\Live-cell-imaging\\HeLa-FCS\\180112-HeLa-gfpSEH1z76z2_gfpNUP107z26z31\\Calibration\\2c_opt_CF1.txt'
ACFs <- read.csv(file = afile, sep = '\t')
ACFs$POI <- ''

ACFs$POI[grepl(patter = 'mEGFPN1', x = ACFs$path)] <- 'mEGFPN1'
ACFs$stage[grepl(patter = 'mEGFPN1', x = ACFs$path)] <- 'interphase'

