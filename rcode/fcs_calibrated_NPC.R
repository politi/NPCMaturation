# Description
# ----
# Antonio Politi (EMBL)
# fcs_calibrated_NPC.R
# plot results of data from FCS calibrated live cell imaging of nuclear pore rim
# The script expects a defined directory structure
# Result/Segmentation -- Contains the segmentation results from JHossain matlab segmentation code.
#   cellname_XYZ.... The units of volume um3 of area um2
# Result/Calibration  -- Contains the calibration factors as computed from the FCS data
#   calibration.mat: cal$calibration.factor.C.nM = c(a,b) can be used to compute C(nM) = a +b*FI
# Result/BGCom        -- Segmentation of BG for cells that do not have an external marker
#   bg: directory that contains tiffs and txt files with bg values for all cells
#   anaphase: a text file that contains information for each cell
#         cell=nameofcell.tif	anaphase=frame of anaphase	nucleus1=1,0 to use nucleus1 or not
#         nucleus=0,1 to use nucleus2 or not, dmin=time difference in min, omitframe=nr of frame to omit
#         0 is none,	maxf= maximal numeber of frames to use,	siRNA=an additional ID for siRNA
# bg_eroded: 5 time erosion with circular element of 3x3x3 of 3D nucleus
# bg_skel:   is skeletonization of 3D nucleus
# -----
# Imports
# ----
require('ggplot2')
require('readxl')
require('reshape')
require('plyr')
require('R.matlab')
require('tools')
# -----
# Parameters
# ----
# Shotaro's settings
# voxel_XY_size_um <- 0.2471488 # this depends on the imaging
# voxel_Z_size_um <- 1
# MitoSys settings
voxel_XY_size_um <- 0.2516
voxel_Z_size_um <- 0.75
voxel_V_um3 <- voxel_XY_size_um^2*voxel_Z_size_um #this is in um3 *10^-15 to get liter
Na <- 0.6022140 #this *10^24 (*10^-15*10^-9 then cancel out)
rimWidth <- 3   #this is the rim width in px used for estimating the nucleus boundary

#fraction of membrane of different regions as computed by Shotaro Otsuka from EM images
fractionNonCore <- 0.635
fractionInnerCore <- 0.247
fractionOuterCore <- 0.118
fractionWhole<- 1
fractionArea<-c(fractionOuterCore, fractionInnerCore, fractionNonCore, fractionWhole)

# interphase density HeLa Cell 10.66 +/- 2.69
surfDenInt <- 10.66
#computation mode is ss for single slice or ms for mean slice of 3 slices
useSlice <- 'ms'

#this is the numeracy/stochiometry of NPC protein in pore. This is different depending on type of Nup

stochiometry <- data.frame(NUP107 = 32, TPR = 32, NUP214 = 16, NUP358 = 32)
# concatResults function -----

concatResults <- function (files, bgSub = 1, coeff = 0.6, useSlice = 'ss') {
  # concatenate different results files generated by Julius Hossain segmentation code and compute calibrated
  # traces.
  # INPUT
  #   files:  is a list of filenames one for each cell
  #   bgSub:  Subtracts background using segmentation results (1) or value at anaphase (0)
  #   coeff:  Bg is computed from average of cytoplasmic and nucleoplasmic values
  #   useSlice: 'ss' - single slice or 'ms' - average of 3 slices
  # OUTPUT
  #   totalNuc: concatenated data

  totalNuc <- data.frame()

  # continuous index of nuclei

  # get anaphase time for each file
  anaphase <- read.csv('./BGcomp/bg/anaphase.txt', skip=0, sep='\t', skipNul = TRUE) #contains the frame of anaphase

  # get calibration factors
  calV <- tryCatch ({
    #cal <- readMat('./Calibration2017-02-23_kappa_5.8/calibration.mat')
    cal <- readMat('./Calibration/calibration.mat')
    cal$calibration.factor.C.nM
  }, error = function(e) {
    c(37.93, 0.62)
  })

  # read files
  for (file in files) {
    locF <- anaphase[anaphase['cell'] == file, ]

    #foreground data
    fg <- read_excel(paste('./Segmentation/', file, '/', file, '.xls', sep = ''))
    #bgdata
    bg <- read.csv(paste('./BGcomp/bg/', file, '_bg.txt', sep=''), skip=0, sep='\t', skipNul = TRUE)
    bg$frame = fg$Timepoint

    # merge cell 1 and 2 for background. This reduces the intrinsic noise in the data----
    mnuc <-  melt(bg[, c('frame', 'Nuc1_nuc_erode',   'Nuc2_nuc_erode', 'Nuc1_nuc_skel', 'Nuc2_nuc_skel')], id = 'frame')
    mcyt <-  melt(bg[, c('frame', 'Nuc1_cyt', 'Nuc2_cyt')], id = 'frame')

    if (dim(bg)[1] > 1) { # several time points
      # merge skeletonize and eroded data and avarage for both nuclei
      bgnuc_pred <- predict(loess(value ~ frame, mnuc, span = 0.5), bg$frame)
      bgcyt_pred <- predict(loess(value ~ frame, mcyt, span = 0.5), bg$frame)


      #bg Whole cell WC is mean of nucleus and cytoplasm according to coeff
      bgS <- data.frame(WC = bgnuc_pred*coeff + bgcyt_pred*(1-coeff),
                        Cyt = bgcyt_pred,
                        Nuc = bgnuc_pred)

    } else { # one time point
      bgS <- data.frame(WC = mean(mnuc$value)*coeff + mean(mcyt$value)*(1-coeff),
                        Cyt = mean(mcyt$value),
                        Nuc = mean(mnuc$value))
    }

    # -------
    # subtract background only from value at anaphase
    if (bgSub == 0) {
      bgS$Nuc =  bgS$Nuc[locF$anaphase]
      bgS$Cyt = bgS$Cyt[locF$anaphase]
      bgS$WC = bgS$WC[locF$anaphase]
    }

    #start point is when 2 nuclei can be clearly segmented. Previous time points are omitted
    if (dim(bgS)[1] == 1) {
      startT = 1
    } else {
      #startT = min(which(!is.na(fg$SurfArea_Nuc2)==TRUE))
      #if (startT == Inf) {
        startT = 1
      #}
    }

    tosub1 = c('AvgIntOuterCore1_', 'AvgIntInnerCore2_', 'AvgIntNonCore12_', 'AvgIntNuc1_')
    tosub2 = c('AvgIntOuterCore4_', 'AvgIntInnerCore3_', 'AvgIntNonCore34_', 'AvgIntNuc2_')
    tosub1 = paste(tosub1, useSlice, sep='')
    tosub2 = paste(tosub2, useSlice, sep='')

    avgInt = c('AvgIntOuter', 'AvgIntInner', 'AvgIntNon', 'AvgIntNuc')

    totInt = c('TotIntOuter', 'TotIntInner', 'TotIntNon', 'TotIntNuc')

    avgIntBgSub = c('AvgIntOuter_bgS', 'AvgIntInner_bgS', 'AvgIntNon_bgS', 'AvgIntNuc_bgS')

    totIntBgSub = c('TotIntOuter_bgS', 'TotIntInner_bgS', 'TotIntNon_bgS', 'TotIntNuc_bgS')

    den = c('DenOuter', 'DenInner', 'DenNon', 'DenNuc')

    denBgSub = c('DenOuter_bgS', 'DenInner_bgS', 'DenNon_bgS', 'DenNuc_bgS')

    totNum = c('TotNumOuter', 'TotNumInner', 'TotNumNon', 'TotNumNuc')
    totNumBgSub = c('TotNumOuter_bgS', 'TotNumInner_bgS', 'TotNumNon_bgS', 'TotNumNuc_bgS')


    # -----

    for (nucNum  in c('1','2')) {
      nucName = paste('nucleus', nucNum,sep='')
      if (locF[,nucName]<=0) {
        next
      }
      if (nucNum == '1') {
        locNuc <- fg[,c('Timepoint', 'Vol_Nuc1', 'SurfArea_Nuc1', tosub1)]
      }
      if (nucNum == '2') {
        locNuc <- fg[,c('Timepoint', 'Vol_Nuc1', 'SurfArea_Nuc1', tosub2)]
      }
      nucid = paste(basename(file), nucNum, sep='_')
      names(locNuc) = c('Timepoint', 'Vol_Nuc', 'SurfArea_Nuc', avgInt)
      # without bg subtraction
      locNuc$Timepoint = (locNuc$Timepoint - locF$anaphase)*locF$dmin

      locNuc[ , totInt] = locNuc[ , avgInt]*locNuc$SurfArea_Nuc

      #bg values (these are the same for 2 nuclei). Whole cell bg is weighted average of nuclear and cytoplasm
      locNuc$WCbg = bgS$WC
      locNuc$CytoplasmInt = bgS$Cyt
      locNuc$NucleoplasmInt = bgS$Nuc

      #background subtraction
      locNuc[, avgIntBgSub] = locNuc[, avgInt] - bgS$WC
      locNuc[, totIntBgSub] = (locNuc[, avgInt] - bgS$WC)*locNuc$SurfArea_Nuc
      locNuc$rimWidth = rimWidth
      locNuc$voxel_XY_size_um = voxel_XY_size_um
      locNuc$coeff1 = calV[1]
      locNuc$coeff2 = calV[2]

      #convert background values to concentration and total number
      locNuc$ConcCytoplasm = (bgS$Cyt - locNuc$coeff1)*locNuc$coeff2
      locNuc$ConcNucleoplasm = (bgS$Nuc - locNuc$coeff1)*locNuc$coeff2
      locNuc$TotNumNucleoplasm = locNuc$ConcNucleoplasm*Na*locNuc$Vol_Nuc

      # densities #molecules/um2 on membrane using just intensity on rim
      locNuc[, den] = (locNuc[, avgInt] - locNuc$coeff1)*locNuc$coeff2*locNuc$voxel_XY_size_um*locNuc$rimWidth*Na

      # densities #molecules/um2 on membrane using just intensity on rim after rbg subtraction of cytoplasmic/nucleoplasmic intensity
      locNuc[, denBgSub] = locNuc[, avgIntBgSub]*locNuc$coeff2*locNuc$voxel_XY_size_um*locNuc$rimWidth*Na

      # total protein number t() is transpose
      locNuc[, totNum] =  t(apply(locNuc[, den], 1, '*', t(fractionArea)))*locNuc$SurfArea_Nuc
      locNuc[, totNumBgSub] =  t(apply(locNuc[, denBgSub], 1, '*', t(fractionArea)))*locNuc$SurfArea_Nuc

      locNuc$nucid = nucid
      locNuc$siRNA = locF$siRNA
      totalNuc= rbind(totalNuc, locNuc[startT:min(locF$maxf, dim(fg)[1]),])

    }

  }
  totalNuc$nucid  = factor(totalNuc$nucid)
  totalNuc$siRNA  = factor(totalNuc$siRNA)

  return(totalNuc)
}


# test some bg options-----

anaphase <- read.csv('./BGcomp/bg/anaphase.txt', skip=0, sep='\t', skipNul = TRUE) #contains the frame of anaphase


# read files
for (file in files[1]) {
  locF <- anaphase[anaphase['cell'] == file, ]

  #foreground data
  fg <- read_excel(paste('./Segmentation/', file, '/', file, '.xls', sep = ''))
  #bgdata
  bg <- read.csv(paste('./BGcomp/bg/', file, '_bg.txt', sep=''), skip=0, sep='\t', skipNul = TRUE)
  bg$frame = fg$Timepoint
}


mnuc <-  melt(bg[, c('frame', 'Nuc1_nuc_erode',   'Nuc2_nuc_erode', 'Nuc1_nuc_skel', 'Nuc2_nuc_skel')], id = 'frame')
#mnuc <-  melt(bg[, c('frame', 'Nuc1_nuc_erode',   'Nuc2_nuc_erode')], id = 'frame')
mcyt <-  melt(bg[, c('frame', 'Nuc1_cyt', 'Nuc2_cyt')], id = 'frame')
mavg <- aggregate(mnuc$value,by = list(mnuc$frame), na.rm = T, FUN = mean)
# predict(loess(Nuc1_nuc_skel~frame, bg ), bg$frame)

ggplot() + geom_line(data=mnuc, aes(x = frame, y = value, colour = variable, group = variable)) +
    stat_smooth(data=mnuc, aes(x = frame, y = value, colour = variable),  colour="gray", geom="line", size = 2, method = 'loess' , span = 0.5) +
geom_line(aes(x = bg$frame, y =  predict(loess(value ~ frame, mnuc, span = 0.5), bg$frame)))
#
pr1 <- predict(loess(x~Group.1, mavg ), mavg$Group.1 )
pr2 <- predict(loess(value ~ frame, mnuc ), bg$frame, span = 0.5)


# function computeMeansSd-----
computeMeansSd <- function(nucRims) {
  # compute mean and standard deviation across all nuclei in data set along time point

  nameV = names(nucRims)
  toAvg = c('Vol_Nuc', 'SurfArea_Nuc', 'AvgIntOuter_bgS', 'AvgIntInner_bgS', 'AvgIntNon_bgS', 'AvgIntNuc_bgS',
            'TotIntOuter_bgS', 'TotIntInner_bgS', 'TotIntNon_bgS', 'ConcCytoplasm', 'ConcNucleoplasm', 'TotNumNucleoplasm',
            'DenOuter_bgS', 'DenInner_bgS', 'DenNon_bgS', 'DenNuc_bgS',  'TotNumOuter_bgS', 'TotNumInner_bgS', 'TotNumNon_bgS', 'TotNumNuc_bgS',
            'DenOuter', 'DenInner', 'DenNon', 'DenNuc',  'TotNumOuter', 'TotNumInner', 'TotNumNon', 'TotNumNuc')

  toAvgName =  c('Vol_Nuc_um3', 'SurfArea_Nuc_um2', 'AvgIntOuter_bgS', 'AvgIntInner_bgS', 'AvgIntNon_bgS', 'AvgIntNuc_bgS',
                 'TotIntOuter_bgS', 'TotIntInner_bgS', 'TotIntNon_bgS', 'ConcCytoplasm_nM', 'ConcNucleoplasm_nM', 'TotNumNucleoplasm',
                 'DenOuter_bgS_prot_um2', 'DenInner_prot_mol_um2', 'DenNon_prot_mol_um2', 'DenNuc_prot_mol_um2',  'TotNumOuter_bgS',
                 'TotNumInner_bgS', 'TotNumNon_bgS', 'TotNumNuc_bgS','DenOuter_prot_um2', 'DenInner_prot_um2', 'DenNon_prot_um2',
                 'DenNuc_prot_um2',  'TotNumOuter_bgS', 'TotNumInner_bgS', 'TotNumNon_bgS', 'TotNumNuc_bgS')

  totMean <- data.frame()
  for (i in seq_along(toAvg)) {
    name <- toAvg[i]
    nameout <- toAvgName[i]
    locMean <- aggregate(nucRims[,name], list(nucRims$Timepoint), function(x) c(mean = mean(x, na.rm=TRUE), sd = sd(x, na.rm=TRUE)))
    locMean <- data.frame(locMean$Group.1, locMean[,'x'][,'mean'],  locMean[,'x'][,'sd'])
    names(locMean) <- c('timeAO_min', paste(nameout, '_avg', sep=''), paste(nameout, '_sd', sep=''))
    if (length(totMean) == 0) {
      totMean <- locMean
    } else {
      totMean <- cbind(totMean, locMean[,2:3])
    }
  }
  return(totMean)
}

# collect all data in table ----
maindir <- 'C:\\Users\\toni\\Dropbox\\NPCMaturation\\expdata\\livecellCalibrated'
wdNUPs<- c('160701_gfpNUP107z26z31','160706_gfpNUP107z26z31', '161014_gfpNUP107z26z31',
             '161111_gfpNUP107z26z31', '160715_TPRgfpc171','161027_gfpNUP214c2-12', '161102_gfpNUP214c2-12')

coeff = 0.6
nucRims_ss <- list()
nucRims_ms <- list()
nucRims_ip_ss <- list()
nucRims_ip_ms <- list()

for (wd in wdNUPs){
  setwd(file.path(maindir, wd, 'Result'))

  cellname <-  sub('\\d+_(.*)', '\\1', wd)
  dateId <- sub('(\\d+)_.*', '\\1', wd)
  # time lapse measurements
  files = file_path_sans_ext(list.files('./BGComp/bg/', pattern = ".*(Nup107|107|TPR|214).*txt"))
  files <- files[!grepl('.*interphase.*', files)]
  files <- sub("(.*)_bg", "\\1", files)

  nucRims <- concatResults(files, bgSub = 1,coeff, useSlice = 'ss')
  nucRims$date <- dateId
  nucRims$cell <- cellname
  nucRims_ss <- rbind(nucRims_ss, nucRims)

  nucRims <- concatResults(files,bgSub = 1, coeff, useSlice = 'ms')
  nucRims$date <- dateId
  nucRims$cell <- cellname
  nucRims_ms <- rbind(nucRims_ms, nucRims)

  #interphase measurements
  files <-  file_path_sans_ext(list.files('./BGComp/bg/', pattern = ".*interphase.*txt"))
  files <- sub("(.*)_bg", "\\1", files)
  if (length(files) > 0) {
    nucRims <- concatResults(files, bgSub = 1, coeff, useSlice = 'ss')
    nucRims$date <- dateId
    nucRims$cell <- cellname
    nucRims_ip_ss <- rbind(nucRims_ip_ss, nucRims)

    nucRims <- concatResults(files, bgSub = 1, coeff, useSlice = 'ms')
    nucRims$date <- dateId
    nucRims$cell <- cellname
    nucRims_ip_ms <- rbind(nucRims_ip_ms, nucRims)}
}

nucRims_ss <- data.frame(nucRims_ss)
nucRims_ms <- data.frame(nucRims_ms)
nucRims_ip_ss <- data.frame(nucRims_ip_ss)
nucRims_ip_ms <- data.frame(nucRims_ip_ms)

nucRims_ss$date <- factor(nucRims_ss$date)
nucRims_ms$date <- factor(nucRims_ms$date)
nucRims_ip_ss$date <- factor(nucRims_ip_ss$date)
nucRims_ip_ms$date <- factor(nucRims_ip_ms$date)





# plot interphase denisties for all experiments -----
setwd(maindir)
titles <- c("Single slice, BG subtracted", "Avg. 3 slices, BG subtracted", "Single slice", "Avg. 3 slices")
fnames <- c("interphase_ss_bgSub.png", "interphase_ms_bgSub.png", "interphase_ss.png", "interphase_ms.png")
for ( i in seq_along(titles)) {
  if (i == 1)
    df = nucRims_ip_ss[ ,c('date', 'cell', 'DenNuc_bgS')]
  if (i == 2)
    df = nucRims_ip_ms[ ,c('date', 'cell', 'DenNuc_bgS')]
  if (i == 3)
    df = nucRims_ip_ss[ ,c('date', 'cell', 'DenNuc')]
  if (i == 4)
    df = nucRims_ip_ms[ ,c('date', 'cell', 'DenNuc')]

  mdf <- melt(df, id = c('date', 'cell'))
  mdf_sd <- aggregate(mdf[, 'value'], by = mdf[, c('date', 'cell', 'variable')], sd)
  colnames(mdf_sd) <- c('date', 'cell', 'Density', 'sd')
  mdf_m <- aggregate(mdf[, 'value'], by = mdf[, c('date', 'cell', 'variable')], mean)

  colnames(mdf_m) <-  c('date', 'cell', 'Density', 'mean')

  mdf <- cbind(mdf_m , sd = mdf_sd$sd)
  mdf$Fac = 1

  mdf[mdf$cell == 'gfpNUP107z26z31', 'Fac'] = 4
  mdf[mdf$cell == 'gfpNUP214c2-12', 'Fac'] = 2

  mdf[mdf$cell == 'gfpNUP107z26z31', 'stoch'] = stochiometry$NUP107
  mdf[mdf$cell == 'gfpNUP214c2-12', 'stoch'] = stochiometry$NUP214
  toplot <- mdf


  p1 <- ggplot(data = toplot,
         aes(y = mean, x = cell, fill = cell, group = date, width = 0.5)) +
    geom_bar( stat = "identity", position = position_dodge(width = 0.6)) +
    geom_errorbar(aes(ymin = (mean-sd), ymax = (mean+sd), x = cell, colour = cell), position = position_dodge(width = 0.6)) +
    ylab('Interphase NUP density (#/um2)') + xlab('') + theme(legend.position="none") + ggtitle(titles[i])




  p2 <- ggplot(data = toplot,
         aes(y = mean/stoch, x = cell, fill = cell,  group = date, width = 0.5)) +
    geom_bar( stat = "identity", position = position_dodge(width = 0.6)) +
    geom_errorbar(aes(ymin = (mean-sd)/stoch, ymax = (mean+sd)/stoch, x = cell, colour = cell), position = position_dodge(width = 0.6)) +
    ylab('Interphase NPC density (#/um2)') + xlab('') + coord_cartesian(ylim = c(0, 15))+ theme(legend.position="none")

  toplot2 <- aggregate(toplot[,c('mean', 'stoch')], by = list(toplot$cell), FUN = mean)

  colnames(toplot2) <- c('cell', 'mean', 'stoch')
  p3 <- ggplot(data = toplot2,
              aes(y = mean, x = cell, fill = cell, width = 0.5)) +
    geom_bar( stat = "identity", position = position_dodge(width = 0.6)) +
    ylab('Interphase NUP density (#/um2)') + xlab('') + theme(legend.position="none")



  p4 <- ggplot(data = toplot2,
              aes(y = mean/stoch, x = cell, fill = cell, width = 0.5)) +
    geom_bar( stat = "identity", position = position_dodge(width = 0.6)) + coord_cartesian(ylim = c(0, 15))  +
    ylab('Interphase NPC density (#/um2)') + xlab('') + theme(legend.position="none")

  pInterphase <- arrangeGrob(p1,p2, p3, p4)
  ggsave(file = file.path('figures', fnames[i]), pInterphase, width = 6, height = 5)
}

# -----
fnames <- c("AvgInt_interphase_ss_bgSub.png", "AvgInt_interphase_ms_bgSub.png", "AvgInt_interphase_ss.png", "AvgInt_interphase_ms.png")
for ( i in seq_along(titles)) {
  if (i == 1)
    df = nucRims_ip_ss[ ,c('date', 'cell', 'AvgIntNuc_bgS')]
  if (i == 2)
    df = nucRims_ip_ms[ ,c('date', 'cell', 'AvgIntNuc_bgS')]
  if (i == 3)
    df = nucRims_ip_ss[ ,c('date', 'cell', 'AvgIntNuc')]
  if (i == 4)
    df = nucRims_ip_ms[ ,c('date', 'cell', 'AvgIntNuc')]

  mdf <- melt(df, id = c('date', 'cell'))
  mdf_sd <- aggregate(mdf[, 'value'], by = mdf[, c('date', 'cell', 'variable')], sd)
  colnames(mdf_sd) <- c('date', 'cell', 'Density', 'sd')
  mdf_m <- aggregate(mdf[, 'value'], by = mdf[, c('date', 'cell', 'variable')], mean)

  colnames(mdf_m) <-  c('date', 'cell', 'Density', 'mean')

  mdf <- cbind(mdf_m , sd = mdf_sd$sd)
  mdf$Fac = 1

  mdf[mdf$cell == 'gfpNUP107z26z31', 'Fac'] = 4
  mdf[mdf$cell == 'gfpNUP214c2-12', 'Fac'] = 2

  mdf[mdf$cell == 'gfpNUP107z26z31', 'stoch'] = stochiometry$NUP107
  mdf[mdf$cell == 'gfpNUP214c2-12', 'stoch'] = stochiometry$NUP214
  toplot <- mdf


  p1 <- ggplot(data = toplot,
               aes(y = mean, x = cell, fill = cell, group = date, width = 0.5)) +
    geom_bar( stat = "identity", position = position_dodge(width = 0.6)) +
    geom_errorbar(aes(ymin = (mean-sd), ymax = (mean+sd), x = cell, colour = cell), position = position_dodge(width = 0.6)) +
    ylab('Interphase Avg. F.I. (a.u.)') + xlab('') + theme(legend.position="none") + ggtitle(titles[i])


  toplot2 <- aggregate(toplot[,c('mean', 'stoch')], by = list(toplot$cell), FUN = mean)

  colnames(toplot2) <- c('cell', 'mean', 'stoch')

   p2 <- ggplot(data = toplot2,
               aes(y = mean, x = cell, fill = cell, width = 0.5)) +
    geom_bar( stat = "identity", position = position_dodge(width = 0.6)) +
    ylab('Interphase Avg. F.I. (a.u.)') + xlab('') + theme(legend.position="none")




  pInterphase <- arrangeGrob(p1,p2)
  ggsave(file = file.path('figures', fnames[i]), pInterphase, width = 6, height = 5)
}


# plot each data set -----

titles <- c("Single slice, BG subtracted", "Avg. 3 slices, BG subtracted", "Single slice", "Avg. 3 slices")
genes <- c('gfpNUP107z26z31', 'gfpNUP214c2-12', 'TPRgfpc171')
igene <- 3
gene <- genes[igene]
stochs <- c(stochiometry$NUP107, stochiometry$NUP214, stochiometry$TPR)
stoch <- stochs[igene]
fnames <- c("_ss_bgSub.png", "_ms_bgSub.png", "_ss.png", "_ms.png")
fnames <- sapply(fnames, FUN = function(x) paste(gene, x, sep = ""), USE.NAMES = F)
for ( i in seq_along(titles)) {

  if (i == 1)
    toplot <- nucRims_ss[nucRims_ss$cell == gene, c('date', 'cell', 'Timepoint', 'AvgIntNon_bgS','DenNon_bgS')]
  if (i == 2)
    toplot <- nucRims_ms[nucRims_ss$cell == gene, c('date', 'cell',  'Timepoint','AvgIntNon_bgS','DenNon_bgS')]
  if (i == 3)
    toplot <- nucRims_ss[nucRims_ss$cell == gene, c('date', 'cell',  'Timepoint', 'AvgIntNon','DenNon')]
  if (i == 4)
    toplot <- nucRims_ms[nucRims_ss$cell == gene, c('date', 'cell',  'Timepoint', 'AvgIntNon','DenNon')]
  colnames(toplot) <- c('date', 'cell', 'Timepoint', 'AvgInt','Den')

  # Average intensity in rim for all experiments,  plot the average +-  mult*std
  p1 <- ggplot(data = toplot, aes(x= Timepoint, y = AvgInt)) +
    stat_summary(fun.data = mean_sdl, geom="ribbon", alpha = 0.5, fun.args = list(mult = 1), na.rm = T)  +
    stat_summary(fun.y = mean, geom="line", alpha = 1, size=1.5, na.rm = T)  +
    coord_cartesian(xlim = c(-5, 120))  + ggtitle(titles[i]) +
    xlab('time AO (min)') + ylab('Avg. FI non-core (a.u.)') + theme(legend.position="right",
                                                                                  text = element_text (size =  10))
  # Average intensity in rim for each single experiment, plot the average +-  mult*std
  p2 <- ggplot(data = toplot, aes(x= Timepoint, y = AvgInt)) +
    stat_summary( aes(group = date, colour = date, fill = date), fun.data = mean_sdl, geom="ribbon", alpha = 0.5, fun.args = list(mult = 1), na.rm = T)  +
    stat_summary(fun.y = mean, geom="line", alpha = 1, size=1.5, na.rm = T,  aes(group = date, colour = date))  +
    coord_cartesian(xlim = c(-5, 120))  +
    xlab('time AO (min)') + ylab('Avg. FI non-core (a.u.)') + theme(legend.position="right",
                                                              text = element_text (size =  10))


  # Density for all experiments,  plot the average +-  mult*std
  p3 <- ggplot(data = toplot, aes(x= Timepoint, y = Den/stoch)) +
    stat_summary(fun.data = mean_sdl, geom="ribbon", alpha = 0.5, fun.args = list(mult = 1), na.rm = T)  +
    stat_summary(fun.y = mean, geom="line", alpha = 1, size=1.5, na.rm = T)  +
    coord_cartesian(xlim = c(-5, 120))  +
    xlab('time AO (min)') + ylab('NPC density (#/um2)') + theme(legend.position="right", text = element_text (size =  10))


  # Density in rim for each single experiment,  plot the average +-  mult*std
  p4 <- ggplot(data = toplot, aes(x= Timepoint, y = Den/stoch)) +
    stat_summary( aes(group = date, colour = date, fill = date), fun.data = mean_sdl, geom="ribbon", alpha = 0.5, fun.args = list(mult = 1), na.rm = T)  +
    stat_summary(fun.y = mean, geom="line", alpha = 1, size=1.5, na.rm = T,  aes(group = date, colour = date))  +
    coord_cartesian(xlim = c(-5, 120))  +
    xlab('time AO (min)') + ylab('NPC density (#/um2)') + theme(legend.position="right",
                                                                text = element_text (size =  10))

  pInterphase <- arrangeGrob(p1, p2, p3, p4)
  ggsave(file = file.path('figures', fnames[i]), pInterphase, width = 10, height = 8)
}
# plot single curves ----
p <- ggplot(data = nucRims_ss[nucRims_ss$cell == gene , ], aes_string(x= 'Timepoint', y = 'AvgIntNon'))+
  geom_line(aes(group= nucid, colour = nucid)) +
  xlab('time AO (min)') + ylab('Average Intensity') + theme(legend.position="top",
                                                            text = element_text (size =  10))
p


# ----

write.table(file = paste('./figures/', genes[1], '_ss.txt', sep = ""),computeMeansSd(nucRims_ss[nucRims_ss$cell == genes[1], ]), sep = '\t', row.names = F)
write.table(file = paste('./figures/', genes[1], '_ms.txt', sep = ""),computeMeansSd(nucRims_ms[nucRims_ss$cell == genes[1], ]), sep = '\t', row.names = F)
write.table(file = paste('./figures/', genes[2], '_ss.txt', sep = ""),computeMeansSd(nucRims_ss[nucRims_ss$cell == genes[2], ]), sep = '\t', row.names = F)
write.table(file = paste('./figures/', genes[2], '_ms.txt', sep = ""),computeMeansSd(nucRims_ms[nucRims_ss$cell == genes[2], ]), sep = '\t', row.names = F)
write.table(file = paste('./figures/', genes[3], '_ss.txt', sep = ""),computeMeansSd(nucRims_ss[nucRims_ss$cell == genes[3], ]), sep = '\t', row.names = F)
write.table(file = paste('./figures/', genes[3], '_ms.txt', sep = ""),computeMeansSd(nucRims_ms[nucRims_ss$cell == genes[3], ]), sep = '\t', row.names = F)
